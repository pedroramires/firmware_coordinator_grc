#include "serial_old.h"


/*******************************************************************************
*		         DECLARA��O DE VARI�VEIS GLOBAIS                               *
*******************************************************************************/


// Buffers de transmiss�o e recep��o

static unsigned char serialBufferRx[SERIAL_BUFFER_TAMANHO_MAX];
static unsigned char serialBufferTx[SERIAL_BUFFER_TAMANHO_MAX];

// Ponteiros do buffer de transmiss�o

static unsigned char serialIndiceBufferTxI;
static unsigned char serialIndiceBufferTxF;

// Ponteiros do buffer de recep��o

static unsigned char serialIndiceBufferRxI;
static unsigned char serialIndiceBufferRxF;

//Vari�veis de Controle

static unsigned char serialRxEmUso;
static unsigned char serialCntRxEmUso;

#define serial_inicia_bufferTx()	 	serialIndiceBufferTxI=0;serialIndiceBufferTxF=0;

#define serial_inicia_bufferRx()		serialIndiceBufferRxI=0;serialIndiceBufferRxF=0;

void insere_dado_bufferRx()
{

	serialRxEmUso=1;
	serialBufferRx[serialIndiceBufferRxF]=PHY_recebe_byte();
        serialIndiceBufferRxF++;


	if (serialIndiceBufferRxF==SERIAL_BUFFER_TAMANHO_MAX)
		serialIndiceBufferRxF=0;

	serialCntRxEmUso=0;

}

void retira_dado_bufferTx()
{
    if (serial_tem_dado_bufferTx())
    {
            PHY_envia_byte(serialBufferTx[serialIndiceBufferTxI]);
            serialIndiceBufferTxI++;
    }
    else
    {
            serialIndiceBufferTxI=0;
            serialIndiceBufferTxF=0;
            PHY_habilita_int_Tx (FALSO); //desabilita interrup��o de buffer de transmiss�o vazio;
    }
}

void serial_init()
{
	configura_uart();
	serial_inicia_bufferTx();
	serial_inicia_bufferRx();
}

void PHY_habilita_int_Tx (unsigned char _habilitado)
{
	if(_habilitado==VERDADEIRO)
	{

		PIE1bits.TX1IE=1;		// Habilita interrup��o na recep��o de um dado pela serial.


	}
	else
	{

		PIE1bits.TX1IE=0;

	}

}

unsigned char PHY_recebe_byte()
{
	while (!PIR1bits.RCIF); // Espera a recep��o de um byte
	return(RCREG);			// Retorna dado recebido.
	//return(fgetc(SERIAL));

}

void PHY_envia_byte(unsigned char _dado)
{

	while (!TXSTA1bits.TRMT); // Espera o hardware de transmiss�o est� desocupado.
	TXREG1=_dado;			 // Transmite dado.

}

unsigned char serial_retira_dado_bufferRx()
{
	unsigned char _dado;

	_dado=serialBufferRx[serialIndiceBufferRxI];
	serialIndiceBufferRxI++;

	if (serialIndiceBufferRxI==SERIAL_BUFFER_TAMANHO_MAX)
		serialIndiceBufferRxI=0;

	return(_dado);
}

unsigned char serial_insere_dados_bufferTx( char *_dados,unsigned char _tamanho)
{
	unsigned char _i=0;

	while (serial_tem_dado_bufferTx());

	for (_i=0;_i<_tamanho;_i++)
	{
                DESABILITA_INTERRUPCOES();
		serialBufferTx[serialIndiceBufferTxF++]=_dados[_i];
		HABILITA_INTERRUPCOES();
		// Verifica se a Fila estourou!

		if ((serialIndiceBufferTxF+1==serialIndiceBufferTxI)||((serialIndiceBufferTxF+1==SERIAL_BUFFER_TAMANHO_MAX)&&(serialIndiceBufferTxI==0)))
		{
			return(_i+1);
		}
	}

	PHY_habilita_int_Tx (VERDADEIRO);
	return(0);
}

unsigned char serial_tem_dado_bufferTx()
{
	if (serialIndiceBufferTxF>serialIndiceBufferTxI)
		return(serialIndiceBufferTxF-serialIndiceBufferTxI);
	else
		return(serialIndiceBufferTxI-serialIndiceBufferTxF);
}

unsigned char serial_tem_dado_bufferRx()
{
        if(RCSTAbits.OERR)
        {
            RCSTAbits.CREN = 0;   // Disable UART receiver
            RCSTAbits.CREN = 1;   // Enable UART receiver
        }
	if (serialIndiceBufferRxF>serialIndiceBufferRxI)
		return((serialIndiceBufferRxF-serialIndiceBufferRxI));
	else
		return((serialIndiceBufferRxI-serialIndiceBufferRxF));

}

unsigned char serial_Rx_Ocupado()
{

	return(serialRxEmUso);

}

void serial_Reseta_Flg_Rx_EmUso()
{
	if (serialRxEmUso==1)
	{
		if (++serialCntRxEmUso>1)
		{
			serialRxEmUso=0;
			serialCntRxEmUso=0;
		}
	}
}

void configura_uart()
{
    WORD aux;
	/*	Configura��o de BaudRate	*/
	/*

	SYNC BRG16 BRGH
	0      0     0 		8-bit/Asynchronous 		FOSC/[64 (n + 1)]
	0      0     1 		8-bit/Asynchronous		FOSC/[16 (n + 1)]
	0      1     0 		16-bit/Asynchronous		FOSC/[16 (n + 1)]
	0      1     1 		16-bit/Asynchronous		FOSC/[4 (n + 1)]
	1      0     x 		8-bit/Synchronous 		FOSC/[4 (n + 1)]
	1      1     x 		16-bit/Synchronous		FOSC/[4 (n + 1)]

	Obs: n � valor de SPBRGH:SPBRG, o valor de SPBRGH s� � usado se BRGH for 1;
	*/


#if BAUD_RATE <= 9600
	TXSTAbits.BRGH=0;
        BAUDCONbits.BRG16=0;
#define SPBRG_VALOR (((FRQ_CLOCK/BAUD_RATE)/64)-1)
#else
	TXSTAbits.BRGH=1;
        BAUDCONbits.BRG16=1;
#define SPBRG_VALOR (((FRQ_CLOCK/BAUD_RATE)/04)-1);
#endif


        aux = SPBRG_VALOR;
        SPBRG = (BYTE)SPBRG_VALOR;
//	SPBRGH = (uint8)(aux >> 8);
//        SPBRG = 0x70;
//        SPBRGH = 0x02;
        //SPBRG=32;				// valores de BRGH=0,BRG16=0 E SPBRG=12 para baud rate de 9600 para um clock de 8Mhz
	RCSTAbits.CREN = 0;
        //__delay_us(20);
        RCSTAbits.CREN = 1;
	TRISCbits.TRISC6=0;
        TRISCbits.TRISC7=1;
	RCSTAbits.SPEN=1;		// habilita pinos de IO como porta serial.
	TXSTAbits.SYNC=0; 		// serial modo ass�ncrono
	TXSTAbits.TXEN=1;		// habilita transmiss�o serial.
	RCSTAbits.CREN=1;		// habilita recep��o serial.
	PIE1bits.RC1IE=1;		// habilita interrup��o na recep��o de um dado pela serial.

}