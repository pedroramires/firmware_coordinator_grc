/*
 * File:   globais.h
 * Author: Renan Bessa
 *
 * Created on 3 de Outubro de 2013, 17:11
 */

#ifndef GLOBAIS_H
#define	GLOBAIS_H

/*******************************************************************************
>   ARQUIVOS DE CABE?ALHO DA BIBLIOTECA C PADR?O
 *******************************************************************************/

/*******************************************************************************
>   ARQUIVOS DE CABE?ALHO DO PROJETO
 *******************************************************************************/
#include <p18f27j13.h>
#include "HardwareProfile.h"
/*******************************************************************************
>   MACROS ,CONSTANTES e DEFINI??ES
 *******************************************************************************/
#define LOW_BYTE(x)     ((unsigned char)((x)&0xFF))
#define HIGH_BYTE(x)    ((unsigned char)(((x)>>8)&0xFF))
#define LOW_WORD(x)     ((unsigned short)((x)&0xFFFF))
#define HIGH_WORD(x)    ((unsigned short)(((x)>>16)&0xFFFF))

typedef unsigned char		uint8;				// 8-bit
typedef unsigned int		uint16;				// 16-bit
typedef unsigned short long     uint24;                        //24bits
typedef unsigned long int	uint32;				// 32-bit

//typedef bit                     int1;
typedef char                    int8;				// 8-bit
typedef int                     int16;				// 16-bit
typedef short long              int24;                          //24bits
typedef signed long int         int32;				// 32-bit

union T_Valor {
    uint16 valor;
    struct {
        uint8 low;
        uint8 high;
    }bytes;
};

struct TCalendario {
                        unsigned char  ano;
                        unsigned char  mes;
                        unsigned char  dia;
                        unsigned char  hora;
                        unsigned char  minuto;
                        unsigned char  segundo;
};

typedef struct TCalendario T_Calendario;

struct Tmedidas{
               unsigned short long      TensaoFaseA;
               unsigned short long      TensaoFaseB;
               unsigned short long      TensaoFaseC;
               unsigned short long      CorrenteFaseA;
               unsigned short long      CorrenteFaseB;
               unsigned short long      CorrenteFaseC;
               unsigned int             CorrenteFuga;
           };

struct Tmedidas2{
               uint32      CorrenteFaseA;
               uint32      CorrenteFaseB;
               uint32      CorrenteFaseC;
               uint16      CorrenteFuga;
               uint32      TensaoFaseA;
               uint32      TensaoFaseB;
               uint32      TensaoFaseC;
               uint8      TensaoBateria;
               uint8      Temperatura;

           };

struct Tstatus{
               uint8                EstadoDisjutor:1;
               uint8                EstadoReligador:2;
               uint8                NumeroOperacoes:5;
           };

union Ueventos{

    uint8   valor;
    struct Teventos{

                   uint8 OperacaoCurto :1;
                   uint8 OperacaoSobreCarga :1;
                   uint8 OperacaoCaboSolo :1;
                   uint8 QuedaFase :1;
                   uint8 Religamento :1;
                   uint8 RetornoFase :1;
                   uint8 Bloqueio   :1;
               }flags;
};

struct TRegistroSistema{

           struct Tmedidas medidas;
           struct Tstatus status;
           union Ueventos eventos;
           struct TCalendario data;
           uint32   CRC;
};

typedef struct TRegistroSistema T_RegistroSistema;

union URegistroSistema{

            uint8     *ponteiro;
            T_RegistroSistema *registro;
};

typedef union URegistroSistema U_RegistroSistema;

#define     MSG_SYSTEMA_TAMANHO_MAX     40
struct TMsgSistema {
                        unsigned int    controle;
                        unsigned char   id[4];
                        unsigned char   funcao;
                        unsigned char   tamanho;
                        unsigned char   dados[MSG_SYSTEMA_TAMANHO_MAX];
                        unsigned int    crc16;
};

typedef struct TMsgSistema T_MsgSistema;

#define BATERIA         5
#define TEMPERATURA     6


//#define BAUD_RATE   19200
#define BAUD_RATE2   9600



#define DESABILITA_INTERRUPCOES() INTCONbits.GIE = 0
#define HABILITA_INTERRUPCOES() INTCONbits.GIE = 1

#define LOCAL_WIFI      1
#define REMOTO_MODEM    2


#define     CONTROLE            0
#define     ID                  CONTROLE + 2
#define     FUNCAO              ID + 4
#define     TAMANHO             FUNCAO + 1
#define     DADOS               TAMANHO + 1
#define     CRC                 DADOS + 1

#define     TAMANHO_REGISTRO                sizeof(T_RegistroSistema)
struct TConfiguracaoInicial{
    uint32     religadorID;
    uint16     correnteSobrecarga;
    uint16     numReligamentosProgramado;
    uint16     tempoReligamentoProgramado;
    uint16     potenciaNominal;
    uint16     limiarFuga;
    uint16     tentativasReligamento;
    uint16     habilitaCaboaoSolo;
    uint16     habilitaReligamentoAutomatico;
    uint16     estadoReligador;
    uint16     periodoLogMedidas;
    uint32     indiceRegistroW;
    uint32     indiceRegistroL;

};

#define     TAMANHO_APN         20
#define     TAMANHO_PORTA       06

struct TConfiguracaoModem
{
    uint8       APN[TAMANHO_APN];
    uint8       Login[TAMANHO_APN];
    uint8       Senha[TAMANHO_APN];
    uint8       IPServidor[TAMANHO_APN];
    uint8       PortaServidor[6];

};

/*******************************************************************************
>   VARI?VEIS
 *******************************************************************************/


/******************************************************************************
//Defini??o dos Fun??es do Religador
*****************************************************************************/

#define FUNCAO_REQ_ID_CONCENTRADOR          0x01
#define FUNCAO_REQ_VERSAO_FW_RBT            0x02
#define FUNCAO_REQ_DATA_HORA                0x03
#define FUNCAO_GRAVA_DATA_HORA              0x04
#define FUNCAO_REQ_NUM_MEDIDORES            0x05
#define FUNCAO_REQ_OPERADORA                0x06
#define FUNCAO_REQ_CONFIGURACAO_GPRS        0x07
#define FUNCAO_GRV_CONFIGURACAO_GPRS        0x08
#define FUNCAO_REQ_ESTADO_RBT               0x09
#define FUNCAO_DESARMA_DISJUNTOR            0x0A
#define FUNCAO_ARMA_DISJUNTOR               0x0B
#define FUNCAO_REQ_CONST_CALIBRACAO         0x0C
#define FUNCAO_GRAVA_CONST_CALIBRACAO       0x0D
#define FUNCAO_MUDA_ESTADO_RELIGADOR        0x0E
#define FUNCAO_REQ_REG_MEMORIA              0x0F
#define FUNCAO_APAGA_TODA_MEMORIA           0x10
#define FUNCAO_REINICIA_INDICE_MEMORIA      0x11
#define FUNCAO_REQ_NUMERO_REGS_GRAVADOS     0x12
#define FUNCAO_REQ_TABELA                   0x13
#define FUNCAO_GRAVA_TABELA                 0x14
#define FUNCAO_REQ_ID_MEDIDOR               0x15
#define FUNCAO_REQ_VERSAO_FW_MEDIDOR        0x16
#define FUNCAO_REQ_INFO_MEDIDOR             0x17
#define FUNCAO_REQ_DATA_HORA_MEDIDOR        0x18
#define FUNCAO_REQ_ENERGIA_MEDIDOR          0x19
#define FUNCAO_GRV_INFO_MEDIDOR             0x1A
#define FUNCAO_GRV_DATA_HORA_MEDIDOR        0x1B
#define FUNCAO_REQ_ID_TRAFO                 0x1C
#define FUNCAO_REQ_VERSAO_FW_TRAFO          0x1D
#define FUNCAO_REQ_INFO_TRAFO               0x1E
#define FUNCAO_REQ_DATA_HORA_TRAFO          0x1F
#define FUNCAO_REQ_MEDICOES_TRAFO           0x20
#define FUNCAO_REQ_ENERGIA_TRAFO            0x21
#define FUNCAO_GRV_INFO_TRAFO               0x22
#define FUNCAO_GRV_DATA_HORA_TRAFO          0x23
#define FUNCAO_REQ_SENSORES_TRAFO           0x24
#define FUNCAO_GRAVA_PERIODO_LOG            0x25
#define FUNCAO_REQ_PERIODO_LOG              0x26


#define FUNCAO_GRAVA_APN                    0x11
#define FUNCAO_GRAVA_LOGIN                  0x12
#define FUNCAO_GRAVA_SENHA                  0x13
#define FUNCAO_GRAVA_IP_SERVIDOR            0x14
#define FUNCAO_GRAVA_PORTA_SERVIDOR         0x15

#define FUNCAO_REQ_REG_MEMORIA_WEB          0x21
#define FUNCAO_ACESSO_LOCAL                 0x22
#define FUNCAO_ACESSO_REMOTO_LIBERADO       0x23

#define FUNCAO_IMPRIME_CONSTANTES           0x54
#define FUNCAO_REQ_CONST_CALIBRACAO2        0x55
#define FUNCAO_EXECUTA_BEEP_LONGO           0x56
#define FUNCAO_EXECUTA_BEEP_CURTO           0x57
#define FUNCAO_DEBUG_LOG_MEMORIA            0x58
#define FUNCAO_DEBUG_GET_RTCC               0x59
#define FUNCAO_IMPRIME_VARIAVEIS_MODEM      0x60

#define FUNCAO_INVALIDA                     0xFF

//TODO: USER CODE

#endif	/* GLOBAIS_H */