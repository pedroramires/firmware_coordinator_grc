/*
 * File:   InfoMedidor.h
 * Author: Ramires
 *
 * Created on 18 de Fevereiro de 2014, 13:45
 */

#ifndef INFOMEDIDOR_H
#define	INFOMEDIDOR_H

#include "globais.h"
#include "SystemProfile.h"
#include "ConfigApp.h"





struct INFO{
    BYTE idMedidor[ID_MEDIDOR_SIZE];
    BYTE  versaoDoFirmware[FIRMWARE_VERSION_SIZE];
    BYTE  numeroDaCasa[NUM_CASA_SIZE];
    BYTE  unidadeConsumidora[UC_SIZE];
    WORD latitude;
    WORD longitude;
    BYTE  dataHoraMedidor[DATA_HORA_SIZE];
    DWORD energia;
    BYTE  datahoraDaMedicaoDeEnergia[DATA_HORA_SIZE];
    BYTE  retornoInfoMedidor;
    BYTE  retornoDataHoraMedidor;
    BYTE tensaoFaseMedidor;
    BYTE correnteFaseMedidor;
    BYTE  datahoraDaMedicao[DATA_HORA_SIZE];
    DWORD  idTransformador;
    DWORD versaoDoFirmwareTrafo;
    WORD potenciaNominal;
    WORD geoLocalizacao;
    BYTE  dataHoraTrafo[DATA_HORA_SIZE];
    BYTE tensaoFaseATrafo;
    BYTE tensaoFaseBTrafo;
    BYTE tensaoFaseCTrafo;
    BYTE correnteFaseATrafo;
    BYTE correnteFaseBTrafo;
    BYTE correnteFaseCTrafo;
    BYTE  status;
    BYTE  eventos;
    BYTE  datahoraDaMedicaoTrafo[DATA_HORA_SIZE];
    DWORD energiaTrafo;
    BYTE  datahoraDaMedicaoDeEnergiaTrafo[DATA_HORA_SIZE];
    BYTE  retornoInfoTrafo;
    BYTE  retornoDataHoraTrafo;
    BYTE  sensorTemperatura;
    BYTE  sensorBateria;

};

typedef struct INFO infoMedidor;

BYTE* getIDMedidor();
void  getVersaoDoFirmware(BYTE *valor);
void   getNumeroDaCasa(BYTE* retorno);
void   getUnidadeConsumidora(BYTE* retorno);
void   getLatitudeMedidor(BYTE* valor);
void   getLongitudeMedidor(BYTE* valor);
void   getDataHoraMedidor(BYTE* retorno);
void  getEnergia(BYTE* energia);
void   getDataHoraDaMedicaoDeEnergia(BYTE* retorno);
BYTE   getRetornoInfoMedidor();
BYTE   getRetornoDataHoraMedidor();
void   getTensaoFaseMedidor(BYTE *retorno);
void   getCorrenteFaseMedidor(WORD retorno);
void   getDatahoraDaMedicao(BYTE* retorno);
void   getPotenciaAtiva(BYTE *retorno);
WORD   getGeoLocalizacao();
BYTE   getStatus();
BYTE   getEvento();
BYTE   getSensorTemperatura();
BYTE   getSensorBateria();
BOOL   gravaInfoMedidor(BYTE* idMedidor, BYTE* numeroDaCasa, BYTE* unidadeConsumidora, BYTE* latitude, BYTE* longitude);
BYTE   gravaDataHoraMedidor(BYTE* idMedidor, BYTE* dataHora);

#endif	/* INFOMEDIDOR_H */