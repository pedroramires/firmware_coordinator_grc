/* 
 * File:   ConfiguracaoViaRadio.h
 * Author: Kassio
 *
 * Created on 12 de Fevereiro de 2014, 16:19
 */
//
//#ifndef CONFIGURACAOVIARADIO_H
//#define	CONFIGURACAOVIARADIO_H
//
//#ifdef	__cplusplus
//extern "C" {
//#endif

#include "Console.h"
#include "WirelessProtocols/MCHP_API.h"
//#include "ConfiguracaoViaRadio.h"
#include "acessaIdentificadores.h"

void ConfiguraRadioParaGRCNovo();

void ConfiguraRadioParaGRCNaRede();

void ConfiguradorIniciaComunicacaoGRCNovo(void);

void ConfiguradorIniciaComunicacaoGRCNaRede(BYTE* payload);

void ConfiguradorRequisitaAtributosDeGRCNovo();

void ConfiguradorRequisitaAtributosDeGRCNaRede(BYTE* address);

void defineShortAddressDoGRCQueEstaSendoConfigurado(BYTE* src, BYTE* dest);

void ConfiguradorAlteraAtributos(BYTE *atributos);

void ConfiguradorFinalizaConfiguracao();

void GRC_EnviaACK();

void GRC_GetAtributos(BYTE* atributos);

void enviaDados(BYTE* dados, BYTE comando);

void enviaDadosNaConfiguracaoGRCNaRede(BYTE* dados, BYTE comando, BYTE* address);

BYTE separaINFOPEER(BYTE* atributos);

void separaUC(BYTE* atributos, BYTE* uc);

void separaLONG_ADDRESS(BYTE* atributos, BYTE* long_address);

void separaPANID(BYTE* atributos, BYTE* panid);

void separaCHANNEL(BYTE* atributos, BYTE* channel);

void GRC_SalvaAtributos(BYTE* atributos);

void GRC_novo_envia_ACK_Comunicacao();

void GRC_na_rede_envia_ACK_Comunicacao(BYTE* shortAddressConfigurador);

void GRC_envia_ACK_DadosSalvos();

void ConfiguradorMostraAtributos(BYTE* atributos);

void separaPayloadRecebidoViaRadio(BYTE* bufferRX, BYTE sizeBuffer, BYTE* payload);

void GRC_MostraAtributos();

void Configurador_ProcuraLongAddress(BYTE* address);

void LongAddressIgual(BYTE* longAddressRecebidoDoConfigurador, BYTE* flag);

void GRC_SinalizaQueLongAddressRecebidoEquivaleAoSeu(BYTE* payload);

#ifdef	__cplusplus
}
#endif
//
//#endif	/* CONFIGURACAOVIARADIO_H */

