
#include "ConfiguracaoViaRadio.h"
#include "acessaIdentificadores.h"
#include "Console.h"

#define INDICE_INICIO_UC                0
#define INDICE_INICIO_LONG_ADDRESS      8
#define INDICE_INICIO_PANID             16
#define INDICE_INICIO_CHANNEL           18

static BYTE longaddressGRC[LONG_ADDRESS_SIZE];

void ConfiguraRadioParaGRCNovo(){

    setFlagGRCNovo(0x01);
    Printf("\r\n--------------- R�DIO CONFIGURADO COMO NOVO !!! ---------------");
    Reset();
}

void ConfiguraRadioParaGRCNaRede(){

    setFlagGRCNovo(0x00);
    Reset();
    Printf("\r\n--------------- R�DIO CONFIGURADO NA REDE !!! ---------------");
}

void ConfiguradorIniciaComunicacaoGRCNovo(void){
    
    MiApp_FlushTx();
    MiApp_WriteData(0xA0);

    //envia o comando para o primeiro da tabela de conexoes
    MiApp_UnicastConnection(0, FALSE);
    
}

void ConfiguradorIniciaComunicacaoGRCNaRede(BYTE* address){

    MiApp_FlushTx();
    MiApp_WriteData(0xB2);

    MiApp_UnicastAddress(address,FALSE,FALSE);
}

void ConfiguradorRequisitaAtributosDeGRCNovo(){
    
    MiApp_FlushTx();
    MiApp_WriteData(0xA2);

    //envia o comando para o primeiro da tabela de conexoes
    MiApp_UnicastConnection(0, FALSE);
}

void ConfiguradorRequisitaAtributosDeGRCNaRede(BYTE* address){

    MiApp_FlushTx();
    MiApp_WriteData(0xB4);

    MiApp_UnicastAddress(address,FALSE,FALSE);
}

void defineShortAddressDoGRCQueEstaSendoConfigurado(BYTE* src, BYTE* dest){
    int i;
    for(i = 0; i < SHORT_ADDRESS_SIZE; i++){
        dest[i] = src[i];
    }
}

void ConfiguradorAlteraAtributos(BYTE *atributos){
    MiApp_FlushTx();
    MiApp_WriteData(0xA4);

    //envia o comando para o primeiro da tabela de conexoes
    MiApp_UnicastConnection(0, FALSE);
}

void GRC_novo_envia_ACK_Comunicacao(){

    MiApp_FlushTx();
    MiApp_WriteData(0xA1);

    //envia o comando para o primeiro da tabela de conexoes
    MiApp_UnicastConnection(0, FALSE);
}

void GRC_na_rede_envia_ACK_Comunicacao(BYTE* shortAddressConfigurador){

    MiApp_FlushTx();
    MiApp_WriteData(0xB3);

    MiApp_UnicastAddress(shortAddressConfigurador,FALSE,FALSE);
}

void GRC_envia_ACK_DadosSalvos(){

    MiApp_FlushTx();
    MiApp_WriteData(0xA5);

    //envia o comando para o primeiro da tabela de conexoes
    MiApp_UnicastConnection(0, FALSE);
}

void GRC_GetAtributos(BYTE* atributos){

    int i = 0, j;
    BYTE uc[UC_SIZE];
    BYTE longAddress[LONG_ADDRESS_SIZE];

    getUC(uc);
    getLongAddress(longAddress);

    for(j = 0;j < UC_SIZE; j++){
        atributos[i] = uc[j];
        i++;
    }

    for(j = 0;j < LONG_ADDRESS_SIZE; j++){
        atributos[i] = longAddress[j];
        i++;
    }
}

void enviaDados(BYTE* dados, BYTE comando){
    int j;
    MiApp_FlushTx();
    MiApp_WriteData(comando);
    
    for(j = 1;j < TOTAL_DATA_PACKET_SIZE; j++){
        MiApp_WriteData(dados[j]);
    }

    MiApp_UnicastConnection(dados[0], FALSE);
}

void enviaDadosNaConfiguracaoGRCNaRede(BYTE* dados, BYTE comando, BYTE* address){
    
    int j;
    MiApp_FlushTx();
    MiApp_WriteData(comando);

    for(j = 1;j < TOTAL_DATA_PACKET_SIZE; j++){
        MiApp_WriteData(dados[j]);
    }

    MiApp_UnicastAddress(address,FALSE,FALSE);
}

BYTE separaINFOPEER(BYTE* atributos){
    BYTE infopeer = atributos[0];
    return infopeer;
}

void separaUC(BYTE* atributos, BYTE* uc){
    int i;
    for(i = 0;i < UC_SIZE;i++){
        uc[i] = atributos[i+INDICE_INICIO_UC];
    }
}

void separaLONG_ADDRESS(BYTE* atributos, BYTE* long_address){
    int i;
    for(i = 0;i < LONG_ADDRESS_SIZE;i++){
        long_address[i] = atributos[i+INDICE_INICIO_LONG_ADDRESS];
    }
}

void separaPANID(BYTE* atributos, BYTE* panid){
    int i;
    for(i = 0;i < PANID_SIZE;i++){
        panid[i] = atributos[i+INDICE_INICIO_PANID];
    }
}

void separaCHANNEL(BYTE* atributos, BYTE* channel){
    int i;
    for(i = 0;i < CHANNEL_SIZE;i++){
        channel[i] = atributos[i+INDICE_INICIO_CHANNEL];
    }
}

void GRC_SalvaAtributos(BYTE* atributos){

    BYTE uc[UC_SIZE];
    BYTE longAddress[LONG_ADDRESS_SIZE];
    BYTE estadoConfiguracao = 0xFF;
    BYTE pan[PANID_SIZE];
    BYTE channel[CHANNEL_SIZE];



    separaUC(atributos, uc);
    separaLONG_ADDRESS(atributos, longAddress);
    separaPANID(atributos, pan);
    separaCHANNEL(atributos, channel);
    
    setUC(uc);
    setLongAddress(longAddress);
    setPANID(pan);
    setPANIDConfigurado(pan);
    setChannel(channel);
    setEstadoConfiguracao(&estadoConfiguracao);
}

void ConfiguradorMostraAtributos(BYTE* atributos){
    int i;

    Printf("\r\nUNIDADE DO CLIENTE: ");
    for(i = INDICE_INICIO_UC; i < UC_SIZE; i++){
        PrintChar(atributos[i]);
    }
    
    Printf("\r\nLONG ADDRESS: ");
    for(i = i; i <(LONG_ADDRESS_SIZE+UC_SIZE); i++){
        PrintChar(atributos[i]);
    }
}

void separaPayloadRecebidoViaRadio(BYTE* bufferRX, BYTE sizeBuffer, BYTE* payload){
    int i;
    for(i = 0;i < (sizeBuffer - 1); i++){
        payload[i] = bufferRX[i+1];//PULA O PRIMEIRO BYTE(COMANDO)
    }
}

void GRC_MostraAtributos(){

    int i;

    BYTE uc[UC_SIZE];
    BYTE longAddress[LONG_ADDRESS_SIZE];
    BYTE channel[4];
    BYTE pan[2];

    getUC(uc);
    getLongAddress(longAddress);
    getChannel(channel);
    getPANID(pan);

    Printf("\r\nUNIDADE DO CLIENTE: ");
    for(i = 0; i<(UC_SIZE); i++){
        PrintChar(uc[i]);
    }
    Printf("\r\nLONG ADDRESS: ");
    for(i = 0; i<(LONG_ADDRESS_SIZE); i++){
        PrintChar(longAddress[i]);
    }
    Printf("\r\nPANID: ");
    for(i = 0; i<(PANID_SIZE); i++){
        PrintChar(pan[i]);
    }
    Printf("\r\nChannelMap: ");
    for(i = 0; i<(CHANNEL_SIZE); i++){
        PrintChar(channel[i]);
    }
}

void Configurador_ProcuraLongAddress(BYTE* address){
    int j;
    
    MiApp_FlushTx();
    MiApp_WriteData(0xB0);

    for(j = 0;j < LONG_ADDRESS_SIZE; j++){
        MiApp_WriteData(address[j]);
    }

    MiApp_BroadcastPacket(FALSE);

}

void LongAddressIgual(BYTE* longAddressRecebidoDoConfigurador, BYTE* flag){

    getLongAddress(longaddressGRC);
    *flag = 0x00;

    
    //OS PRIMEIROS OITO BYTES DO PAYLOAD RECEBIDO EQUIVALEM AO LONG ADDRESS
    if(longAddressRecebidoDoConfigurador[0] == longaddressGRC[0] && longAddressRecebidoDoConfigurador[1] == longaddressGRC[1]){
        if(longAddressRecebidoDoConfigurador[2] == longaddressGRC[2] && longAddressRecebidoDoConfigurador[3] == longaddressGRC[3]){
            if(longAddressRecebidoDoConfigurador[4] == longaddressGRC[4] && longAddressRecebidoDoConfigurador[5] == longaddressGRC[5]){
                if(longAddressRecebidoDoConfigurador[6] == longaddressGRC[6] && longAddressRecebidoDoConfigurador[7] == longaddressGRC[7]){
                    *flag = 0x01;
                }
            }
        }
    }
}

void GRC_SinalizaQueLongAddressRecebidoEquivaleAoSeu(BYTE* address){

    MiApp_FlushTx();
    MiApp_WriteData(0xB1);

    MiApp_UnicastAddress(address,FALSE,FALSE);
}