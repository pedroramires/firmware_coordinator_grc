/*
 * File:   TratadorDePacotes.h
 * Author: Ramires
 *
 * Created on 17 de Fevereiro de 2014, 14:13
 */

#ifndef TRATADORDEPACOTES_H
#define	TRATADORDEPACOTES_H

#include "../MontadorDePacotesGRC.h"

void trataPacoteRequisitaIDMedidor(BYTE *bufferDerecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote);
void trataPacoteRequisitaVersaoFirmware(BYTE *bufferDerecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote);
void trataPacoteRequisitaVersaoFirmwareTrafo(BYTE *bufferDerecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote);
void trataPacoteRequisitaInfoMedidor(BYTE *bufferDerecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote);
void trataPacoteRequisitaData_Hora(BYTE *bufferDerecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote);
void trataPacoteRequisitaData_HoraTrafo(BYTE *bufferDerecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote);
void trataPacoteRequisitaEnergiaMedidor(BYTE *bufferDerecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote);
void trataPacoteRequisitaEnergiaTrafo(BYTE *bufferDerecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote);
void trataPacoteRequisitaMedicoesInstantaneasTrafo(BYTE *bufferDerecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote);
void trataPacoteGravaInfoMedidor(BYTE *bufferDerecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote);
void trataPacoteGravaData_HoraMedidor(BYTE *bufferDerecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote);
void trataPacoteGravaData_HoraTrafo(BYTE *bufferDerecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote);
void trataPacoteRequisitaDadosDeMedicaoUC(BYTE *bufferDeRecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote);
void trataPacoteRespostaArmaRele(BYTE *bufferDeRecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote);
void trataPacoteRespostaDesarmaRele(BYTE *bufferDeRecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote);
void trataPacoteRequisitaTotalData(BYTE *bufferDeRecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote);


#endif	/* TRATADORDEPACOTES_H */