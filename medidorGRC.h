/***************************************************
Nome do M�dulo: medidorGRC.h
Hexa Tecnolgia Copyright 2013 como trabalho n�o publicado.
Todos os direitos reservados.
A informa��o contida � propriedade confidencial da
Hexa Tecnolgia. O uso, c�pia, tranfer�ncia ou revela��o
das informa��es a seguir s�o proibidos exceto quando
houver permiss�o por escrito da Hexa Tecnolgia.

Escrito primeiramente por AlbertoHexa em 10 de Dezembro de 2013, 16:42
Descri��o do M�dulo:
(Descri��o detalhado m�dulo aqui).
 ***************************************************/
//#ifndef medidorGRC_H
//#define medidorGRC_H

#include "GenericTypeDefs.h"

/*******************************************************************************
>   ARQUIVOS DE CABE�ALHO DA BIBLIOTECA C PADR�O E COMPILADOR
 *******************************************************************************/

/*******************************************************************************
>   ARQUIVOS DE CABE�ALHO DO PROJETO
 *******************************************************************************/

/*******************************************************************************
>   MACROS ,CONSTANTES e DEFINI��ES
 *******************************************************************************/
struct T_PacoteGRC {

    BYTE   comando;
    WORD  endereco;
    BYTE   tamanho;
    BYTE   dados[40];

};

typedef struct T_PacoteGRC TPacoteGRC;


//Maquina de Estados

#define     ESTADO_GRC_AGUARDA_COMANDO      0
#define     ESTADO_GRC_DADOS                1

#define REQUISITA_ENERGIA               0x00
#define ESPERANDO_ENERGIA               0x01
#define REQUISITA_DATA_HORA_MEDIDOR     0x02
#define ESPERANDO_DATA_HORA_MEDIDOR     0x03
#define REQUISITA_TENSAO                0x04
#define ESPERANDO_TENSAO                0x05
#define REQUISITA_CORRENTE              0x06
#define ESPERANDO_CORRENTE              0x07
#define REQUISITA_POTENCIA_ATIVA        0x08
#define ESPERANDO_POTENCIA_ATIVA        0x09
#define REQUISITA_FATOR_POTENCIA        0x0A
#define ESPERANDO_FATOR_POTENCIA        0x0B


extern TPacoteGRC  pacoteMedidorGRC,pacoteRecebidoGRC;

extern BYTE                medidorEnergia[4];
extern BYTE                medidorDataHoraMedidor[6];
extern BYTE                medidorTensaoFaseMedidor[2];
extern BYTE                medidorCorrenteFaseMedidor[2];
extern BYTE                medidorPotenciaAtiva[4];
extern BYTE                medidorFatorPotencia[2];
/*******************************************************************************
>   PROT�TIPOS DE FUN��ES
 *******************************************************************************/
DWORD medidorGRC_envia_comando(TPacoteGRC *pct);
void zerarEnergiaMedidor();
DWORD make32(BYTE var1, BYTE var2, BYTE var3, BYTE var4);
void medidor_acumula_energia();

void medidor_Reset();

void gravaHoraMedidorGRC(BYTE *dados);

/*********************************************************************
 * @Func�o:         uint8 medidorGRC_init(void)
 *
 * @PreCondi��o:    nenhuma
 *
 * @Entrada:        (n�o se aplica)
 *
 * @Sa�da:          0 - Se inicializa��o correta
 *                  1 - Se inicializa��o teve erro (Falha no Barramento SPI)
 *
 * @Efeitos :       Altera estado de vari�veis e perif�rico (SPI).
 *
 * @Descri��o:      Esta fun��o manda os comandos para a configura��o inicial
 *                  do mcp3903.
 *
 * @Autor:          Alberto
 *
 ********************************************************************/
BYTE medidorGRC_init(void);

//#endif

