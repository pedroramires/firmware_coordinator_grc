/***************************************************
Nome do M�dulo: medidorGRC.c
Hexa Tecnolgia Copyright 2013 como trabalho n�o publicado.
Todos os direitos reservados.
A informa��o contida � propriedade confidencial da
Hexa Tecnolgia. O uso, c�pia, tranfer�ncia ou revela��o
das informa��es a seguir s�o proibidos exceto quando
houver permiss�o por escrito da Hexa Tecnolgia.

Escrito primeiramente por AlbertoHexa em 10 de Dezembro de 2013, 16:43
Descri��o do M�dulo:
(Descri��o detalhado m�dulo aqui).
 ***************************************************/


/*******************************************************************************
>   ARQUIVOS DE CABE�ALHO DA BIBLIOTECA C PADR�O E COMPILADOR
 *******************************************************************************/

#include <string.h>
/*******************************************************************************
>   ARQUIVOS DE CABE�ALHO DO PROJETO
 *******************************************************************************/
#include "medidorGRC.h"
#include "serial_old.h"
#include "acessaIdentificadores.h"
#include "Utils.h"
#include "MontadorDePacotesGRC.h"
#include "TimeDelay.h"
/*******************************************************************************
>   MACROS ,CONSTANTES e DEFINI��ES
 *******************************************************************************/


/*******************************************************************************
>   VARI�VEIS DO M�DULO
 *******************************************************************************/

    BYTE                medidorEnergia[4];
    BYTE                medidorDataHoraMedidor[6];
    BYTE                medidorTensaoFaseMedidor[2];
    BYTE                medidorCorrenteFaseMedidor[2];
    BYTE                medidorPotenciaAtiva[4];
    BYTE                medidorFatorPotencia[2];
    BYTE                estadoLeiturasMedidor = 0;
/*******************************************************************************
>   IMPLEMENTA��O DE FUN��ES
 ******************************************************************************/
TPacoteGRC  pacoteMedidorGRC={0},pacoteRecebidoGRC={0};
BYTE       pacoteGRCRecebido=0;
BYTE estadoGRCAT=0;
DWORD      GRCEnergia, GRCPotenciaAtiva;
conversor2 GRCCorrente, GRCTensao;

BYTE       comandoGRC[40], dataHoraMedidor[6];

DWORD potencia(BYTE base, BYTE exp)
{
    DWORD ret = 1;


    while(exp)
    {
        ret = ret * base;
        exp--;
    }

    return ret;
}

WORD atoih(char st[])
{
    BYTE tamanho,i,aux;
    WORD retorno;

    //strupr(st);

    tamanho = strlen(st);
    retorno = 0;
    for (i=0;i<tamanho;i++)
    {
        aux = st[tamanho-i-1];
        if ((aux>47)&&(aux<58))
        {
            aux = aux - 48;

        }
        else if ((aux>65)&&(aux<71))
        {
            aux = aux - 55;

        }
        else
        {
            aux = 0;
        }

        retorno += aux*(potencia(16,(i)));

    }

    return(retorno);
}

static BYTE comando, i;
static BYTE estadoProtocoloGRC = 0;
static BYTE dadosMedidorGRC[20];
static BYTE idxMedidorGRC = 0;


void medidorGRC_trata_protocolo(BYTE byte)
{

    BYTE aux[6]={0};

    switch (estadoProtocoloGRC)
    {

        case ESTADO_GRC_AGUARDA_COMANDO:
            comando = byte;
            estadoProtocoloGRC = ESTADO_GRC_DADOS;
            break;

        case ESTADO_GRC_DADOS:
            if (byte == 'X')
            {
                //Pacote completo recebido
                estadoProtocoloGRC = ESTADO_GRC_AGUARDA_COMANDO;
                dadosMedidorGRC[idxMedidorGRC] = 0x00;
                idxMedidorGRC = 0;
                pacoteGRCRecebido=1;
//                Printf("\r\n");
//                Printf("Resposta ");
//                Printf("\r\n");
//
//                ConsolePutString(dadosMedidorGRC);
//                Printf("\r\n");
                switch(comando)
                {
                    case 'R':

                        strncpy(aux,dadosMedidorGRC,3);
                        pacoteRecebidoGRC.endereco = atoih(aux);
                        memset(aux,0x00,sizeof(aux));
                        for(i=0;dadosMedidorGRC[3+i]>0;i++)
                        {
                            strncpy(aux,&dadosMedidorGRC[3+2*i],2);
                            pacoteRecebidoGRC.dados[i] = atoih(aux);
                        }
                        break;

                   case 'G':

                        memset(aux,0x00,sizeof(aux));
                        for(i=0;dadosMedidorGRC[2*i]>0;i++)
                        {
                            strncpy(aux,&dadosMedidorGRC[2*i],2);
                            pacoteRecebidoGRC.dados[i] = atoi(aux);
                        }
                        break;
                    default:
                        idxMedidorGRC= 0;
                        estadoProtocoloGRC=0;
                        break;
//
                        
                }
                comando = 0;
            }
            else
            {
                dadosMedidorGRC[idxMedidorGRC++] = byte;
            }
            break;
        }

}




DWORD medidorGRC_envia_comando(TPacoteGRC *pct)
{

    BYTE i;
    memset(comandoGRC,0x00,40);
    switch(pct->comando)
    {
        case 'R':

            comandoGRC[0] = 'R';
            sprintf(&comandoGRC[1],"%03X",pct->endereco);
            sprintf(&comandoGRC[4],"%02X",pct->tamanho);
            comandoGRC[6] = 'X';
            comandoGRC[7] = 0x00;

        break;

        case 'W':

            comandoGRC[0] = 'W';
            sprintf(&comandoGRC[1],"%03X",pct->endereco);
            for(i=0;i<pct->tamanho;i++)
            {
                sprintf(&comandoGRC[4+2*i],"%02X",pct->dados[i]);
            }
            comandoGRC[4+2*pct->tamanho] = 'X';
            comandoGRC[4+2*pct->tamanho+1] = 0x00;

            break;

        case 'E':
            comandoGRC[0]='E';
            comandoGRC[1]='X';
            break;

        case 'L':
            comandoGRC[0]='L';
            comandoGRC[1]='X';
            break;

        case 'S':
            comandoGRC[0]='S';
            comandoGRC[1]='X';
            break;

        case 'G':
            comandoGRC[0]='G';
            comandoGRC[1]='X';
            break;

        case 'N':
            comandoGRC[0]='N';
            comandoGRC[1]='X';
            break;

        case 'C':
            comandoGRC[0]='C';
            comandoGRC[1]='X';
            break;

        case 'T':

            comandoGRC[0] = 'T';
            //sprintf(&comandoGRC[1],"%03X",pct->endereco);
//              Printf("\r\n");
//                Printf("Tamanho");
//                PrintDec(pct->tamanho);
//                Printf("\r\n");
            for(i=0;i<pct->tamanho;i++)
            {
                sprintf(&comandoGRC[1+2*i],"%02d",pct->dados[i]);
//                Printf("\r\n");
//                Printf("Comando GRC");
//                ConsolePrintString(comandoGRC,strlen(comandoGRC));
//                Printf("\r\n");
            }
            comandoGRC[2*pct->tamanho+1] = 'X';

//            Printf("\r\n");
//            Printf("Pacote de grava��o de hora: ");
//            Printf("\r\n");
//            ConsolePutString(comandoGRC);
//
//            Printf("\n\r");

            break;
    }
//    ConsolePutString(comandoGRC);
    serial_insere_dados_bufferTx(comandoGRC,strlen(comandoGRC));
}

void gravaHoraMedidorGRC(BYTE *dados)
{
     pacoteMedidorGRC.comando = 'T';
     pacoteMedidorGRC.tamanho = 7;
     memcpy(pacoteMedidorGRC.dados,dados,7);
//      Printf("\r\n");
//                Printf("Bytes de Data/Hora: ");
//                Printf("\r\n");
//                PrintDec(pacoteMedidorGRC.dados[0]);
//                Printf("|");
//                PrintDec(pacoteMedidorGRC.dados[1]);
//                Printf("|");
//                PrintDec(pacoteMedidorGRC.dados[2]);
//                Printf("|");
//                PrintDec(pacoteMedidorGRC.dados[3]);
//                Printf("|");
//                PrintDec(pacoteMedidorGRC.dados[4]);
//                Printf("|");
//                PrintDec(pacoteMedidorGRC.dados[5]);
//                Printf("|");
//                PrintDec(pacoteMedidorGRC.dados[6]);
//      Printf("\r\n");
     medidorGRC_envia_comando(&pacoteMedidorGRC);

}


DWORD make32(BYTE var1, BYTE var2, BYTE var3, BYTE var4)
{
    return (((((DWORD)var1 << 8 | var2) << 8) | var3) << 8) | var4;
}




void medidorGRC_atualiza_medidas()
{
    static BYTE contadorTimeout=0;
    static BYTE contadorTimeoutResetMedidor=0;

    switch(estadoGRCAT)
    {
        case REQUISITA_ENERGIA:
            pacoteMedidorGRC.comando = 'R';
            pacoteMedidorGRC.endereco = 0x100;
            pacoteMedidorGRC.tamanho = 0x04;
//            Printf("\r\n");
//            Printf("Comando de Energia Enviado!");
//            Printf("\r\n");
            medidorGRC_envia_comando(&pacoteMedidorGRC);
            estadoGRCAT=ESPERANDO_ENERGIA;
            break;

        case ESPERANDO_ENERGIA:
            if (pacoteGRCRecebido)
            {
                //GRCEnergia = make32(pacoteRecebidoGRC.dados[3],pacoteRecebidoGRC.dados[2],pacoteRecebidoGRC.dados[1],pacoteRecebidoGRC.dados[0]);
                estadoGRCAT=REQUISITA_DATA_HORA_MEDIDOR;
                pacoteGRCRecebido =0;
//                Printf("\r\n");
//                Printf("Bytes de Energia: ");
//                Printf("\r\n");
//                PrintDec(pacoteRecebidoGRC.dados[0]);
//                Printf("|");
//                PrintDec(pacoteRecebidoGRC.dados[1]);
//                Printf("|");
//                PrintDec(pacoteRecebidoGRC.dados[2]);
//                Printf("|");
//                PrintDec(pacoteRecebidoGRC.dados[3]);
//                Printf("\r\n");
//                setLeituraEnergia(pacoteRecebidoGRC.dados);
                
                memcpy(medidorEnergia,pacoteRecebidoGRC.dados,  4);
                if (medidorEnergia[1]> 0x09)
                    medidor_acumula_energia();
                contadorTimeoutResetMedidor=0;
            }
            else
            {
                //Faz nada
                contadorTimeout++;
                if(contadorTimeout>2)
                {
                    Printf("\r\n Timeout energia\r\n");
                    contadorTimeout = 0;
                    pacoteGRCRecebido =0;
                    idxMedidorGRC= 0;
                    estadoProtocoloGRC=0;

                    contadorTimeoutResetMedidor++;

                    estadoGRCAT=REQUISITA_ENERGIA;
                   
                }
            }
            break;

        case REQUISITA_DATA_HORA_MEDIDOR:
            pacoteMedidorGRC.comando = 'G';
            medidorGRC_envia_comando(&pacoteMedidorGRC);
            estadoGRCAT=ESPERANDO_DATA_HORA_MEDIDOR;
            break;

        case ESPERANDO_DATA_HORA_MEDIDOR:
            if (pacoteGRCRecebido)
            {
                dataHoraMedidor[0] = pacoteRecebidoGRC.dados[0];
                dataHoraMedidor[1] = pacoteRecebidoGRC.dados[1];
                dataHoraMedidor[2] = pacoteRecebidoGRC.dados[2];
                //Dia do M�s
                dataHoraMedidor[3] = pacoteRecebidoGRC.dados[3];
                //pacoteRecebidoGRC.dados[4] � equivalente ao dia da semana
                //M�s
                dataHoraMedidor[4] = pacoteRecebidoGRC.dados[5];
                //Ano
                dataHoraMedidor[5] = pacoteRecebidoGRC.dados[6];
                estadoGRCAT=REQUISITA_TENSAO;
                pacoteGRCRecebido =0;

//                Printf("\r\n");
//                Printf("Bytes de Data/Hora: ");
//                Printf("\r\n");
//                PrintDec(pacoteRecebidoGRC.dados[0]);
//                Printf("|");
//                PrintDec(pacoteRecebidoGRC.dados[1]);
//                Printf("|");
//                PrintDec(pacoteRecebidoGRC.dados[2]);
//                Printf("|");
//                PrintDec(pacoteRecebidoGRC.dados[3]);
//                Printf("|");
//                PrintDec(pacoteRecebidoGRC.dados[4]);
//                Printf("|");
//                PrintDec(pacoteRecebidoGRC.dados[5]);
//                Printf("\r\n");



//                setLeituraDataHora(dataHoraMedidor);
                memcpy(medidorDataHoraMedidor,dataHoraMedidor,6);
                memset(&pacoteRecebidoGRC,0,sizeof(TPacoteGRC));
                contadorTimeoutResetMedidor=0;
            }
            else
            {
                //Faz nada
                contadorTimeout++;
                if(contadorTimeout>2)
                {
                    Printf("\r\n Timeout data hora \r\n");
                    contadorTimeout = 0;
                    pacoteGRCRecebido =0;
                    idxMedidorGRC= 0;
                    estadoProtocoloGRC=0;

                    contadorTimeoutResetMedidor++;

                    estadoGRCAT = REQUISITA_DATA_HORA_MEDIDOR;
                }
            }
            break;

        case REQUISITA_TENSAO:
            pacoteMedidorGRC.comando = 'R';
            pacoteMedidorGRC.endereco = 0x011;
            pacoteMedidorGRC.tamanho = 0x02;
            medidorGRC_envia_comando(&pacoteMedidorGRC);
            estadoGRCAT=ESPERANDO_TENSAO;
            break;

        case ESPERANDO_TENSAO:
            if (pacoteGRCRecebido)
            {

//                GRCTensao.toArrayBytes[0] = pacoteRecebidoGRC.dados[0];
//                GRCTensao.toArrayBytes[1] = pacoteRecebidoGRC.dados[1];
                estadoGRCAT=REQUISITA_CORRENTE;
                pacoteGRCRecebido =0;
//                Printf("\r\n");
//                Printf("Bytes de Tens�o: ");
//                Printf("\r\n");
//                PrintDec(pacoteRecebidoGRC.dados[1]);
//                Printf("|");
//                PrintDec(pacoteRecebidoGRC.dados[0]);
//                Printf("\r\n");

//                setLeituraTensao(pacoteRecebidoGRC.dados);
                memcpy(medidorTensaoFaseMedidor,pacoteRecebidoGRC.dados,2);
                contadorTimeoutResetMedidor=0;
            }
            else
            {
                //Faz nada
                Printf("\r\n Timeout tensao\r\n");
                contadorTimeout++;
                if(contadorTimeout>2)
                {
                    contadorTimeout = 0;
                    pacoteGRCRecebido =0;
                    idxMedidorGRC= 0;
                    estadoProtocoloGRC=0;

                    contadorTimeoutResetMedidor++;

                    estadoGRCAT = REQUISITA_TENSAO;
                }
            }
            break;

        case REQUISITA_CORRENTE:
            pacoteMedidorGRC.comando = 'R';
            pacoteMedidorGRC.endereco = 0x00D;
            pacoteMedidorGRC.tamanho = 0x02;
            medidorGRC_envia_comando(&pacoteMedidorGRC);
            estadoGRCAT=ESPERANDO_CORRENTE;
            break;

        case ESPERANDO_CORRENTE:
            if (pacoteGRCRecebido)
            {

//                GRCCorrente.toArrayBytes[0] = pacoteRecebidoGRC.dados[0];
//                GRCCorrente.toArrayBytes[1] = pacoteRecebidoGRC.dados[1];
                estadoGRCAT=REQUISITA_POTENCIA_ATIVA;
                pacoteGRCRecebido =0;

//                setLeituraCorrente(pacoteRecebidoGRC.dados);
                memcpy(medidorCorrenteFaseMedidor,pacoteRecebidoGRC.dados,  2);
                contadorTimeoutResetMedidor=0;
            }
            else
            {
                //Faz nada
                contadorTimeout++;
                if(contadorTimeout>2)
                {
                    Printf("\r\n Timeout corrente\r\n");
                    contadorTimeout = 0;
                    pacoteGRCRecebido =0;
                    idxMedidorGRC= 0;
                    estadoProtocoloGRC=0;

                    contadorTimeoutResetMedidor++;

                    estadoGRCAT = REQUISITA_CORRENTE;
                }
            }
            break;

        case REQUISITA_POTENCIA_ATIVA:
            pacoteMedidorGRC.comando = 'R';
            pacoteMedidorGRC.endereco = 0x019;
            pacoteMedidorGRC.tamanho = 0x04;
            medidorGRC_envia_comando(&pacoteMedidorGRC);
            estadoGRCAT= ESPERANDO_POTENCIA_ATIVA;
            break;

        case ESPERANDO_POTENCIA_ATIVA:

            if (pacoteGRCRecebido)
            {
//              GRCPotenciaAtiva = make32(pacoteRecebidoGRC.dados[3],pacoteRecebidoGRC.dados[2],pacoteRecebidoGRC.dados[1],pacoteRecebidoGRC.dados[0]);
                estadoGRCAT = REQUISITA_FATOR_POTENCIA ;
                pacoteGRCRecebido =0;
//                Printf("\r\n");
//                Printf("Bytes de Potencia: ");
//                Printf("\r\n");
//                PrintDec(pacoteRecebidoGRC.dados[0]);
//                Printf("|");
//                PrintDec(pacoteRecebidoGRC.dados[1]);
//                Printf("|");
//                PrintDec(pacoteRecebidoGRC.dados[2]);
//                Printf("|");
//                PrintDec(pacoteRecebidoGRC.dados[3]);
//                Printf("\r\n");

//                setLeituraPotenciaAtiva(pacoteRecebidoGRC.dados);
                memcpy(medidorPotenciaAtiva,pacoteRecebidoGRC.dados,  4);
                contadorTimeoutResetMedidor=0;
                
            }
            else
            {
                //Faz nada
                contadorTimeout++;
                if(contadorTimeout>2)
                {
                    Printf("\r\n Timeout potencia\r\n");
                    contadorTimeout = 0;
                    pacoteGRCRecebido =0;
                    idxMedidorGRC= 0;
                    estadoProtocoloGRC=0;

                    contadorTimeoutResetMedidor++;

                    estadoGRCAT = REQUISITA_POTENCIA_ATIVA;
                }
            }
            break;
       case REQUISITA_FATOR_POTENCIA:
            pacoteMedidorGRC.comando = 'R';
            pacoteMedidorGRC.endereco = 0x021;
            pacoteMedidorGRC.tamanho = 0x02;
            medidorGRC_envia_comando(&pacoteMedidorGRC);
            estadoGRCAT= ESPERANDO_FATOR_POTENCIA;
            break;

        case ESPERANDO_FATOR_POTENCIA:

            if (pacoteGRCRecebido)
            {
//              GRCPotenciaAtiva = make32(pacoteRecebidoGRC.dados[3],pacoteRecebidoGRC.dados[2],pacoteRecebidoGRC.dados[1],pacoteRecebidoGRC.dados[0]);
                estadoGRCAT = REQUISITA_ENERGIA ;
                pacoteGRCRecebido =0;
//                Printf("\r\n");
//                Printf("Bytes de Potencia: ");
//                Printf("\r\n");
//                PrintDec(pacoteRecebidoGRC.dados[0]);
//                Printf("|");
//                PrintDec(pacoteRecebidoGRC.dados[1]);
//                Printf("|");
//                PrintDec(pacoteRecebidoGRC.dados[2]);
//                Printf("|");
//                PrintDec(pacoteRecebidoGRC.dados[3]);
//                Printf("\r\n");

//                setLeituraPotenciaAtiva(pacoteRecebidoGRC.dados);
                memcpy(medidorFatorPotencia,pacoteRecebidoGRC.dados,  2);
                estadoLeiturasMedidor =1;
                contadorTimeoutResetMedidor=0;
            }
            else
            {
                //Faz nada
                contadorTimeout++;
                if(contadorTimeout>2)
                {
                    Printf("\r\n Timeout fator de potencia\r\n");
                    contadorTimeout = 0;
                    pacoteGRCRecebido =0;
                    idxMedidorGRC= 0;
                    estadoProtocoloGRC=0;

                    contadorTimeoutResetMedidor++;

                    estadoGRCAT = REQUISITA_FATOR_POTENCIA;
                }
            }
            break;
       default:
           estadoGRCAT = 0;
           pacoteGRCRecebido =0;
           idxMedidorGRC= 0;
           estadoProtocoloGRC=0;
           contadorTimeout =0;
           contadorTimeoutResetMedidor = 0;

           break;
    }

    if(contadorTimeoutResetMedidor>=3 ){
        medidor_acumula_energia();
        medidor_Reset();
        zerarEnergiaMedidor();
        contadorTimeoutResetMedidor=0;
    }
}


void zerarEnergiaMedidor()
{
    DWORD_VAL aux;


    memset(&aux.v[0], 0x00, 4);
    setLeituraEnergia(&aux.v[0]);


    GRCEnergia = aux.Val;
    memset(medidorEnergia,0x00,4);

    pacoteMedidorGRC.comando = 'S';
    medidorGRC_envia_comando(&pacoteMedidorGRC);
//    pacoteMedidorGRC.comando = 'W';
//    pacoteMedidorGRC.endereco = 0x100;
//    pacoteMedidorGRC.tamanho = 0x04;
//    memset(pacoteMedidorGRC.dados,0,4);
////    pacoteMedidorGRC.dados[0]=0x01;
////    pacoteMedidorGRC.dados[1]=0x01;
////    pacoteMedidorGRC.dados[2]=0x01;
////    pacoteMedidorGRC.dados[3]=0x01;
////            Printf("\r\n");
////            Printf("Comando de Energia Enviado!");
////            Printf("\r\n");
//    medidorGRC_envia_comando(&pacoteMedidorGRC);

}


void medidorGRC_tarefas()
{
    if(serial_tem_dado_bufferRx())
    {
        medidorGRC_trata_protocolo(serial_retira_dado_bufferRx());
    }
}

void medidor_acumula_energia(){

    DWORD aux;

    memcpy(&GRCEnergia, medidorEnergia, 4);

    aux = getLeituraEnergia();
    aux = aux + GRCEnergia;
    setLeituraEnergia(&aux);

    memset(&GRCEnergia, 0x00, 4);
    memset(medidorEnergia, 0x00, 4);

    
    pacoteMedidorGRC.comando = 'S';
    medidorGRC_envia_comando(&pacoteMedidorGRC);

}


void medidor_Reset(){
    MCRL_MEDICAO = 0;
    DelayMs(200);
    MCRL_MEDICAO = 1;

}