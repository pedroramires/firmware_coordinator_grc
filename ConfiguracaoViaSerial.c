#include "ConfiguracaoViaSerial.h"

#if defined(PROTOCOL_MIWI_PRO)


ROM char * const menu =
    "\r\n     1: Enable/Disable Join"
    "\r\n     2: Show Family Tree"
    "\r\n     3: Show Routing Table"
    "\r\n     4: Send Message"
    "\r\n     5: Set Family Tree"
    "\r\n     6: Set Routing Table"
    "\r\n     7: Set Neighbor Routing Table"
    "\r\n     8: Start Frequency Agility"
    "\r\n     9: Socket"
    "\r\n     r: Ressync Connection"
    "\r\n     s: Tasks"
    "\r\n     x: Remove Connection"
    "\r\n     z: Dump Connection"
    "\r\n     y: 0x00"
    "\r\n     u: 0x01"
    "\r\n     i: 0x02"
    "\r\n     o: 0x03"
    "\r\n     d: destroy network"
    "\r\n     t: update table with nodes actives"
    ;

ROM char * const menuConfiguracao =
    "\r\n     1: Alterar Tipo"
    "\r\n     2: Alterar Info Peer"
    "\r\n     3: Alterar Short Address"
    "\r\n     4: Alterar Long Address"
//    "\r\n     5: Alterar PANID"
    "\r\n     5: Sair"
    ;

ROM char * const menuAlterarTipo =
    "\r\n     0: CORDINATOR"
    "\r\n     1: PAN CORDINATOR"
    "\r\n     2: END DEVICE"
    ;

ROM char * const msg_01 =
    "\r\nEnter a menu choice: "
    ;

BYTE convertASCII_to_HEX(BYTE c){

    if (('0' <= c) && (c <= '9'))
        c -= '0';
    else if (('a' <= c) && (c <= 'f'))
        c = c - 'a' + 10;
    else if (('A' <= c) && (c <= 'F'))
        c = c - 'A' + 10;
    else
        c = 0;

    return c;
}

BYTE GetHexDigit( void )
{
    BYTE    c;

    while (!serial2IsGetReady());
    c = serial2_retira_dado_bufferRx();
    ConsolePut(c);

    return convertASCII_to_HEX(c);
}

void formatArrayToHex(BYTE* src , int16 size_dest, BYTE* dest){
    int i = 0,j = 0;
    while(i < size_dest){
        dest[i] = src[j] << 4;
        j++;
        dest[i] += src[j];
        j++;
        i++;
    }
}

BYTE GetMACByte( void )
{
    BYTE    oneByte;

    oneByte = GetHexDigit() << 4;
    oneByte += GetHexDigit();

    return oneByte;
}

BYTE GetTwoDigitDec(void)
{
    BYTE oneByte;

    oneByte = GetHexDigit() * 10;
    oneByte += GetHexDigit();

    return oneByte;
}

void PrintMenu( void )
{
    ConsolePutROMString(menu);
    ConsolePutROMString( (ROM char * const) msg_01 );
}

void showInfo(void)
{
    BYTE sa[2];
    BYTE la[8];
    BYTE pi[2];

    ConsolePutROMString( (ROM char * const) "\r\n   TIPO: " );
    switch (getType())
    {
        case 0x01:
            ConsolePutROMString( (ROM char * const) "PAN CORDINATOR" );
            break;
        case 0x00:
            ConsolePutROMString( (ROM char * const) "CORDINATOR" );
            break;
        case 0x02:
            ConsolePutROMString( (ROM char * const) "END DEVICE" );
            break;

        default:
            break;
    }

    ConsolePutROMString( (ROM char * const) "\r\n   INFO PEER: " );
    PrintChar(getInfoPeer());

    ConsolePutROMString( (ROM char * const) "\r\n   SHORT ADDRESS: " );
    getShortAddress(sa);
    ConsolePrintString(sa, 2);

    ConsolePutROMString( (ROM char * const) "\r\n   LONG ADDRESS: " );
    getLongAddress(la);
    ConsolePrintString(la, 8);

    ConsolePutROMString( (ROM char * const) "\r\n   PANID: " );
    getPANID(pi);
    ConsolePrintString(pi, 2);
    ConsolePutROMString( (ROM char * const) "\r\n" );

}

void PrintMenuConfiguracao( void )
{
    showInfo();
    ConsolePutROMString(menuConfiguracao);
    ConsolePutROMString( (ROM char * const) msg_01 );
}

void inverteArray(BYTE *ori, BYTE *dest, int size)
{
    int i,j = 0;
    for(i = (size - 1);i >=0; i--)
    {
        dest[i] = ori[j];
        j++;
    }
}

void ProcessMenuConfiguracao( void )
{

    BYTE        c;
    int         i;
    BYTE        short_address[2];
    BYTE        long_address[8];
    BYTE        panid[2];
    WORD_VAL    panid_hex_val = 0x0000;

    // Get the key value.
    while( !serial2IsGetReady());

    c = ConsoleGet();
    ConsolePut( c );

    switch (c)
    {
        case '1':
            ConsolePutROMString(menuAlterarTipo);
            ConsolePutROMString( (ROM char * const) msg_01 );

            setType(GetHexDigit());

            break;

        case '2':
            ConsolePutROMString( (ROM char * const) "\r\n DIGITE O NOVO INFO PEER (COME�ANDO PELO MAIS SIGNIFICATIVO):" );
            setInfoPeer(GetMACByte());
            break;

        case '3':
            ConsolePutROMString( (ROM char * const) "\r\n DIGITE O NOVO SHORT ADDRESS (COME�ANDO PELO MAIS SIGNIFICATIVO):" );

            for(i=0;i<2;i++)
            {
                short_address[i] = GetMACByte();
            }

            setShortAddress(short_address);
            break;

        case '4':
            ConsolePutROMString( (ROM char * const) "\r\n DIGITE O NOVO LONG ADDRESS (COME�ANDO PELO MAIS SIGNIFICATIVO):" );

            for(i=0;i<8;i++)
            {
                long_address[i] = GetMACByte();
            }

            setLongAddress(long_address);
            break;

        case '5':
            ConsolePutROMString( (ROM char * const) "\r\n DIGITE O NOVO PANID (COME�ANDO PELO MAIS SIGNIFICATIVO):" );

            for(i=0;i<2;i++)
            {
                panid[i] = GetMACByte();
            }

            setPANID(panid);
            break;

        case '6':
            ConsolePutROMString( (ROM char * const) "\r\n------------SAIU DO MODO DE CONFIGURACAO!!!--------------" );            setFlagEmConfiguracao(0x00);
            Reset();
            break;

        default:
            break;
    }
}

void ProcessMenu(BYTE c)
{
    BYTE        i;

    switch (c)
    {
        case '1':
            ConsolePutROMString((ROM char * const)"\r\n1=ENABLE_ALL 2=ENABLE PREV 3=ENABLE SCAN 4=DISABLE: ");
            while( !serial2IsGetReady());
        	c = ConsoleGet();
        	ConsolePut(c);
        	switch(c)
        	{
        		case '1':
        		    MiApp_ConnectionMode(ENABLE_ALL_CONN);
        		    break;

        		case '2':
        		    MiApp_ConnectionMode(ENABLE_PREV_CONN);
        		    break;

        		case '3':
        		    MiApp_ConnectionMode(ENABLE_ACTIVE_SCAN_RSP);
        		    break;

        		case '4':
        		    MiApp_ConnectionMode(DISABLE_ALL_CONN);
        		    break;

        	    default:
        	        break;
            }
            break;

        case '2':
            Printf("\r\nFamily Tree: ");
            for(i = 0; i < NUM_COORDINATOR; i++)
            {
                PrintChar(FamilyTree[i]);
                Printf(" ");
            }
            break;

        case '3':
            Printf("\r\nMy Routing Table: ");
            for(i = 0; i < NUM_COORDINATOR/8; i++)
            {
                PrintChar(RoutingTable[i]);
            }
            Printf("\r\nNeighbor Routing Table: ");
            for(i = 0; i < NUM_COORDINATOR; i++)
            {
                BYTE j;
                for(j = 0; j < NUM_COORDINATOR/8; j++)
                {
                    PrintChar(NeighborRoutingTable[i][j]);
                }
                Printf(" ");
            }
            break;

        case '4':
            {
                WORD_VAL tempShortAddr;
                Printf("\r\n1=Broadcast 2=Unicast Connection 3=Unicast Addr: ");
                while( !serial2IsGetReady());
            	c = serial2_retira_dado_bufferRx();
            	ConsolePut(c);

    	        MiApp_FlushTx();
    	        MiApp_WriteData('T');
    	        MiApp_WriteData('e');
    	        MiApp_WriteData('s');
    	        MiApp_WriteData('t');
    	        MiApp_WriteData(0x0D);
    	        MiApp_WriteData(0x0A);
        	    switch(c)
        	    {
            	    case '1':
            	        if(MiApp_BroadcastPacket(FALSE))
                            Printf("\r\nMensagem enviada com sucesso");
                        else
                            Printf("\r\nMensagem n�o enviada");
            	        break;

            	    case '2':
            	        Printf("\r\nConnection Index: ");
            	        while( !serial2IsGetReady());
            	        c = GetHexDigit();
                        if(MiApp_UnicastConnection(c, FALSE))
                            Printf("\r\nMensagem enviada com sucesso");
                        else
                            Printf("\r\nMensagem n�o enviada");
                        break;

                    case '3':
                        Printf("\r\n1=Long Address 2=Short Address: ");
                        while( !serial2IsGetReady());
                    	c = serial2_retira_dado_bufferRx();
                    	ConsolePut(c);
                    	switch(c)
                    	{
                        	case '1':
                        	    Printf("\r\nDestination Long Address: ");
                        	    for(i = 0; i < MY_ADDRESS_LENGTH; i++)
                        	    {
                                        tempLongAddress[MY_ADDRESS_LENGTH-1-i] = GetMACByte();
                                    }
                                    if(MiApp_UnicastAddress(tempLongAddress, TRUE, FALSE))
                                        Printf("\r\nMensagem enviada com sucesso");
                                    else
                                        Printf("\r\nMensagem n�o enviada");
                                    break;

                        	case '2':
                        	    Printf("\r\nDestination Short Address: ");
                        	    tempLongAddress[1] = GetMACByte();
                        	    tempLongAddress[0] = GetMACByte();
                        	    if(MiApp_UnicastAddress(tempLongAddress, FALSE, FALSE))
                                        Printf("\r\nMensagem enviada com sucesso");
                                    else
                                        Printf("\r\nMensagem n�o enviada");
                        	    break;

                        	default:
                        	    break;
                        }
                        break;

                    default:
                        break;
            	}
            }
            break;

        case '5':
            {
                Printf("\r\nMSB of the Coordinator: ");
                i = GetMACByte();
                Printf("\r\nSet MSB of this Node's Parent: ");
                FamilyTree[i] = GetMACByte();
            }
            break;

        case '6':
            {
                Printf("\r\nSet my Routing Table: ");
                for(i = 0; i < NUM_COORDINATOR/8; i++)
                {
                    RoutingTable[i] = GetMACByte();
                    Printf(" ");
                }
            }
            break;

        case '7':
            {
                BYTE j;

                Printf("\r\nNode Number: ");
                i = GetMACByte();
                Printf("\r\nContent of Neighbor Routing Table: ");
                for(j = 0; j < NUM_COORDINATOR/8; j++)
                {
                    NeighborRoutingTable[i][j] = GetMACByte();
                    Printf(" ");
                }
            }
            break;

        case '8':
            {
                MiApp_InitChannelHopping(0xFFFFFFFF);
            }
            break;


        case '9':
            {
                Printf("\r\nSocket: ");
                PrintChar(MiApp_EstablishConnection(0xFF, CONN_MODE_INDIRECT));
            }
            break;
        case 'r':
            {
                MiApp_ResyncConnection(0xFF,0xFFFFFFFF);
            }
            break;
        case 's':
            {
                 MiWiPROTasks();

            }
            break;
        case 'x':
            {
                BYTE address;
                Printf("\r\nRemove connection handle: ");
                address=GetMACByte();
                MiApp_RemoveConnection(address);
            }
            break;
        case 'z':
        case 'Z':
            {
                DumpConnection(0xFF);
            }

            break;
        case 'y':
            {
                BYTE address;
                Printf("\r\n0x00 para o endere�o: ");
                address=GetMACByte();

                MiApp_FlushTx();
    	        MiApp_WriteData(0x00);
    	        MiApp_WriteData(0x0D);
    	        MiApp_WriteData(0x0A);

                MiApp_UnicastConnection(address, FALSE);

            }
            break;
        case 'u':
            {
                BYTE address;
                Printf("\r\n0x01 para o endere�o: ");
                address=GetMACByte();

                MiApp_FlushTx();
    	        MiApp_WriteData(0x01);
    	        MiApp_WriteData(0x0D);
    	        MiApp_WriteData(0x0A);

                MiApp_UnicastConnection(address, FALSE);

            }
            break;
            case 'i':
            {
                BYTE address;
                Printf("\r\n0x02 para o endere�o: ");
                address=GetMACByte();

                MiApp_FlushTx();
    	        MiApp_WriteData(0x02);
    	        MiApp_WriteData(0x0D);
    	        MiApp_WriteData(0x0A);

                MiApp_UnicastConnection(address, FALSE);

            }
            break;
            case 'o':
            {
                BYTE address;
                Printf("\r\n0x03 para o endere�o: ");
                address=GetMACByte();

                MiApp_FlushTx();
    	        MiApp_WriteData(0x03);
    	        MiApp_WriteData(0x0D);
    	        MiApp_WriteData(0x0A);

                MiApp_UnicastConnection(address, FALSE);

            }
            break;
            case 'd':{
                MiApp_FlushTx();
    	        MiApp_WriteData(0x06);
    	        MiApp_WriteData(0x0D);
    	        MiApp_WriteData(0x0A);
                MiApp_BroadcastPacket(FALSE);

            }
            break;

            case 't':
            {
                MiApp_FlushTx();
    	        MiApp_WriteData(0x05);
    	        MiApp_WriteData(0x0D);
    	        MiApp_WriteData(0x0A);
                MiApp_BroadcastPacket(FALSE);

            }
            break;

            case 'w':
            {
                DumpConnection(0x00);
            }
            break;

        default:
            break;
    }
    PrintMenu();
}

#else
    #define PrintMenu()
    #define ProcessMenu()
#endif



void trataMenuDeEnvio(BYTE dado){
    //cria o pacote a ser enviado via radio
    MiApp_FlushTx();
    MiApp_WriteData('T');
    MiApp_WriteData('e');
    MiApp_WriteData('s');
    MiApp_WriteData('t');
    MiApp_WriteData(0x0D);
    MiApp_WriteData(0x0A);



    if(dado == 'z' && !RECEBENDO_ENDERECO && !RECEBENDO_INICE && !RECEBENDO_LONG_ADDRESS && !RECEBENDO_POTENCIA){
        Printf("\r\nMENU DE TESTE: ");
        Printf("\r\n        1 - VER TABELA DE ROTEAMENTO; ");
        Printf("\r\n        2 - ENVIAR MENSAGEM BROADCAST; ");
        Printf("\r\n        3 - ENVIAR MENSAGEM UNICAST PELO ENDERE�O; ");
        Printf("\r\n        4 - ENVIAR MENSAGEM UNICAST PELO �NDICE; ");
        Printf("\r\n        5 - VER TABELA DE CONEX�ES; ");
        Printf("\r\n        6 - EXCLUIR CONEX�O; ");
        Printf("\r\nESCOLHA UMA OP��O: ");
    }else
    if(dado == 't' && !RECEBENDO_ENDERECO && !RECEBENDO_INICE && !RECEBENDO_LONG_ADDRESS && !RECEBENDO_POTENCIA){
        RECEBENDO_ENDERECO = TRUE;
        REQUISITANDO_REDE=TRUE;
        indiceBuffer = 0;
        Printf("\r\nDestination Short Address: ");

    }else
    if(dado == 'd' && !RECEBENDO_ENDERECO && !RECEBENDO_INICE && !RECEBENDO_LONG_ADDRESS && !RECEBENDO_POTENCIA){
        RECEBENDO_ENDERECO = TRUE;
        DESTRUINDO_REDE=TRUE;
        indiceBuffer = 0;
        Printf("\r\nDestination Short Address: ");

    }else
    if(dado == 'e' && !RECEBENDO_ENDERECO && !RECEBENDO_INICE && !RECEBENDO_LONG_ADDRESS && !RECEBENDO_POTENCIA){
        exibe_energia();


    }else
     if(dado == 'l' && !RECEBENDO_ENDERECO && !RECEBENDO_INICE && !RECEBENDO_LONG_ADDRESS && !RECEBENDO_POTENCIA){
        RECEBENDO_LONG_ADDRESS = TRUE;
        indiceBuffer = 0;
        Printf("\r\nDestination Long Address: ");
     }else
     if(dado == 'f' && !RECEBENDO_ENDERECO && !RECEBENDO_INICE && !RECEBENDO_LONG_ADDRESS && !RECEBENDO_POTENCIA){
            Printf("\r\nFamily Tree: ");
            for(i = 0; i < NUM_COORDINATOR; i++)
            {
                PrintChar(FamilyTree[i]);
                Printf(" ");
            }
     }else
     if(dado == 'p' && !RECEBENDO_ENDERECO && !RECEBENDO_INICE && !RECEBENDO_LONG_ADDRESS && !RECEBENDO_POTENCIA){
        RECEBENDO_POTENCIA = TRUE;
        indiceBuffer=0;
        Printf("\r\nPotencia: ");
     }else
     if(dado == '1' && !RECEBENDO_ENDERECO && !RECEBENDO_INICE && !RECEBENDO_LONG_ADDRESS && !RECEBENDO_POTENCIA){
        //mostra a tabela de roteamento
        Printf("\r\nMy Routing Table: ");
        for(i = 0; i < NUM_COORDINATOR/8; i++)
        {
            PrintChar(RoutingTable[i]);
        }
        Printf("\r\nNeighbor Routing Table: ");
        for(i = 0; i < NUM_COORDINATOR; i++)
        {
            BYTE j;
            for(j = 0; j < NUM_COORDINATOR/8; j++)
            {
                PrintChar(NeighborRoutingTable[i][j]);
            }
            Printf(" ");
        }
        Printf("\r\nESCOLHA UMA OP��O: ");
    }else
    if(dado == '2' && !RECEBENDO_ENDERECO && !RECEBENDO_INICE && !RECEBENDO_LONG_ADDRESS && !RECEBENDO_POTENCIA){
        if(MiApp_BroadcastPacket(FALSE))
            Printf("\r\nMensagem Broadcast enviada com sucesso");
        else
            Printf("\r\nMensagem Broadcast n�o enviada");
    }else
    if(dado == '3' && !RECEBENDO_ENDERECO && !RECEBENDO_INICE && !RECEBENDO_LONG_ADDRESS && !RECEBENDO_POTENCIA){
//        BYTE Address[2];
        RECEBENDO_ENDERECO = TRUE;
        indiceBuffer = 0;
        Printf("\r\nDestination Short Address: ");
//        Address[1] = GetMACByte();
//        Address[0] = GetMACByte();
//        if(MiApp_UnicastAddress(Address, FALSE, FALSE))
//            Printf("\r\nMensagem enviada com sucesso");
//        else
//            Printf("\r\nMensagem n�o enviada");
//        TxNum++;
//
    }else
    if(dado == '4' && !RECEBENDO_ENDERECO && !RECEBENDO_INICE && !RECEBENDO_LONG_ADDRESS && !RECEBENDO_POTENCIA){
//        BYTE indice;
        RECEBENDO_INICE = TRUE;
        Printf("\r\nConnection Index: ");
//        indice = GetMACByte();
//        if(MiApp_UnicastAddress(indice, FALSE, FALSE))
//            Printf("\r\nMensagem enviada com sucesso");
//        else
//            Printf("\r\nMensagem n�o enviada");
//        TxNum++;
    }else
    if(dado == '5' && !RECEBENDO_ENDERECO && !RECEBENDO_INICE && !RECEBENDO_LONG_ADDRESS && !RECEBENDO_POTENCIA){
        DumpConnection(0xFF);
    }else
    if(dado == '6' && !RECEBENDO_ENDERECO && !RECEBENDO_INICE && !RECEBENDO_LONG_ADDRESS && !RECEBENDO_POTENCIA){
        MiApp_RemoveConnection(0XFF);
        ler_Conf_Inicial2();
//        Reset();
    }else
    if(dado == '7' && !RECEBENDO_ENDERECO && !RECEBENDO_INICE && !RECEBENDO_LONG_ADDRESS && !RECEBENDO_POTENCIA){
        Reset();
    }else
    if(dado == '8' && !RECEBENDO_ENDERECO && !RECEBENDO_INICE && !RECEBENDO_LONG_ADDRESS && !RECEBENDO_POTENCIA){

        Printf("\r\n--------------- SETADO COMO CONFIGURADO !!! ---------------");
        estadoConfiguracao = 0xFF;
        setEstadoConfiguracao(estadoConfiguracao);
        MiApp_RemoveConnection(0xFF);
        Reset();
    }else
    if(dado == '9' && !RECEBENDO_ENDERECO && !RECEBENDO_INICE && !RECEBENDO_LONG_ADDRESS && !RECEBENDO_POTENCIA){

        Printf("\r\n--------------- SETADO COMO DESCONFIGURADO !!! ---------------");
        estadoConfiguracao = 0x00;
        setEstadoConfiguracao(estadoConfiguracao);
        MiApp_RemoveConnection(0xFF);
        Reset();
    }else{
        if(RECEBENDO_ENDERECO){
            if(indiceBuffer < ((SHORT_ADDRESS_SIZE*2))){
                shortAddressDestinoASCII[indiceBuffer] = convertASCII_to_HEX(dado);
                indiceBuffer++;
                if(indiceBuffer==4){
                    formatArrayToHex(shortAddressDestinoASCII,SHORT_ADDRESS_SIZE, shortAddressDestinoHex);
                    inverteArray(shortAddressDestinoHex,shortAddressDestinoHexInvertido,SHORT_ADDRESS_SIZE);

                    if(REQUISITANDO_REDE)
                    {
                        MiApp_FlushTx();
                        MiApp_WriteData(0xDC);
                        REQUISITANDO_REDE=FALSE;
                    }else
                    if(DESTRUINDO_REDE){
                        MiApp_FlushTx();
                        MiApp_WriteData(0xDD);
                        DESTRUINDO_REDE=FALSE;
                    }

                    if(MiApp_UnicastAddress(shortAddressDestinoHexInvertido, FALSE, FALSE)){
                        Printf("\r\nMensagem Unicast enviada com sucesso");
                    }
                    else{
                        Printf("\r\nMensagem Unicast n�o enviada");
                    }
                    RECEBENDO_ENDERECO = FALSE;

                }
            }
        }else if(RECEBENDO_INICE){
            if(MiApp_UnicastConnection(convertASCII_to_HEX(dado), FALSE)){
                Printf("\r\nMensagem Unicast enviada com sucesso");
            }
            else{
                Printf("\r\nMensagem Unicast n�o enviada");
            }
            RECEBENDO_INICE = FALSE;

        }else if(RECEBENDO_LONG_ADDRESS){
            if(indiceBuffer < ((MY_ADDRESS_LENGTH*2))){
                longAddressDestinoASCII[indiceBuffer] = convertASCII_to_HEX(dado);
                indiceBuffer++;
            }
            if(indiceBuffer==16){
                formatArrayToHex(longAddressDestinoASCII,MY_ADDRESS_LENGTH, longAddressDestinoHex);
                inverteArray(longAddressDestinoHex,longAddressDestinoHexInvertido,MY_ADDRESS_LENGTH);

                if(MiApp_UnicastAddress(longAddressDestinoHexInvertido, TRUE, FALSE))
                    Printf("\r\nMensagem enviada com sucesso");
                else
                    Printf("\r\nMensagem n�o enviada");
                RECEBENDO_LONG_ADDRESS=FALSE;
            }
        }else if(RECEBENDO_POTENCIA){
            if(indiceBuffer<2){
                atributos_HEX[indiceBuffer] = convertASCII_to_HEX(dado);
                indiceBuffer++;
            }
            if(indiceBuffer==2){
                formatArrayToHex(atributos_HEX , 1, longAddressDestinoHex);
                MiMAC_SetPower(longAddressDestinoHex[0]);
                Printf("\r\n");
                PrintChar(longAddressDestinoHex[0]);
                Printf("\r\n");
                RECEBENDO_POTENCIA = FALSE;
            }
        }else{

        }


    }
}

void trataPacotesVindosDaSerial(BYTE dado){

    BOOL CONDICOES;

    CONDICOES = (!RECEBENDO_ATRIBUTOS_PELA_SERIAL && !RECEBENDO_LONG_ADDRESS_PELA_SERIAL && !RECEBENDO_SOLICITACAO_PELA_SERIAL);
    #if defined(CONFIGURADOR)

    if(dado == 0xAA && CONDICOES){
        ConfiguraRadioParaGRCNovo();
        L3_RADIO = 1;
    }else
    if(dado == 0xAB && CONDICOES){
        ConfiguraRadioParaGRCNaRede();
        L3_RADIO = 1;
    }else
    if(dado == 0xAC && CONDICOES){
        ConfiguradorIniciaComunicacaoGRCNovo();
        CONFIGURANDO_GRC_NOVO = TRUE;
    }else
    if(dado == 0xAD && CONDICOES){
        RECEBENDO_ATRIBUTOS_PELA_SERIAL = TRUE;
        CONFIGURANDO_GRC_NOVO = TRUE;
        indiceBuffer = 0;
    }else
    if(dado == 0xAE && CONDICOES){
        ConfiguradorRequisitaAtributosDeGRCNovo();
    }else
    if(dado == 0xBA && CONDICOES){
        RECEBENDO_LONG_ADDRESS_PELA_SERIAL = TRUE;
        indiceBuffer = 0;
    }else
    if(dado == 0xBB && CONDICOES){
        Printf("\r\n---------- REQUISITANDO ATRIBUTOS DO GRC: ----------");
        ConfiguradorRequisitaAtributosDeGRCNaRede(short_address_GRC_na_rede);
    }else
    if(dado == 0xBC && CONDICOES){
        RECEBENDO_ATRIBUTOS_PELA_SERIAL = TRUE;
        indiceBuffer = 0;
    }else
    if(dado == 0xBD && CONDICOES){
        RECEBENDO_ATRIBUTOS_PELA_SERIAL = TRUE;
        CONFIGURANDO_GRC_NOVO = FALSE;
        indiceBuffer = 0;
    }else
    if(dado == 0xC1 && CONDICOES){
        RECEBENDO_SOLICITACAO_PELA_SERIAL = TRUE;
        indiceBuffer = 0;
         MiApp_FlushTx();

    }else{
        if(RECEBENDO_ATRIBUTOS_PELA_SERIAL){
            atributos_HEX[indiceBuffer] = dado;
            indiceBuffer++;
            if(indiceBuffer >= TOTAL_DATA_PACKET_SIZE){
                Printf("\r\n---------- Atrbutos Recebidos !!! ----------");
                RECEBENDO_ATRIBUTOS_PELA_SERIAL = FALSE;
                Printf("\r\n---------- ENVIANDO ATRIBUTOS PARA O GRC !!! ----------");
                //envia os dados para o GRC para serem salvos
                if(CONFIGURANDO_GRC_NOVO){
                    enviaDados(atributos_HEX, 0xA4);
                }else{
                    enviaDadosNaConfiguracaoGRCNaRede(atributos_HEX, 0xB6, short_address_GRC_na_rede);
                }

                Printf("\r\n---------- ATRIBUTOS ENVIADOS !!! ----------");
            }
        }
        else if(RECEBENDO_LONG_ADDRESS_PELA_SERIAL){
            longAddressAbuscar_HEX[indiceBuffer] = dado;
            indiceBuffer++;
            if(indiceBuffer >= (LONG_ADDRESS_SIZE)){
                Printf("\r\n---------- LONG ADDRESS RECEBIDO !!! ----------");
                RECEBENDO_LONG_ADDRESS_PELA_SERIAL = FALSE;
                Printf("\r\n---------- BUSCANDO GRC COM ESSE LONG ADDRESS !!! ----------");
                Configurador_ProcuraLongAddress(longAddressAbuscar_HEX);
            }
        }
        else if(RECEBENDO_SOLICITACAO_PELA_SERIAL){
            if(indiceBuffer < ((SHORT_ADDRESS_SIZE))){
                shortAddressDestinoHex[indiceBuffer] = dado;
                indiceBuffer++;
            }else if (indiceBuffer>1 && indiceBuffer<5){
                MiApp_WriteData(dado);
                indiceBuffer++;
            }else{
                MiApp_WriteData(0x0D);
                MiApp_WriteData(0x0A);
                inverteArray(shortAddressDestinoHex,shortAddressDestinoHexInvertido,SHORT_ADDRESS_SIZE);
                if(MiApp_UnicastAddress(shortAddressDestinoHexInvertido, FALSE, FALSE)){
                    Printf("\r\nMensagem Unicast enviada com sucesso");
                }
                else{
                    Printf("\r\nMensagem Unicast n�o enviada");
                }
                RECEBENDO_SOLICITACAO_PELA_SERIAL = FALSE;
            }


        }
        else{
//            L2_RADIO ^=1;
//            ProcessMenu(dado);
            trataMenuDeEnvio(dado);
        }
    }

    #else
    trataMenuDeEnvio(dado);
    #endif
}


   void exibe_energia(){
       DWORD_VAL aux, p;
       

       Printf("\r\n Energia medidor: ");
       aux.Val = getLeituraEnergia();
       GRCEnergia = make32(medidorEnergia[3], medidorEnergia[2], medidorEnergia[1], medidorEnergia[0]);
       //GRCEnergia = GRCEnergia + aux.Val;

       p.Val = GRCEnergia;

       
       

       PrintChar(p.v[3]);
       PrintChar(p.v[2]);
       PrintChar(p.v[1]);
       PrintChar(p.v[0]);

       Printf("\r\n Energia comunica��o: ");
       PrintChar(aux.v[3]);
       PrintChar(aux.v[2]);
       PrintChar(aux.v[1]);
       PrintChar(aux.v[0]);


   }