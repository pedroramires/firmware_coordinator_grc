/* 
 * File:   inicio_miwi.h
 * Author: Thiago Serpa
 *
 * Created on 21 de Mar�o de 2014, 11:39
 */

#ifndef INICIO_MIWI_H
#define	INICIO_MIWI_H


void iniciar_Protocolo();

void ler_Conf_Inicial();

void ler_Conf_Inicial2();

void setEstadoConfiguracao(BYTE *estadoConfig);

void getEstadoConfiguracao(BYTE *estadoConfig);

#endif	/* INICIO_MIWI_H */

