/*
 * File:   inicio_miwi.c
 * Author: Thiago Serpa
 *
 * Created on 21 de Mar�o de 2014, 11:37
 */

#include <stdio.h>
#include <stdlib.h>
#include "Console.h"
#include "acessaIdentificadores.h"
#include "../inicio_miwi.h"
#include "MRF24J40.h"
#include "NVM.h"

extern BYTE myLongAddress[MY_ADDRESS_LENGTH];

extern WORD_VAL myPANID;

extern BYTE estadoConfiguracao;



void iniciar_Protocolo(){



//    if(estadoConfiguracao != 0xFF){
//        return();
//    }
//    else
//    {
////        #ifdef(CONFIGURADOR)
////            #ifdef ENABLE_ED_SCAN
////                MiApp_StartConnection(START_CONN_ENERGY_SCN, 9, 0x00000800);
////            #endif
////        #endif
//    }






}


void ler_Conf_Inicial(){
    MIWI_TICK tickDifference;
    MIWI_TICK currentTime;
    MIWI_TICK eventTime;
    BYTE i;
    BYTE novoLongAddress[MY_ADDRESS_LENGTH];
    WORD_VAL pan;
    char str;
    int num;

    eventTime.Val = 0;
    num = 0;
    
    L2_RADIO = 1;//indicar que ele n�o est� conectado a nenhuma rede
    getEstadoConfiguracao(&estadoConfiguracao);
//    PrintChar(estadoConfiguracao);
    getPANIDConfigurado(myPANID.v);
    setPANID(myPANID.v);
    if (estadoConfiguracao==0x00){
        Printf("\r\n\n---------------------    ");
        for(i=0;i< MY_ADDRESS_LENGTH; i++){
            currentTime = MiWi_TickGet();
            tickDifference.Val = MiWi_TickGetDiff(currentTime,eventTime);
            srand(tickDifference.Val);

            num = num+((rand()));
    //        sprintf(&str,"%x",(num%256));
            PrintChar(num%256);
            novoLongAddress[i]=num%256;

        }
//        novoLongAddress[0]=0x0E;
//        novoLongAddress[1]=0x77;
//        novoLongAddress[2]=0x66;
//        novoLongAddress[3]=0x55;
//        novoLongAddress[4]=0x44;
//        novoLongAddress[5]=0x33;
//        novoLongAddress[6]=0x22;
//        novoLongAddress[7]=0x11;
        Printf("     --------------------- \r\n\n ");

        setLongAddress(novoLongAddress);
        getLongAddress(myLongAddress);
        pan.Val = MY_PAN_ID;
//        pan[0]=0x17;
//        pan[1]=0x34;
        setPANIDConfigurado(pan.v);
        setPANID(pan.v);
        return;
    }
    getLongAddress(myLongAddress);
    MiMAC_SetAltAddress(myShortAddress.v, myPANID.v);
    getPANID(myPANID.v);
}

void ler_Conf_Inicial2(){
    L2_RADIO = 1;
    getPANIDConfigurado(myPANID.v);
    getLongAddress(myLongAddress);
}


void getEstadoConfiguracao(BYTE *estadoConfig){
    nvmGetEstadoConfiguracao(estadoConfig);
}


void setEstadoConfiguracao(BYTE *estadoConfig){
    nvmPutEstadoConfiguracao(estadoConfig);
}