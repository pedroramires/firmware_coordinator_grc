 /*
 * File:   MontadorGRC.h
 * Author: Ramires
 *
 * Created on 10 de Fevereiro de 2014, 15:04
 */


//#ifndef MONTADORGRC_H
#define	MONTADORGRC_H

#define CONCENTRADOR_PAN
#define MEDIDOR

#include "Utils.h"

typedef struct PACOTE{
    BYTE   *getId;
    BYTE   getFuncao;
    BYTE   getTamanho;
    BYTE   *getPacoteMontado;

}pacoteGRC;

//typedef struct PACOTE pacoteGRC;




#define REQUISITAR_ID_MEDIDOR              21
#define REQUISITAR_VERSAO_FIRMWARE         22
#define REQUISITAR_INFO_MEDIDOR            23
#define REQUISITAR_DATA_HORA_MEDIDOR       24
#define REQUISITAR_ENERGIA_MEDIDOR         25
#define GRAVAR_INFO_MEDIDOR                26
#define GRAVAR_DATA_HORA_MEDIDOR           27
#define REQUISITAR_DADOS_MEDICAO_UC        42
#define FUNCAO_REQ_ARMA_RELE               43
#define FUNCAO_REQ_DESARMA_RELE            44
#define FUNCAO_ZERA_ENERGIA_MEDIDOR        132
#define FUNCAO_REQ_TOTAL_DATA              133

void montaPacoteRespostaRequisitaDadosDeMedicaoUC(int32 idConcentrador,BYTE *idMedidor, BYTE *tensaoFase, BYTE *correnteFase, BYTE *potenciaNominal, pacoteGRC *pacote);

void montaPacoteRespostaRequisitaVersaoFirmware(int32 idConcentrador,BYTE *idMedidor, BYTE *versaoFirmware,pacoteGRC *pacote);

void montaPacoteRespostaRequisitaInfoMedidor(int32 idConcentrador, BYTE *idMedidor,BYTE *numeroDaCasa, BYTE *unidadeConsumidora, BYTE* latitude, BYTE* longitude, pacoteGRC *pacote);

void montaPacoteRespostaRequisitaData_Hora(int32 idConcentrador,BYTE *idMedidor,  BYTE *dataHora, pacoteGRC *pacote );

void montaPacoteRespostaRequisitaEnergiaMedidor(int32 idConcentrador,BYTE *idMedidor,BYTE *horaDoAferimento, BYTE *energia, pacoteGRC *pacote );

void montaPacoteRespostaRequisitaInfoMedidor(int32 idConcentrador, BYTE *idMedidor,BYTE *numeroDaCasa, BYTE *unidadeConsumidora, BYTE* latitude, BYTE* longitude, pacoteGRC *pacote);

void montaPacoteRespostaGravaData_HoraMedidor(int32 idConcentrador,BYTE *idMedidor, BYTE retorno, pacoteGRC *pacote );

void montaPacoteRespostaArmaRele(int32 idConcentrador,BYTE *idMedidor, BYTE estadoRele, pacoteGRC *pacote);

void montaPacoteRespostaDesarmaRele(int32 idConcentrador,BYTE *idMedidor, BYTE estadoRele, pacoteGRC *pacote);

void montaPacoteRespostaRequisitaTotalData(int32 idConcentrador, BYTE* idMedidor,BYTE* tensaoFase, BYTE* correnteFase,BYTE* potenciaAtiva,BYTE* energiaMedidor,BYTE* dataHoraMedidor, BYTE* fatorPotenciaMedidor,pacoteGRC *pacote);






//#endif	/* MONTADORGRC_H */

