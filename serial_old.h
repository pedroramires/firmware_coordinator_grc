
#ifndef __SERIAL_H
#define __SERIAL_H

#include "../Console.h"

#define         FALSO                 0
#define         VERDADEIRO            1

#define FRQ_CLOCK   48000000.0

void configura_uart(void);
void trata_recepcao_uart (void);
void envia_byte_uart(char _byte);
char recebe_byte_uart(void);

void insere_dado_bufferRx(void);
void retira_dado_bufferTx(void);

void PHY_envia_byte(unsigned char _dado);
unsigned char PHY_recebe_byte(void);
void PHY_habilita_int_Tx (unsigned char _habilitado);


void serial_init(void);
unsigned char serial_retira_dado_bufferRx(void);
unsigned char serial_insere_dados_bufferTx(char *_dados,unsigned char _tamanho);
//unsigned char serial_insere_dados_bufferTx_rom(rom const char *_dados,unsigned char _tamanho);
unsigned char serial_tem_dado_bufferTx(void);
unsigned char serial_tem_dado_bufferRx(void);



#endif