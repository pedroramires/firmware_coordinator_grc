/********************************************************************
* FileName:		HardwareProfile.h
* Dependencies:    
* Processor:	PIC18, PIC24F, PIC24H, dsPIC30, dsPIC33
*               tested with 18F4620, dsPIC33FJ256GP710	
* Hardware:		PICDEM Z, Explorer 16
* Complier:     Microchip C18 v3.04 or higher
*				Microchip C30 v2.03 or higher		
* Company:		Microchip Technology, Inc.
*
* Copyright and Disclaimer Notice
*
* Copyright � 2007-2008 Microchip Technology Inc.  All rights reserved.
*
* Microchip licenses to you the right to use, modify, copy and distribute 
* Software only when embedded on a Microchip microcontroller or digital 
* signal controller and used with a Microchip radio frequency transceiver, 
* which are integrated into your product or third party product (pursuant 
* to the terms in the accompanying license agreement).   
*
* You should refer to the license agreement accompanying this Software for 
* additional information regarding your rights and obligations.
*
* SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY 
* KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY 
* WARRANTY OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A 
* PARTICULAR PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE 
* LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, 
* CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY 
* DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO 
* ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, 
* LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, 
* TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT 
* NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*
*********************************************************************
* File Description:
*
*  This file defines functions used for demo board hardware
*
* Change History:
*  Rev   Date         Author    Description
*  1.0   2/15/2009    yfy       Initial revision
*  2.0   4/15/2009    yfy       MiMAC and MiApp revision
********************************************************************/

#ifndef _HARDWARE_PROFILE_H
    #define _HARDWARE_PROFILE_H

    #include <stdlib.h>
    #include "GenericTypeDefs.h"
    #include "ConfigApp.h"
    #include "../ConfiguracaoViaRadio.h"


    

    #define PICDEMZ

//    #define TESTE

//#define VERIFY_WRITE
//    #define GRCS
    #define GRC_NOVA
//    #define BOIA

//    #define CONFIGURADOR
//    #define CONCENTRADOR


    //Se concentrador = 1, grc =0
//    static BYTE CONCENTRADOR = 0X00;
//    #define LONGADDRESS0 0X02
//    #define INFOPEER 0X02

    #define EUI_7 0x11
    #define EUI_6 0x22
    #define EUI_5 0x33
    #define EUI_4 0x44
    #define EUI_3 0x55
    #define EUI_2 0x66
    #define EUI_1 0x77
    #define EUI_0 0x08




//   

   
    // there are three ways to use NVM to store data: External EPROM, Data EEPROM and 
    // programming space, with following definitions:
    //  #define USE_EXTERNAL_EEPROM
    //  #define USE_DATA_EEPROM
//      #define USE_PROGRAMMING_SPACE
    // Each demo board has defined the method of using NVM, as
    // required by Network Freezer feature.
    
    #if defined(PICDEMZ)
        
        #if defined(GRC_NOVA)
               #define CLOCK_FREQ          48000000
               #define USE_PROGRAMMING_SPACE
        #else
//                #define USE_DATA_EEPROM
                #define USE_PROGRAMMING_SPACE
                #define CLOCK_FREQ          10000000
        #endif
        
        

//atuador grc cs wake reset     intpin  sdi  sck sdo   RFIF                 RFIE
//pic          c0   c1    c2        b1  =       =  =    INTCON3bits.INT1IF  INTCON3bits.INT1IE


//boia         cs wake reset   intpin   sdi  sck  sdo   RFIF                 RFIE
//pic           c0   c2    c1       b0   =   =     =    INTCONbits.INT0IF  INTCONbits.INT0IE


//concentrador     cs   wake  reset intpin  sdi sck sdo   RFIF                 RFIE
//pic              c0    c1   c2    b1       =    =  =    INTCON3bits.INT1IF  INTCON3bits.INT1IE

        #if defined(GRCS)
                // Transceiver Configuration concentador e atuador
            #define RFIF                INTCON3bits.INT1IF
            #define RFIE                INTCON3bits.INT1IE
            #define PHY_CS              LATCbits.LATC0
            #define PHY_CS_TRIS         TRISCbits.TRISC0
            #define PHY_RESETn          LATCbits.LATC2
            #define PHY_RESETn_TRIS     TRISCbits.TRISC2
            #define PHY_WAKE            LATCbits.LATC1
            #define PHY_WAKE_TRIS       TRISCbits.TRISC1
            #define RF_INT_PIN          PORTBbits.RB1
            #define RF_INT_TRIS         TRISBbits.TRISB1

             #define L2_RADIO               LATBbits.LATB4
             #define L1_RADIO               LATAbits.LATA5
             #define L2_RADIO_TRIS          TRISBbits.TRISB4
             #define L1_RADIO_TRIS          TRISAbits.TRISA5

            #define PUSH_BUTTON_1       PORTAbits.RA0
            #define PUSH_BUTTON_2       PORTAbits.RA1
            #define BUTTON_1_TRIS       TRISAbits.TRISA0
            #define BUTTON_2_TRIS       TRISAbits.TRISA1

            #define SPI_SDI             PORTCbits.RC4
            #define SDI_TRIS            TRISCbits.TRISC4
            #define SPI_SDO             LATCbits.LATC5
            #define SDO_TRIS            TRISCbits.TRISC5
            #define SPI_SCK             LATCbits.LATC3
            #define SCK_TRIS            TRISCbits.TRISC3
        #endif

        #if defined(GRC_NOVA)
                  // Transceiver Configuration GRC_NOVA
            #define RFIF                INTCONbits.INT0IF
            #define RFIE                INTCONbits.INT0IE
            #define PHY_CS              LATBbits.LATB3
            #define PHY_CS_TRIS         TRISBbits.TRISB3
            #define PHY_RESETn          LATBbits.LATB2
            #define PHY_RESETn_TRIS     TRISBbits.TRISB2



            #define PHY_WAKE            LATBbits.LATB1
            #define PHY_WAKE_TRIS       TRISBbits.TRISB1
            #define RF_INT_PIN          PORTBbits.RB0
            #define RF_INT_TRIS         TRISBbits.TRISB0


            #define L2_RADIO               LATBbits.LATB4
            #define L1_RADIO               LATBbits.LATB5
            #define L3_RADIO               LATAbits.LATA5
            #define L2_RADIO_TRIS          TRISBbits.TRISB4
            #define L1_RADIO_TRIS          TRISBbits.TRISB5
            #define L3_RADIO_TRIS          TRISAbits.TRISA5
            #define DISJUNTOR_ON        LATAbits.LATA2
            #define DISJUNTOR_OFF       LATAbits.LATA3
            #define DISJUNTOR_ON_TRIS       TRISAbits.TRISA2
            #define DISJUNTOR_OFF_TRIS       TRISAbits.TRISA3



            #define SPI_SDI             PORTCbits.RC4
            #define SDI_TRIS            TRISCbits.TRISC4
            #define SPI_SDO             LATCbits.LATC5
            #define SDO_TRIS            TRISCbits.TRISC5
            #define SPI_SCK             LATCbits.LATC3
            #define SCK_TRIS            TRISCbits.TRISC3

            #define MCRL_MEDICAO         PORTCbits.RC2
            #define MCRL_MEDICAO_TRIS    TRISCbits.TRISC2

        #endif

        #if defined(BOIA)
                // Transceiver Configuration boia
            #define RFIF                INTCONbits.INT0IF
            #define RFIE                INTCONbits.INT0IE
            #define PHY_CS              LATCbits.LATC0
            #define PHY_CS_TRIS         TRISCbits.TRISC0
            #define PHY_RESETn          LATCbits.LATC1
            #define PHY_RESETn_TRIS     TRISCbits.TRISC1
            #define PHY_WAKE            LATCbits.LATC2
            #define PHY_WAKE_TRIS       TRISCbits.TRISC2
            #define RF_INT_PIN          PORTBbits.RB0
            #define RF_INT_TRIS         TRISBbits.TRISB0


            #define L2_RADIO               LATBbits.LATB5
            #define L1_RADIO               LATBbits.LATB6
            #define L2_RADIO_TRIS          TRISBbits.TRISB5
            #define L1_RADIO_TRIS          TRISBbits.TRISB6
            #define L3_RADIO               LATBbits.LATB4
            #define LED_4               LATBbits.LATB1
            #define L3_RADIO_TRIS          TRISAbits.TRISB4
            #define LED_4_TRIS          TRISAbits.TRISB1
            #define PUSH_BUTTON_1       PORTAbits.RA2
            #define PUSH_BUTTON_2       PORTAbits.RA3
            #define PUSH_BUTTON_1_TRIS  TRISAbits.TRISA2
            #define PUSH_BUTTON_2_TRIS  TRISAbits.TRISA3

            #define BUTTON_1            PORTBbits.RB2
            #define BUTTON_2            PORTBbits.RB3
            #define BUTTON_1_TRIS       TRISBbits.TRISB2
            #define BUTTON_2_TRIS       TRISBbits.TRISB3

            #define SPI_SDI             PORTCbits.RC4
            #define SDI_TRIS            TRISCbits.TRISC4
            #define SPI_SDO             LATCbits.LATC5
            #define SDO_TRIS            TRISCbits.TRISC5
            #define SPI_SCK             LATCbits.LATC3
            #define SCK_TRIS            TRISCbits.TRISC3

        #endif


//

//

//    
      
      
        
        #define TMRL                TMR0L
    #endif        

    // Following definition is for delay functionality
    #if defined(__18CXX)
        #define GetInstructionClock()	(CLOCK_FREQ/4)
    #endif
   

    BYTE ButtonPressed(void);
    void BoardInit(void);

    void LCDDisplay(char *, BYTE, BOOL);
    void LCDTRXCount(BYTE, BYTE);

    
#endif