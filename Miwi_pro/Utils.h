/* 
 * File:   Utils.h
 * Author: Ramires
 *
 * Created on 11 de Fevereiro de 2014, 09:04
 */

#ifndef UTILS_H
#define	UTILS_H

#include "GenericTypeDefs.h"
#include "../globais.h"

union Data2
{
   int16 numero;
   BYTE toArrayBytes[2];
};

union Data3
{
   int32 numero;
   BYTE toArrayBytes[3];
};

union Data4
{
   int32 numero;
   BYTE toArrayBytes[4];
};




typedef union Data2 conversor2;
typedef union Data4 conversor4;
typedef union Data3 conversor3;


#endif	/* UTILS_H */

