#include "acessaIdentificadores.h"
#include "Console.h"

BYTE getFlagEmConfiguracao(void)
{
    BYTE array[1];
    nvmGetEmConfiguracao(array);
    return array[0];
}

void setFlagEmConfiguracao(BYTE val)
{
    BYTE array[1] = {val};
    nvmPutEmConfiguracao(array);
}

BYTE getType(void)
{
    BYTE typeArray[1];
    nvmGetMiwiProType(typeArray);
    return typeArray[0];
}

void setType(BYTE val)
{
    BYTE typeArray[1] = {val};
    nvmPutMiwiProType(typeArray);
}

BYTE getInfoPeer(void)
{
    BYTE array[1];
    nvmGetAdditionalNodeAddress(array);
    return array[0];
}

void setInfoPeer(BYTE val)
{
    BYTE array[1] = {val};
    nvmPutAdditionalNodeAddress(array);
}

void getLongAddress(BYTE *longAddress)
{
    nvmGetMyLongAddress(longAddress);
}

void setLongAddress(BYTE *longAddress)
{
    nvmPutMyLongAddress(longAddress);
}

void getUC(BYTE *uc)
{
    nvmGetUnidadeConsumidora(uc);
}

void setUC(BYTE *uc)
{
    nvmPutUnidadeConsumidora(uc);
}

void getShortAddress(BYTE *shortAddress)
{
    nvmGetMyShortAddress(shortAddress);
}

void setShortAddress(BYTE *shortAddress)
{
    nvmPutMyShortAddress(shortAddress);
}

void getPANID(BYTE *pan)
{
    nvmGetMyPANID(pan);
}

void setPANID(BYTE *pan)
{
    nvmPutMyPANID(pan);
}

void getPANIDConfigurado(BYTE *pan)
{
    nvmGetMyPANIDConfigurado(pan);
}

void setPANIDConfigurado(BYTE *pan)
{
    nvmPutMyPANIDConfigurado(pan);
}

void setChannel(BYTE *channel)
{
    nvmPutMyChannelMap(channel);
}

void getChannel(BYTE *channel)
{
    nvmGetMyChannelMap(channel);
}
BOOL getFlagGRCNovo()
{
    BYTE array[1];
    BOOL flag = FALSE;
    nvmGetConfigurarNovoGRC(array);

    if(array[0] == 0x01){
        flag = TRUE;
    }

    return flag;
}

void setFlagGRCNovo(BYTE flag)
{
    BYTE typeArray[1] = {flag};
    nvmPutConfigurarNovoGRC(typeArray);
}

DWORD getLeituraEnergia()
{
    DWORD_VAL energia;
    nvmGetEnergia(&energia.v[0]);
    return energia.Val;
}

void setLeituraEnergia(BYTE* energia)
{
    nvmPutEnergia(energia);
}


//WORD getLeituraCorrente()
//{
//    WORD corrente;
//    nvmGetCorrenteFaseMedidor(corrente);
//    return corrente;
//}
//
void setLeituraCorrente(BYTE *corrente)
{
    nvmPutCorrenteFaseMedidor(corrente);
}
//
//WORD getLeituraTensao()
//{
//    WORD tensao;
//    nvmGetTensaoFaseMedidor(tensao);
//    return tensao;
//}
//
void setLeituraTensao(BYTE *tensao)
{
    nvmPutTensaoFaseMedidor(tensao);
}
//
//DWORD getLeituraPotenciaAtiva()
//{
//    DWORD potenciaAtiva;
//    nvmGetPotenciaNominal(potenciaAtiva);
//    return potenciaAtiva;
//}
//
void setLeituraPotenciaAtiva(BYTE* potenciaAtiva)
{
    nvmPutPotenciaAtiva(potenciaAtiva);

}
//
//BYTE * getLeituraDataHoraMedidor()
//{
//    BYTE dataHoraMedidor[6];
//    nvmGetDataHoraMedidor(dataHoraMedidor);
//    return dataHoraMedidor;
//}
//
void setLeituraDataHora(BYTE *dataHoraMedidor)
{
    nvmPutDataHoraMedidor(dataHoraMedidor);
}
