#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/MiWi_PRO_Test_Interface_-_PIC18_-_C18.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/MiWi_PRO_Test_Interface_-_PIC18_-_C18.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../HardwareProfile.c "../MiWi PRO Test Interface.c" ../../ConfiguracaoViaRadio.c ../../medidorGRC.c ../../serial_old.c ../acessaIdentificadores.c ../../NVM.c ../../TratadorDePacotes.c ../../InfoMedidor.c ../../inicio_miwi.c ../../ConfiguracaoViaSerial.c ../../MontadorDePacotesGRC.c ../../Microchip/Common/TimeDelay.c ../../Microchip/WirelessProtocols/LCDBlocking.c ../../Microchip/WirelessProtocols/MiWiPRO/MiWiPRO.c ../../Microchip/WirelessProtocols/SymbolTime.c ../../Microchip/WirelessProtocols/P2P/P2P.c ../../Microchip/WirelessProtocols/MSPI.c ../../Microchip/WirelessProtocols/MiWi/MiWi.c ../../Microchip/WirelessProtocols/Console.c ../../Microchip/Transceivers/crc.c ../../Microchip/Transceivers/security.c ../../Microchip/Transceivers/MRF24J40/MRF24J40.c ../../Microchip/Transceivers/MRF49XA/MRF49XA.c ../../Microchip/Transceivers/MRF89XA/MRF89XA.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1472/HardwareProfile.o "${OBJECTDIR}/_ext/1472/MiWi PRO Test Interface.o" ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaRadio.o ${OBJECTDIR}/_ext/43898991/medidorGRC.o ${OBJECTDIR}/_ext/43898991/serial_old.o ${OBJECTDIR}/_ext/1472/acessaIdentificadores.o ${OBJECTDIR}/_ext/43898991/NVM.o ${OBJECTDIR}/_ext/43898991/TratadorDePacotes.o ${OBJECTDIR}/_ext/43898991/InfoMedidor.o ${OBJECTDIR}/_ext/43898991/inicio_miwi.o ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaSerial.o ${OBJECTDIR}/_ext/43898991/MontadorDePacotesGRC.o ${OBJECTDIR}/_ext/385479254/TimeDelay.o ${OBJECTDIR}/_ext/293088702/LCDBlocking.o ${OBJECTDIR}/_ext/544971058/MiWiPRO.o ${OBJECTDIR}/_ext/293088702/SymbolTime.o ${OBJECTDIR}/_ext/438677213/P2P.o ${OBJECTDIR}/_ext/293088702/MSPI.o ${OBJECTDIR}/_ext/714055519/MiWi.o ${OBJECTDIR}/_ext/293088702/Console.o ${OBJECTDIR}/_ext/860796210/crc.o ${OBJECTDIR}/_ext/860796210/security.o ${OBJECTDIR}/_ext/1684968892/MRF24J40.o ${OBJECTDIR}/_ext/776994990/MRF49XA.o ${OBJECTDIR}/_ext/777114154/MRF89XA.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1472/HardwareProfile.o.d "${OBJECTDIR}/_ext/1472/MiWi PRO Test Interface.o.d" ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaRadio.o.d ${OBJECTDIR}/_ext/43898991/medidorGRC.o.d ${OBJECTDIR}/_ext/43898991/serial_old.o.d ${OBJECTDIR}/_ext/1472/acessaIdentificadores.o.d ${OBJECTDIR}/_ext/43898991/NVM.o.d ${OBJECTDIR}/_ext/43898991/TratadorDePacotes.o.d ${OBJECTDIR}/_ext/43898991/InfoMedidor.o.d ${OBJECTDIR}/_ext/43898991/inicio_miwi.o.d ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaSerial.o.d ${OBJECTDIR}/_ext/43898991/MontadorDePacotesGRC.o.d ${OBJECTDIR}/_ext/385479254/TimeDelay.o.d ${OBJECTDIR}/_ext/293088702/LCDBlocking.o.d ${OBJECTDIR}/_ext/544971058/MiWiPRO.o.d ${OBJECTDIR}/_ext/293088702/SymbolTime.o.d ${OBJECTDIR}/_ext/438677213/P2P.o.d ${OBJECTDIR}/_ext/293088702/MSPI.o.d ${OBJECTDIR}/_ext/714055519/MiWi.o.d ${OBJECTDIR}/_ext/293088702/Console.o.d ${OBJECTDIR}/_ext/860796210/crc.o.d ${OBJECTDIR}/_ext/860796210/security.o.d ${OBJECTDIR}/_ext/1684968892/MRF24J40.o.d ${OBJECTDIR}/_ext/776994990/MRF49XA.o.d ${OBJECTDIR}/_ext/777114154/MRF89XA.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1472/HardwareProfile.o ${OBJECTDIR}/_ext/1472/MiWi\ PRO\ Test\ Interface.o ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaRadio.o ${OBJECTDIR}/_ext/43898991/medidorGRC.o ${OBJECTDIR}/_ext/43898991/serial_old.o ${OBJECTDIR}/_ext/1472/acessaIdentificadores.o ${OBJECTDIR}/_ext/43898991/NVM.o ${OBJECTDIR}/_ext/43898991/TratadorDePacotes.o ${OBJECTDIR}/_ext/43898991/InfoMedidor.o ${OBJECTDIR}/_ext/43898991/inicio_miwi.o ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaSerial.o ${OBJECTDIR}/_ext/43898991/MontadorDePacotesGRC.o ${OBJECTDIR}/_ext/385479254/TimeDelay.o ${OBJECTDIR}/_ext/293088702/LCDBlocking.o ${OBJECTDIR}/_ext/544971058/MiWiPRO.o ${OBJECTDIR}/_ext/293088702/SymbolTime.o ${OBJECTDIR}/_ext/438677213/P2P.o ${OBJECTDIR}/_ext/293088702/MSPI.o ${OBJECTDIR}/_ext/714055519/MiWi.o ${OBJECTDIR}/_ext/293088702/Console.o ${OBJECTDIR}/_ext/860796210/crc.o ${OBJECTDIR}/_ext/860796210/security.o ${OBJECTDIR}/_ext/1684968892/MRF24J40.o ${OBJECTDIR}/_ext/776994990/MRF49XA.o ${OBJECTDIR}/_ext/777114154/MRF89XA.o

# Source Files
SOURCEFILES=../HardwareProfile.c ../MiWi PRO Test Interface.c ../../ConfiguracaoViaRadio.c ../../medidorGRC.c ../../serial_old.c ../acessaIdentificadores.c ../../NVM.c ../../TratadorDePacotes.c ../../InfoMedidor.c ../../inicio_miwi.c ../../ConfiguracaoViaSerial.c ../../MontadorDePacotesGRC.c ../../Microchip/Common/TimeDelay.c ../../Microchip/WirelessProtocols/LCDBlocking.c ../../Microchip/WirelessProtocols/MiWiPRO/MiWiPRO.c ../../Microchip/WirelessProtocols/SymbolTime.c ../../Microchip/WirelessProtocols/P2P/P2P.c ../../Microchip/WirelessProtocols/MSPI.c ../../Microchip/WirelessProtocols/MiWi/MiWi.c ../../Microchip/WirelessProtocols/Console.c ../../Microchip/Transceivers/crc.c ../../Microchip/Transceivers/security.c ../../Microchip/Transceivers/MRF24J40/MRF24J40.c ../../Microchip/Transceivers/MRF49XA/MRF49XA.c ../../Microchip/Transceivers/MRF89XA/MRF89XA.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/MiWi_PRO_Test_Interface_-_PIC18_-_C18.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F27J13
MP_PROCESSOR_OPTION_LD=18f27j13
MP_LINKER_DEBUG_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1472/HardwareProfile.o: ../HardwareProfile.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/HardwareProfile.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/HardwareProfile.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/HardwareProfile.o   ../HardwareProfile.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/HardwareProfile.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/HardwareProfile.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/MiWi\ PRO\ Test\ Interface.o: ../MiWi\ PRO\ Test\ Interface.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} "${OBJECTDIR}/_ext/1472/MiWi PRO Test Interface.o".d 
	@${RM} "${OBJECTDIR}/_ext/1472/MiWi PRO Test Interface.o" 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo "${OBJECTDIR}/_ext/1472/MiWi PRO Test Interface.o"   "../MiWi PRO Test Interface.c" 
	@${DEP_GEN} -d "${OBJECTDIR}/_ext/1472/MiWi PRO Test Interface.o" 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/MiWi PRO Test Interface.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/ConfiguracaoViaRadio.o: ../../ConfiguracaoViaRadio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaRadio.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaRadio.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaRadio.o   ../../ConfiguracaoViaRadio.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaRadio.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/ConfiguracaoViaRadio.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/medidorGRC.o: ../../medidorGRC.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/medidorGRC.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/medidorGRC.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/medidorGRC.o   ../../medidorGRC.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/medidorGRC.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/medidorGRC.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/serial_old.o: ../../serial_old.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/serial_old.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/serial_old.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/serial_old.o   ../../serial_old.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/serial_old.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/serial_old.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/acessaIdentificadores.o: ../acessaIdentificadores.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/acessaIdentificadores.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/acessaIdentificadores.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/acessaIdentificadores.o   ../acessaIdentificadores.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/acessaIdentificadores.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/acessaIdentificadores.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/NVM.o: ../../NVM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/NVM.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/NVM.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/NVM.o   ../../NVM.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/NVM.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/NVM.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/TratadorDePacotes.o: ../../TratadorDePacotes.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/TratadorDePacotes.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/TratadorDePacotes.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/TratadorDePacotes.o   ../../TratadorDePacotes.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/TratadorDePacotes.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/TratadorDePacotes.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/InfoMedidor.o: ../../InfoMedidor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/InfoMedidor.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/InfoMedidor.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/InfoMedidor.o   ../../InfoMedidor.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/InfoMedidor.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/InfoMedidor.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/inicio_miwi.o: ../../inicio_miwi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/inicio_miwi.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/inicio_miwi.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/inicio_miwi.o   ../../inicio_miwi.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/inicio_miwi.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/inicio_miwi.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/ConfiguracaoViaSerial.o: ../../ConfiguracaoViaSerial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaSerial.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaSerial.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaSerial.o   ../../ConfiguracaoViaSerial.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaSerial.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/ConfiguracaoViaSerial.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/MontadorDePacotesGRC.o: ../../MontadorDePacotesGRC.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/MontadorDePacotesGRC.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/MontadorDePacotesGRC.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/MontadorDePacotesGRC.o   ../../MontadorDePacotesGRC.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/MontadorDePacotesGRC.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/MontadorDePacotesGRC.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/385479254/TimeDelay.o: ../../Microchip/Common/TimeDelay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/385479254" 
	@${RM} ${OBJECTDIR}/_ext/385479254/TimeDelay.o.d 
	@${RM} ${OBJECTDIR}/_ext/385479254/TimeDelay.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/385479254/TimeDelay.o   ../../Microchip/Common/TimeDelay.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/385479254/TimeDelay.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/385479254/TimeDelay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/293088702/LCDBlocking.o: ../../Microchip/WirelessProtocols/LCDBlocking.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/293088702" 
	@${RM} ${OBJECTDIR}/_ext/293088702/LCDBlocking.o.d 
	@${RM} ${OBJECTDIR}/_ext/293088702/LCDBlocking.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/293088702/LCDBlocking.o   ../../Microchip/WirelessProtocols/LCDBlocking.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/293088702/LCDBlocking.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/293088702/LCDBlocking.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/544971058/MiWiPRO.o: ../../Microchip/WirelessProtocols/MiWiPRO/MiWiPRO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/544971058" 
	@${RM} ${OBJECTDIR}/_ext/544971058/MiWiPRO.o.d 
	@${RM} ${OBJECTDIR}/_ext/544971058/MiWiPRO.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/544971058/MiWiPRO.o   ../../Microchip/WirelessProtocols/MiWiPRO/MiWiPRO.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/544971058/MiWiPRO.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/544971058/MiWiPRO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/293088702/SymbolTime.o: ../../Microchip/WirelessProtocols/SymbolTime.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/293088702" 
	@${RM} ${OBJECTDIR}/_ext/293088702/SymbolTime.o.d 
	@${RM} ${OBJECTDIR}/_ext/293088702/SymbolTime.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/293088702/SymbolTime.o   ../../Microchip/WirelessProtocols/SymbolTime.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/293088702/SymbolTime.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/293088702/SymbolTime.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/438677213/P2P.o: ../../Microchip/WirelessProtocols/P2P/P2P.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/438677213" 
	@${RM} ${OBJECTDIR}/_ext/438677213/P2P.o.d 
	@${RM} ${OBJECTDIR}/_ext/438677213/P2P.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/438677213/P2P.o   ../../Microchip/WirelessProtocols/P2P/P2P.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/438677213/P2P.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/438677213/P2P.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/293088702/MSPI.o: ../../Microchip/WirelessProtocols/MSPI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/293088702" 
	@${RM} ${OBJECTDIR}/_ext/293088702/MSPI.o.d 
	@${RM} ${OBJECTDIR}/_ext/293088702/MSPI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/293088702/MSPI.o   ../../Microchip/WirelessProtocols/MSPI.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/293088702/MSPI.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/293088702/MSPI.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/714055519/MiWi.o: ../../Microchip/WirelessProtocols/MiWi/MiWi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/714055519" 
	@${RM} ${OBJECTDIR}/_ext/714055519/MiWi.o.d 
	@${RM} ${OBJECTDIR}/_ext/714055519/MiWi.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/714055519/MiWi.o   ../../Microchip/WirelessProtocols/MiWi/MiWi.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/714055519/MiWi.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/714055519/MiWi.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/293088702/Console.o: ../../Microchip/WirelessProtocols/Console.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/293088702" 
	@${RM} ${OBJECTDIR}/_ext/293088702/Console.o.d 
	@${RM} ${OBJECTDIR}/_ext/293088702/Console.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/293088702/Console.o   ../../Microchip/WirelessProtocols/Console.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/293088702/Console.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/293088702/Console.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/860796210/crc.o: ../../Microchip/Transceivers/crc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/860796210" 
	@${RM} ${OBJECTDIR}/_ext/860796210/crc.o.d 
	@${RM} ${OBJECTDIR}/_ext/860796210/crc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/860796210/crc.o   ../../Microchip/Transceivers/crc.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/860796210/crc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/860796210/crc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/860796210/security.o: ../../Microchip/Transceivers/security.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/860796210" 
	@${RM} ${OBJECTDIR}/_ext/860796210/security.o.d 
	@${RM} ${OBJECTDIR}/_ext/860796210/security.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/860796210/security.o   ../../Microchip/Transceivers/security.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/860796210/security.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/860796210/security.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1684968892/MRF24J40.o: ../../Microchip/Transceivers/MRF24J40/MRF24J40.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1684968892" 
	@${RM} ${OBJECTDIR}/_ext/1684968892/MRF24J40.o.d 
	@${RM} ${OBJECTDIR}/_ext/1684968892/MRF24J40.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1684968892/MRF24J40.o   ../../Microchip/Transceivers/MRF24J40/MRF24J40.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1684968892/MRF24J40.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1684968892/MRF24J40.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/776994990/MRF49XA.o: ../../Microchip/Transceivers/MRF49XA/MRF49XA.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/776994990" 
	@${RM} ${OBJECTDIR}/_ext/776994990/MRF49XA.o.d 
	@${RM} ${OBJECTDIR}/_ext/776994990/MRF49XA.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/776994990/MRF49XA.o   ../../Microchip/Transceivers/MRF49XA/MRF49XA.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/776994990/MRF49XA.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/776994990/MRF49XA.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/777114154/MRF89XA.o: ../../Microchip/Transceivers/MRF89XA/MRF89XA.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/777114154" 
	@${RM} ${OBJECTDIR}/_ext/777114154/MRF89XA.o.d 
	@${RM} ${OBJECTDIR}/_ext/777114154/MRF89XA.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/777114154/MRF89XA.o   ../../Microchip/Transceivers/MRF89XA/MRF89XA.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/777114154/MRF89XA.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/777114154/MRF89XA.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
else
${OBJECTDIR}/_ext/1472/HardwareProfile.o: ../HardwareProfile.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/HardwareProfile.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/HardwareProfile.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/HardwareProfile.o   ../HardwareProfile.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/HardwareProfile.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/HardwareProfile.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/MiWi\ PRO\ Test\ Interface.o: ../MiWi\ PRO\ Test\ Interface.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} "${OBJECTDIR}/_ext/1472/MiWi PRO Test Interface.o".d 
	@${RM} "${OBJECTDIR}/_ext/1472/MiWi PRO Test Interface.o" 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo "${OBJECTDIR}/_ext/1472/MiWi PRO Test Interface.o"   "../MiWi PRO Test Interface.c" 
	@${DEP_GEN} -d "${OBJECTDIR}/_ext/1472/MiWi PRO Test Interface.o" 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/MiWi PRO Test Interface.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/ConfiguracaoViaRadio.o: ../../ConfiguracaoViaRadio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaRadio.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaRadio.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaRadio.o   ../../ConfiguracaoViaRadio.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaRadio.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/ConfiguracaoViaRadio.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/medidorGRC.o: ../../medidorGRC.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/medidorGRC.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/medidorGRC.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/medidorGRC.o   ../../medidorGRC.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/medidorGRC.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/medidorGRC.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/serial_old.o: ../../serial_old.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/serial_old.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/serial_old.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/serial_old.o   ../../serial_old.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/serial_old.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/serial_old.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/acessaIdentificadores.o: ../acessaIdentificadores.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/acessaIdentificadores.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/acessaIdentificadores.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1472/acessaIdentificadores.o   ../acessaIdentificadores.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/acessaIdentificadores.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/acessaIdentificadores.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/NVM.o: ../../NVM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/NVM.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/NVM.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/NVM.o   ../../NVM.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/NVM.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/NVM.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/TratadorDePacotes.o: ../../TratadorDePacotes.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/TratadorDePacotes.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/TratadorDePacotes.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/TratadorDePacotes.o   ../../TratadorDePacotes.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/TratadorDePacotes.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/TratadorDePacotes.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/InfoMedidor.o: ../../InfoMedidor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/InfoMedidor.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/InfoMedidor.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/InfoMedidor.o   ../../InfoMedidor.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/InfoMedidor.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/InfoMedidor.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/inicio_miwi.o: ../../inicio_miwi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/inicio_miwi.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/inicio_miwi.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/inicio_miwi.o   ../../inicio_miwi.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/inicio_miwi.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/inicio_miwi.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/ConfiguracaoViaSerial.o: ../../ConfiguracaoViaSerial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaSerial.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaSerial.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaSerial.o   ../../ConfiguracaoViaSerial.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/ConfiguracaoViaSerial.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/ConfiguracaoViaSerial.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/43898991/MontadorDePacotesGRC.o: ../../MontadorDePacotesGRC.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/MontadorDePacotesGRC.o.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/MontadorDePacotesGRC.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/43898991/MontadorDePacotesGRC.o   ../../MontadorDePacotesGRC.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/43898991/MontadorDePacotesGRC.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/43898991/MontadorDePacotesGRC.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/385479254/TimeDelay.o: ../../Microchip/Common/TimeDelay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/385479254" 
	@${RM} ${OBJECTDIR}/_ext/385479254/TimeDelay.o.d 
	@${RM} ${OBJECTDIR}/_ext/385479254/TimeDelay.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/385479254/TimeDelay.o   ../../Microchip/Common/TimeDelay.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/385479254/TimeDelay.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/385479254/TimeDelay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/293088702/LCDBlocking.o: ../../Microchip/WirelessProtocols/LCDBlocking.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/293088702" 
	@${RM} ${OBJECTDIR}/_ext/293088702/LCDBlocking.o.d 
	@${RM} ${OBJECTDIR}/_ext/293088702/LCDBlocking.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/293088702/LCDBlocking.o   ../../Microchip/WirelessProtocols/LCDBlocking.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/293088702/LCDBlocking.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/293088702/LCDBlocking.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/544971058/MiWiPRO.o: ../../Microchip/WirelessProtocols/MiWiPRO/MiWiPRO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/544971058" 
	@${RM} ${OBJECTDIR}/_ext/544971058/MiWiPRO.o.d 
	@${RM} ${OBJECTDIR}/_ext/544971058/MiWiPRO.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/544971058/MiWiPRO.o   ../../Microchip/WirelessProtocols/MiWiPRO/MiWiPRO.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/544971058/MiWiPRO.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/544971058/MiWiPRO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/293088702/SymbolTime.o: ../../Microchip/WirelessProtocols/SymbolTime.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/293088702" 
	@${RM} ${OBJECTDIR}/_ext/293088702/SymbolTime.o.d 
	@${RM} ${OBJECTDIR}/_ext/293088702/SymbolTime.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/293088702/SymbolTime.o   ../../Microchip/WirelessProtocols/SymbolTime.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/293088702/SymbolTime.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/293088702/SymbolTime.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/438677213/P2P.o: ../../Microchip/WirelessProtocols/P2P/P2P.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/438677213" 
	@${RM} ${OBJECTDIR}/_ext/438677213/P2P.o.d 
	@${RM} ${OBJECTDIR}/_ext/438677213/P2P.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/438677213/P2P.o   ../../Microchip/WirelessProtocols/P2P/P2P.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/438677213/P2P.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/438677213/P2P.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/293088702/MSPI.o: ../../Microchip/WirelessProtocols/MSPI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/293088702" 
	@${RM} ${OBJECTDIR}/_ext/293088702/MSPI.o.d 
	@${RM} ${OBJECTDIR}/_ext/293088702/MSPI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/293088702/MSPI.o   ../../Microchip/WirelessProtocols/MSPI.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/293088702/MSPI.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/293088702/MSPI.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/714055519/MiWi.o: ../../Microchip/WirelessProtocols/MiWi/MiWi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/714055519" 
	@${RM} ${OBJECTDIR}/_ext/714055519/MiWi.o.d 
	@${RM} ${OBJECTDIR}/_ext/714055519/MiWi.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/714055519/MiWi.o   ../../Microchip/WirelessProtocols/MiWi/MiWi.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/714055519/MiWi.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/714055519/MiWi.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/293088702/Console.o: ../../Microchip/WirelessProtocols/Console.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/293088702" 
	@${RM} ${OBJECTDIR}/_ext/293088702/Console.o.d 
	@${RM} ${OBJECTDIR}/_ext/293088702/Console.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/293088702/Console.o   ../../Microchip/WirelessProtocols/Console.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/293088702/Console.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/293088702/Console.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/860796210/crc.o: ../../Microchip/Transceivers/crc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/860796210" 
	@${RM} ${OBJECTDIR}/_ext/860796210/crc.o.d 
	@${RM} ${OBJECTDIR}/_ext/860796210/crc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/860796210/crc.o   ../../Microchip/Transceivers/crc.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/860796210/crc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/860796210/crc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/860796210/security.o: ../../Microchip/Transceivers/security.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/860796210" 
	@${RM} ${OBJECTDIR}/_ext/860796210/security.o.d 
	@${RM} ${OBJECTDIR}/_ext/860796210/security.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/860796210/security.o   ../../Microchip/Transceivers/security.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/860796210/security.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/860796210/security.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1684968892/MRF24J40.o: ../../Microchip/Transceivers/MRF24J40/MRF24J40.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1684968892" 
	@${RM} ${OBJECTDIR}/_ext/1684968892/MRF24J40.o.d 
	@${RM} ${OBJECTDIR}/_ext/1684968892/MRF24J40.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1684968892/MRF24J40.o   ../../Microchip/Transceivers/MRF24J40/MRF24J40.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1684968892/MRF24J40.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1684968892/MRF24J40.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/776994990/MRF49XA.o: ../../Microchip/Transceivers/MRF49XA/MRF49XA.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/776994990" 
	@${RM} ${OBJECTDIR}/_ext/776994990/MRF49XA.o.d 
	@${RM} ${OBJECTDIR}/_ext/776994990/MRF49XA.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/776994990/MRF49XA.o   ../../Microchip/Transceivers/MRF49XA/MRF49XA.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/776994990/MRF49XA.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/776994990/MRF49XA.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/777114154/MRF89XA.o: ../../Microchip/Transceivers/MRF89XA/MRF89XA.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/777114154" 
	@${RM} ${OBJECTDIR}/_ext/777114154/MRF89XA.o.d 
	@${RM} ${OBJECTDIR}/_ext/777114154/MRF89XA.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../../Microchip/Include" -I"../../Microchip/Include/Transceivers" -I"../../Microchip/Include/WirelessProtocols" -I"../../Microchip/Include/WirelessProtocols/MiWi" -I"../../Microchip/Include/WirelessProtocols/MiWiPRO" -I"../../Microchip/Include/WirelessProtocols/P2P" -I"../../Microchip/Include/Transceivers/MRF24J40" -I"../" -ml --extended -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/777114154/MRF89XA.o   ../../Microchip/Transceivers/MRF89XA/MRF89XA.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/777114154/MRF89XA.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/777114154/MRF89XA.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/MiWi_PRO_Test_Interface_-_PIC18_-_C18.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../18f27j13_g.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "..\18f27j13_g.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w -x -u_DEBUG -m"${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" -u_EXTENDEDMODE -z__MPLAB_BUILD=1  -u_CRUNTIME -z__MPLAB_DEBUG=1 -z__MPLAB_DEBUGGER_PK3=1 $(MP_LINKER_DEBUG_OPTION) -l ${MP_CC_DIR}\\..\\lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/MiWi_PRO_Test_Interface_-_PIC18_-_C18.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
else
dist/${CND_CONF}/${IMAGE_TYPE}/MiWi_PRO_Test_Interface_-_PIC18_-_C18.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../18f27j13_g.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "..\18f27j13_g.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w  -m"${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" -u_EXTENDEDMODE -z__MPLAB_BUILD=1  -u_CRUNTIME -l ${MP_CC_DIR}\\..\\lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/MiWi_PRO_Test_Interface_-_PIC18_-_C18.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
