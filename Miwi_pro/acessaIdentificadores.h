/*
 * File:   acessaIdentificadores.h
 * Author: KASSIO
 *
 * Created on 22 de Janeiro de 2014, 13:59
 */

#ifndef ACESSAIDENTIFICADORES_H
#define	ACESSAIDENTIFICADORES_H

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif



#include "../NVM.h"
#include "GenericTypeDefs.h"

BYTE getType(void);

void setType(BYTE val);

BYTE getInfoPeer(void);

void setInfoPeer(BYTE val);

void getLongAddress(BYTE *longAddress);

void setLongAddress(BYTE *longAddress);

void getUC(BYTE *uc);

void setUC(BYTE *uc);

void setChannel(BYTE *channel);

void getChannel(BYTE *channel);

void getShortAddress(BYTE *shortAddress);

void setShortAddress(BYTE *shortAddress);

void getPANID(BYTE *pan);

void setPANID(BYTE *pan);

void getPANIDConfigurado(BYTE *pan);

void setPANIDConfigurado(BYTE *pan);

BYTE getFlagEmConfiguracao(void);

void setFlagEmConfiguracao(BYTE val);

BOOL getFlagGRCNovo();

void setFlagGRCNovo(BYTE flag);

DWORD getLeituraEnergia();

void setLeituraEnergia(BYTE* energia);

void setLeituraCorrente(BYTE *corrente);

void setLeituraTensao(BYTE *tensao);

void setLeituraPotenciaAtiva(BYTE* potenciaAtiva);

void getEstadoConfiguracao(BYTE *estadoConfig);

void setEstadoConfiguracao(BYTE *estadoConfig);

void setLeituraDataHora(BYTE *dataHoraMedidor);

//WORD getLeituraCorrente();
//
//void setLeituraCorrente(WORD corrente);
//
//WORD getLeituraTensao();
//
//void setLeituraTensao(WORD tensao);
//
//BYTE * getLeituraDataHoraMedidor();
//

#endif	/* ACESSAIDENTIFICADORES_H */