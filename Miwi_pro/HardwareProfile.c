/*****************************************************************************
 *
 *              Main.c -- user main program
 *
 *****************************************************************************
 * FileName:        HardwareProfile.c
 * Dependencies:
 * Processor:       PIC18F
 * Compiler:        C18 02.20.00 or higher
 *                  PICC 9.50.00 or higher
 *                  C30 02.03.00 or higher
 * Linker:          MPLINK 03.40.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * Copyright � 2007-2008 Microchip Technology Inc.  All rights reserved.
 *
 * Microchip licenses to you the right to use, modify, copy and distribute 
 * Software only when embedded on a Microchip microcontroller or digital 
 * signal controller and used with a Microchip radio frequency transceiver, 
 * which are integrated into your product or third party product (pursuant 
 * to the terms in the accompanying license agreement).   
 *
 * You should refer to the license agreement accompanying this Software for 
 * additional information regarding your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY 
 * KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A 
 * PARTICULAR PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE 
 * LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, 
 * CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY 
 * DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO 
 * ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, 
 * LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, 
 * TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT 
 * NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *****************************************************************************
 * File Description:
 *
 *   This file provides configuration and basic hardware functionality 
 *   based on chosen hardware demo boards.
 *
 * Change History:
 *  Rev   Date         Description
 *  0.1   2/17/2009    Initial revision
 *****************************************************************************/
#include "SystemProfile.h"
#include "Compiler.h"
#include "WirelessProtocols/Console.h"
#include "WirelessProtocols/LCDBlocking.h"
#include "TimeDelay.h"
#include "HardwareProfile.h"
#include "Console.h"
#include "acessaIdentificadores.h"
#include "../TratadorDePacotes.h"


extern BOOL DISJUNTOR_ARMADO;


extern MIWI_TICK TickDifference_interrupt;
extern MIWI_TICK CurrentTime_interrupt;
extern MIWI_TICK EventTime_interrupt;

extern MIWI_TICK TickDifference_interruptled;
extern MIWI_TICK CurrentTime_interruptled;
extern MIWI_TICK EventTime_interruptled;
extern BOOL Flag;
extern int16 ativador_armar;
extern int16 ativador_desarmar;


#if defined(__18F2620) 
        #pragma romdata CONFIG1H = 0x300001
        const rom unsigned char config1H = 0b00000010;      // HS oscillator
    
        #pragma romdata CONFIG2L = 0x300002
        const rom unsigned char config2L = 0b00011111;      // Brown-out Reset Enabled in hardware @ 2.0V, PWRTEN disabled
    
        #pragma romdata CONFIG2H = 0x300003
        const rom unsigned char config2H = 0b00010000;      // HW WD disabled, 1:32 prescaler
    
        #pragma romdata CONFIG3H = 0x300005
        const rom unsigned char config3H = 0b10000000;      // PORTB digital on RESET
    
        #pragma romdata CONFIG4L = 0x300006
        const rom unsigned char config4L = 0b11000001;      // DEBUG disabled,
                                                            // XINST enabled
                                                            // LVP disabled
                                                            // STVREN enabled

#elif defined(__18F27J13)
//    #pragma romdata CONFIG2L = 0x300002
//    const rom unsigned char config2L = 0b00000010;      // HS oscillator
//    #pragma romdata CONFIG2H = 0x300003
//    const rom unsigned char config2H = 0b00001000;      // HW WD disabled, 1:32 prescaler
//    #pragma romdata CONFIG1L = 0x300000
//    const rom unsigned char config1L = 0b11100000;
//    #pragma config FOSC = HSPLL, WDTEN = OFF, WDTPS = 128, XINST = ON

    // PIC18F27J13 Configuration Bit Settings

//    #include <p18F27J13.h>

    // CONFIG1L
#pragma config WDTEN = ON      // Watchdog Timer (Enabled)
#pragma config PLLDIV = 1       // 96MHz PLL Prescaler Selection (PLLSEL=0) (No prescale (4 MHz oscillator input drives PLL directly))
#pragma config CFGPLLEN = ON   // PLL Enable Configuration Bit (PLL Disabled)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset (Enabled)
#pragma config XINST = ON
       // Extended Instruction Set (Disabled)

// CONFIG1H
#pragma config CP0 = OFF        // Code Protect (Program memory is not code-protected)

// CONFIG2L
#pragma config OSC = HSPLL      // Oscillator (HS+PLL)
#pragma config SOSCSEL = HIGH   // T1OSC/SOSC Power Selection Bits (High Power T1OSC/SOSC circuit selected)
#pragma config CLKOEC = ON      // EC Clock Out Enable Bit  (CLKO output enabled on the RA6 pin)
#pragma config FCMEN = ON       // Fail-Safe Clock Monitor (Enabled)
#pragma config IESO = ON        // Internal External Oscillator Switch Over Mode (Enabled)

// CONFIG2H
#pragma config WDTPS = 32768    // Watchdog Postscaler (1:32768)

// CONFIG3L
#pragma config DSWDTOSC = INTOSCREF// DSWDT Clock Select (DSWDT uses INTRC)
#pragma config RTCOSC = T1OSCREF// RTCC Clock Select (RTCC uses T1OSC/T1CKI)
#pragma config DSBOREN = ON     // Deep Sleep BOR (Enabled)
#pragma config DSWDTEN = ON     // Deep Sleep Watchdog Timer (Enabled)
#pragma config DSWDTPS = G2     // Deep Sleep Watchdog Postscaler (1:2,147,483,648 (25.7 days))

// CONFIG3H
#pragma config IOL1WAY = ON     // IOLOCK One-Way Set Enable bit (The IOLOCK bit (PPSCON<0>) can be set once)
#pragma config ADCSEL = BIT10   // ADC 10 or 12 Bit Select (10 - Bit ADC Enabled)
#pragma config PLLSEL = PLL96   // PLL Selection Bit (Selects 96MHz PLL)
#pragma config MSSP7B_EN = MSK7 // MSSP address masking (7 Bit address masking mode)

// CONFIG4L
#pragma config WPFP = PAGE_127  // Write/Erase Protect Page Start/End Location (Write Protect Program Flash Page 127)
#pragma config WPCFG = OFF      // Write/Erase Protect Configuration Region  (Configuration Words page not erase/write-protected)

// CONFIG4H
#pragma config WPDIS = OFF      // Write Protect Disable bit (WPFP<6:0>/WPEND region ignored)
#pragma config WPEND = PAGE_WPFP
    // CONFIG4L
//    #pragma config WPFP = PAGE_127  // Write/Erase Protect Page Start/End Location (Write Protect Program Flash Page 127)
//    #pragma config WPCFG = OFF      // Write/Erase Protect Configuration Region  (Configuration Words page not erase/write-protected)

    // CONFIG4H
//    #pragma config WPDIS = OFF      // Write Protect Disable bit (WPFP<6:0>/WPEND region ignored)
//    #pragma config WPEND = PAGE_WPFP// Write/Erase Protect Region Select bit (valid when WPDIS = 0) (Pages WPFP<6:0> through Configuration Words erase/write protected)


#elif defined(__18F2520)
        #pragma romdata CONFIG1H = 0x300001
        const rom unsigned char config1H = 0b00001000;      // use internal oscillator
        
        #pragma romdata CONFIG2L = 0x300002
        const rom unsigned char config2L = 0b00011111;      // Brown-out Reset Enabled in hardware @ 2.0V, PWRTEN disabled
        
        #pragma romdata CONFIG2H = 0x300003
        const rom unsigned char config2H = 0b00010110;      // HW WD disabled, 1:32 prescaler
        
        #pragma romdata CONFIG3H = 0x300005
        const rom unsigned char config3H = 0b00000011;      // PORTB digital as PORT
        
        #pragma romdata CONFIG4L = 0x300006
        const rom unsigned char config4L = 0b10000001;      // DEBUG disabled,
                                                            // XINST disabled
                                                            // LVP disabled
                                                            // STVREN enabled
#elif defined(__18F87J11)

    #pragma config FOSC = HSPLL, WDTEN = OFF, WDTPS = 128, XINST = ON  
    //#pragma config FOSC = INTOSC, WDTEN = OFF, WDTPS = 128, XINST = ON  
                                                        
#elif defined(__18F46J50)

    //#pragma config OSC = INTOSCPLL, WDTEN = OFF, XINST = ON, WDTPS = 2048, PLLDIV = 2, CPUDIV = OSC2_PLL2
    #pragma config OSC = INTOSC, WDTEN = OFF, XINST = ON, WDTPS = 2048, PLLDIV = 2, CPUDIV = OSC2_PLL2




#endif

#define DEBOUNCE_TIME 0x00003FFF


//////////////////////////////////////////////////////////////////////////////////////
//
//        void low_isr(void);
//        void high_isr(void);
//// Prevent application code from
//// being written into FLASH
//// memory space needed for the
//// Bootloader firmware at
//// addresses 0 through 3FFh.
//#pragma romdata BootloaderProgramMemorySpace = 0x6
//const rom char bootloaderProgramMemorySpace[0x400 - 0x6];
//// The routine _startup() is
//// defined in the C18 startup
//// code (usually c018i.c) and
//// is usually the first code to be called by a GOTO at the
//// normal reset vector of 0.
//extern void _startup(void);
//// Since the bootloader isn't
//// going to write the normal
//// reset vector at 0, we have
//// to generate our own remapped
//// reset vector at the address
//// specified in the
//// bootloader firmware.
//#pragma code AppVector = 0x400
//void AppVector(void)
//{
//_asm GOTO _startup _endasm
//}
//// For PIC18 devices the high
//// priority interrupt vector is
//// normally positioned at
//// address 0008h, but the
//// bootloader resides there.
//// Therefore, the bootloader's
//// interrupt vector code is set
//// up to branch to our code at
//// 0408h.
//#pragma code AppHighIntVector = 0x408
//void AppHighIntVector(void)
//{
//_asm GOTO high_isr _endasm // branch to the high_isr()
//// function to handle priority
//// interrupts.
//}
//#pragma code AppLowIntVector = 0x418
//void low_vector(void)
//{
//_asm GOTO low_isr _endasm // branch to the low_isr()
//// function to handle low
//// priority interrupts.
//}
//#pragma code // return to the default
//// code section
//
//void low_isr(void){Nop();}
//void high_isr(void){Nop();}






        
//////////////////////////////////////////////////////////////////////////////////////

MIWI_TICK eventTime_Disjuntor_Armar,eventTime_Disjuntor_Desarmar;
extern INT CONTADOR;
//extern CONNECTION_ENTRY    ConnectionTable;

/*********************************************************************
 * Function:        void BoardInit( void )
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    Board is initialized for P2P usage
 *
 * Overview:        This function configures the board 
 *
 * Note:            This routine needs to be called before the function 
 *                  to initialize P2P stack or any other function that
 *                  operates on the stack
 ********************************************************************/

void BoardInit(void)
{
    #if defined(PICDEMZ)
        WDTCONbits.SWDTEN = 0; //disable WDT
        
        // Switches S2 and S3 are on RB5 and RB4 respectively. We want interrupt-on-change
        INTCON = 0x00;
        
        // There is no external pull-up resistors on S2 and S3. We will use internal pull-ups.
        // The MRF24J40 is using INT0 for interrupts
        // Enable PORTB internal pullups
        INTCON2 = 0x00;
        INTCON3 = 0x00;
    
        // Make PORTB as input - this is the RESET default
         #if defined(BOIA)
              TRISB = 0b00001101;//0xff;
              PORTB=0;
         #elif defined(GRCS)
              TRISB = 0b00000110;
         #elif defined(GRC_NOVA)
              TRISB = 0b00000001;
              TRISAbits.TRISA5 = 0;
         #endif
        // Set PORTC control signal direction and initial states
        // disable chip select
        LATC = 0xfd;
    
        // Set the SPI module for use by Stack
        TRISC = 0xD4;
    
        // Set the SPI module
        #if defined(HARDWARE_SPI)
            SSPSTAT = 0xC0;
            SSPCON1 = 0x20;
        #endif
        
        // D1 and D2 are on RA0 and RA1 respectively, and CS of TC77 is on RA2.
        // Make PORTA as digital I/O.
        // The TC77 temp sensor CS is on RA2.
        ADCON1 = 0x0F;
#ifdef GRC_NOVA
        ADCON1 = 0xFF;
#endif
        // Deselect TC77 (RA2)
//        LATA = 0x04;
    
        // Make RA0, RA1, RA2 and RA4 as outputs.
//        TRISA = 0xFF;
//        PORTA = 0;RISA1 = 1;
        
//        TRISAbits.TRISA1 = 1;
        PHY_CS = 1;             //deselect the MRF24J40
        PHY_CS_TRIS = 0;        //make chip select an output   
        
        RFIF = 0;               //clear the interrupt flag
    
        RCONbits.IPEN = 1;

#if defined(GRCS)
        INTCON2bits.INTEDG1 = 0;
#endif

#if defined(GRC_NOVA)
        INTCON2bits.INTEDG0 = 0;
        INTCONbits.INT0IE = 1;
#endif
#if defined(BOIA)
        INTCON2bits.INTEDG0 = 0;
#endif
        INTCONbits.GIEH = 1;

    #else
        #error "Unknown demo board.  Please properly initialize the part for the board."
    #endif
        ANCON1bits.PCFG11 = 1;
        ANCON0bits.PCFG4 = 1;        MCRL_MEDICAO_TRIS = 0;
}

void disjuntorInit(void)
{
    DISJUNTOR_ON  = 0;
    DISJUNTOR_OFF = 0;
    DISJUNTOR_ON_TRIS = 0;
    DISJUNTOR_OFF_TRIS = 0;
    ANCON0bits.PCFG2 = 1;
    ANCON0bits.PCFG3 = 1;
}


/*********************************************************************
 * Function:        void LCDTRXCount(BYTE txCount, BYTE rxCount)
 *
 * PreCondition:    LCD has been initialized
 *
 * Input:           txCount - the total number of transmitted messages
 *                  rxCount - the total number of received messages
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        This function display the total numbers of TX and
 *                  RX messages on the LCD, if applicable.
 *
 * Note:            This routine is only effective if Explorer16 or
 *                  PIC18 Explorer demo boards are used
 ********************************************************************/
void LCDTRXCount(BYTE txCount, BYTE rxCount)
{
    #if defined(EXPLORER16) || defined(PIC18_EXPLORER) || (defined(EIGHT_BIT_WIRELESS_BOARD) && defined(SENSOR_PORT_LCD))
        LCDErase();
        #if defined(PIC18_EXPLORER) || defined(EIGHT_BIT_WIRELESS_BOARD)
            sprintf((char *)LCDText, (far rom char*)"TX Messages: %3d", txCount);
            sprintf((char *)&(LCDText[16]), (far rom char*)"RX Messages: %3d", rxCount);
        #else
            sprintf((char *)LCDText, (const char*)"TX Messages: %d", txCount);
            sprintf((char *)&(LCDText[16]), (const char*)"RX Messages: %d", rxCount);
        #endif
        LCDUpdate();    
    #endif
}



/*********************************************************************
 * Function:        void LCDDisplay(char *text, BYTE value, BOOL delay)
 *
 * PreCondition:    LCD has been initialized
 *
 * Input:           text - text message to be displayed on LCD
 *                  value - the text message allows up to one byte 
 *                          of variable in the message
 *                  delay - whether need to display the message for
 *                          2 second without change
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        This function display the text message on the LCD, 
 *                  including up to one BYTE variable, if applicable.
 *
 * Note:            This routine is only effective if Explorer16 or
 *                  PIC18 Explorer demo boards are used
 ********************************************************************/
void LCDDisplay(char *text, BYTE value, BOOL delay)
{
    #if defined(EXPLORER16) || defined(PIC18_EXPLORER) || (defined(EIGHT_BIT_WIRELESS_BOARD) && defined(SENSOR_PORT_LCD))
        LCDErase();
            
        #if defined(PIC18_EXPLORER) || defined(EIGHT_BIT_WIRELESS_BOARD)
            sprintf((char *)LCDText, (far rom char*)text, value); 
        #elif defined(EXPLORER16)
            sprintf((char *)LCDText, (const char*)text, value); 
        #endif
        LCDUpdate();
        
        // display the message for 2 seconds
        if( delay )
        {
            BYTE i;
            for(i = 0; i < 8; i++)
            {
                DelayMs(250);
            }
        }
    #endif
}


/*********************************************************************
 * Function:        BYTE ButtonPressed(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          Byte to indicate which button has been pressed. 
 *                  Return 0 if no button pressed.
 *
 * Side Effects:    
 *
 * Overview:        This function check if any button has been pressed
 *
 * Note:            
 ********************************************************************/
/*BYTE ButtonPressed(void)
{
    MIWI_TICK tickDifference;

    if(PUSH_BUTTON_1 == 0)
    {
        //if the button was previously not pressed
        if(PUSH_BUTTON_pressed == FALSE)
        {
            PUSH_BUTTON_pressed = TRUE;
            PUSH_BUTTON_press_time = MiWi_TickGet();
            return 1;
        }
    }
    else if(PUSH_BUTTON_2 == 0)
    {
        //if the button was previously not pressed
        if(PUSH_BUTTON_pressed == FALSE)
        {
            PUSH_BUTTON_pressed = TRUE;
            PUSH_BUTTON_press_time = MiWi_TickGet();
            return 2;
        }
    }
    else
    {
        //get the current time
        MIWI_TICK t = MiWi_TickGet();

        //if the button has been released long enough
        tickDifference.Val = MiWi_TickGetDiff(t,PUSH_BUTTON_press_time);

        //then we can mark it as not pressed
        if(tickDifference.Val > DEBOUNCE_TIME)
        {
            PUSH_BUTTON_pressed = FALSE;
        }
    }

    return 0;
}*/

void armaDisjuntor(){
    MIWI_TICK tickDifference;

    if(DISJUNTOR_ON != 1){
        DISJUNTOR_ON = 1;
        eventTime_Disjuntor_Armar = MiWi_TickGet();
    }

    tickDifference.Val = MiWi_TickGetDiff(MiWi_TickGet(),eventTime_Disjuntor_Armar);

     if(tickDifference.Val > (2*HUNDRED_MILI_SECOND/*ONE_SECOND*/))
     {
        DISJUNTOR_ON = 0;
        DISJUNTOR_ARMADO = TRUE;
        ativador_armar = 0;
     }

}

void desarmaDisjuntor(){
    MIWI_TICK tickDifference;
    if(DISJUNTOR_OFF != 1){
        DISJUNTOR_OFF = 1;
        eventTime_Disjuntor_Desarmar = MiWi_TickGet();
    }

    tickDifference.Val = MiWi_TickGetDiff(MiWi_TickGet(),eventTime_Disjuntor_Desarmar);

     if(tickDifference.Val > (2*HUNDRED_MILI_SECOND/*ONE_SECOND*/))
     {
        DISJUNTOR_OFF = 0;
        DISJUNTOR_ARMADO = FALSE;
        ativador_desarmar = 0;
     }

}





#if defined(__18CXX)

    void UserInterruptHandler(void)
    {
        
    }

    void high_isr();

    void low_isr();

#endif
