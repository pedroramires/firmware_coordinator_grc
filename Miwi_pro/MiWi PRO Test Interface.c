/********************************************************************
* FileName:		MiWi PRO Test Interface.c
* Dependencies: none
* Processor:	PIC18, PIC24F, PIC24H, dsPIC30, dsPIC33
*               tested with 18F4620, dsPIC33FJ256GP710
* Hardware:		PICDEM Z, PIC18 Explorer, 8bit-WDK, Explorer 16
* Complier:     Microchip C18 v3.04 or higher
*
 * 		Microchip C30 v2.03 or higher
*               Microchip C32 v1.11 or higher
* Company:		Microchip Technology, Inc.
*
* Copyright and Disclaimer Notice for P2P Software:
*
* Copyright � 2007-2011 Microchip Technology Inc.  All rights reserved.
*
* Microchip licenses to you the right to use, modify, copy and distribute
* Software only when embedded on a Microchip microcontroller or digital
* signal controller and used with a Microchip radio frequency transceiver,
* which are integrated into your product or third party product (pursuant
* to the terms in the accompanying license agreement).
*
* You should refer to the license agreement accompanying this Software for
* additional information regarding your rights and obligations.
*
* SOFTWARE AND DOCUMENTATION ARE PROVIDED ?AS IS? WITHOUT WARRANTY OF ANY
* KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY
* WARRANTY OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A
* PARTICULAR PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE
* LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY,
* CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY
* DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO
* ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES,
* LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS,
* TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT
* NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*
*********************************************************************
* File Description:
*
*  This is the testing interface that is written for MiWi PRO.
*  Hyper terminal is used to display and select the testing
*  interface driven by menu.
*
* Change History:
*  Rev   Date         Author    Description
*  4.1   06/01/2011   yfy       Initial release
********************************************************************/

/************************ HEADERS **********************************/
#include "WirelessProtocols/Console.h"
#include "Transceivers/Transceivers.h"
#include "WirelessProtocols/MCHP_API.h"
#include "../NVM.h"
#include "GenericTypeDefs.h"
#include "acessaIdentificadores.h"
#include "../ConfiguracaoViaSerial.h"
#include "../../medidorGRC.h"
#include "../serial_old.h"
#include "../MontadorDePacotesGRC.h"
#include "../globais.h"
#include "../TratadorDePacotes.h"
#include "../inicio_miwi.h"
#include "HardwareProfile.h"
#include "TimeDelay.h"

//#include "WirelessProtocols/SymbolTime.h"

#define CONF_CHANNEL_MAP  1
#define CONF_PANID        2
#define CONF_LONGADDRESS  3
#define CONF_UC           4

#define AG_START 1
#define AG_STOP  2
#define SIZE_PACOTE 50




/************************ VARIABLES ********************************/

/*******************************************************************/
// AdditionalConnectionPayload variable array defines the additional
// information to identify a device on a P2P connection. This array
// will be transmitted with the P2P_CONNECTION_REQUEST command to
// initiate the connection between the two devices. Along with the
// long address of this device, this  variable array will be stored
// in the P2P Connection Entry structure of the partner device. The
// size of this array is ADDITIONAL_CONNECTION_PAYLOAD, defined in
// P2PDefs.h.
// In this demo, this variable array is set to be empty.
/******************************************************************/
#if ADDITIONAL_NODE_ID_SIZE > 0
   BYTE AdditionalNodeID[ADDITIONAL_NODE_ID_SIZE] = {0x00};
#endif

/*******************************************************************/
// The variable myChannel defines the channel that the P2P connection
// is operate on. This variable will be only effective if energy scan
// (ENABLE_ED_SCAN) is not turned on. Once the energy scan is turned
// on, the operating channel will be one of the channels available with
// least amount of energy (or noise).
/*******************************************************************/
BYTE myChannel = 0x25;

BYTE TxNum = 0;
BYTE RxNum = 0;
 
BOOL bReceivedMessage = FALSE;
BOOL DISJUNTOR_ARMADO  = FALSE;

BYTE ESTADO_CONFIGURADOR = 0x00;

BYTE atributos_HEX[TOTAL_DATA_PACKET_SIZE], bufferDeRecepcao[50];
BYTE atributos_para_enviar_ao_configurador[TOTAL_DATA_PACKET_SIZE];

BYTE longAddressAbuscar_HEX[LONG_ADDRESS_SIZE];

BOOL RECEBENDO_ATRIBUTOS_VIA_RADIO = FALSE;
BYTE indice_buffer_RX_radio = 0;
BYTE atributos_recebidos_via_radio[TOTAL_DATA_PACKET_SIZE];
BYTE longAddress_recebido_via_radio[LONG_ADDRESS_SIZE];

BYTE short_address_GRC_na_rede[SHORT_ADDRESS_SIZE];
BOOL CONFIGURANDO_GRC_NOVO = FALSE;
BYTE FLAG;
BOOL CALIBRACAO_ATIVADA = FALSE;


int contador = 0;
pacoteGRC p, pacoteRecepcao;

BYTE estadoConfiguracao = 0x00;
int i;

MIWI_TICK TickDifference_interrupt;
MIWI_TICK CurrentTime_interrupt;

MIWI_TICK EventTime_interrupt;
//MIWI_TICK TickDifference_interruptled;
//MIWI_TICK CurrentTime_interruptled;
MIWI_TICK EventTime_interruptled;
MIWI_TICK EventTime_interrupt2;
MIWI_TICK EventTime_interruptrele;


BOOL Flag;
BOOL Flag2;


int16 ativador_armar = 0;
int16 ativador_desarmar = 0;


extern ROM char * const menu;

extern ROM char * const menuConfiguracao;

extern ROM char * const menuAlterarTipo;

extern ROM char * const msg_01;

extern BYTE estadoLeiturasMedidor;


#if defined TESTE

                    extern BYTE     NeighborRoutingTable[NUM_COORDINATOR][16/8];
                    extern BYTE     RoutingTable[16/8];
                    extern BYTE     FamilyTree[16];
#else

                    extern BYTE     NeighborRoutingTable[NUM_COORDINATOR][NUM_COORDINATOR/8];
                    extern BYTE     RoutingTable[NUM_COORDINATOR/8];
                    extern BYTE     FamilyTree[NUM_COORDINATOR];
#endif
//extern BYTE RoutingTable[NUM_COORDINATOR/8];
//extern BYTE FamilyTree[NUM_COORDINATOR];
//extern BYTE NeighborRoutingTable[NUM_COORDINATOR][NUM_COORDINATOR/8];

extern void PHYSetShortRAMAddr(INPUT BYTE address, INPUT BYTE value);
extern void InitMRF24J40(void);


void tratar_comandos_radio(RECEIVED_MESSAGE  rxMessage);
void iniciar_leds();

INT CONTADOR;

extern BYTE myLongAddress[8];

BOOL RECEBENDO_ATRIBUTOS_PELA_SERIAL = FALSE;
BOOL RECEBENDO_LONG_ADDRESS_PELA_SERIAL = FALSE;
BOOL RECEBENDO_SOLICITACAO_PELA_SERIAL = FALSE;

int indiceBuffer = 0;
BYTE dado;

BOOL RECEBENDO_INICE = FALSE;
BOOL RECEBENDO_POTENCIA = FALSE;
BOOL RECEBENDO_ENDERECO = FALSE;
BOOL REQUISITANDO_REDE = FALSE;
BOOL DESTRUINDO_REDE = FALSE;
BOOL RECEBENDO_LONG_ADDRESS = FALSE;


BYTE shortAddressDestinoHex[SHORT_ADDRESS_SIZE];
BYTE shortAddressDestinoHexInvertido[SHORT_ADDRESS_SIZE];
BYTE shortAddressDestinoASCII[SHORT_ADDRESS_SIZE*2];

BYTE longAddressDestinoHex[MY_ADDRESS_LENGTH];
BYTE longAddressDestinoHexInvertido[MY_ADDRESS_LENGTH];
BYTE longAddressDestinoASCII[MY_ADDRESS_LENGTH*2];

BYTE Array[100],Array2[100], Pacote[50];

extern BYTE defaultHops;
extern BYTE MiWiPROSeqNum;
extern BYTE TxBuffer[TX_BUFFER_SIZE+MIWI_PRO_HEADER_LEN];
extern BYTE            TxData;
extern MAC_TRANS_PARAM MTP;




   

void iniciar_leds(){
    L3_RADIO = 0;
    L2_RADIO = 0;
    L1_RADIO = 0;
    DelayMs(500);
    L3_RADIO = 1;
    L2_RADIO = 0;
    L1_RADIO = 0;
    DelayMs(500);
    L3_RADIO = 1;
    L2_RADIO = 1;
    L1_RADIO = 0;
    DelayMs(500);
    L3_RADIO = 1;
    L2_RADIO = 1;
    L1_RADIO = 1;
    
}

void encaminhaPacoteParaTratamento(RECEIVED_MESSAGE  rxMessage){
            int16 count, count2;
            Printf("\r\n");
            

            //CONCENTRADOR_ID
            bufferDeRecepcao[0] = rxMessage.Payload[1];
            bufferDeRecepcao[1] = rxMessage.Payload[2];
            bufferDeRecepcao[2] = rxMessage.Payload[3];
            bufferDeRecepcao[3] = rxMessage.Payload[4];
            //FUNCAO
            bufferDeRecepcao[4] = rxMessage.Payload[5];
//            printf("\r\n");
//            printf("FUN��O RECEBIDA: ");
//            PrintDec( bufferDeRecepcao[4]);
//             printf("\r\n");
//              printf("\r\n");
//            printf("TAMANHO RECEBIDO: ");
//            PrintDec(rxMessage.Payload[6]);
//             printf("\r\n");
//             for(count2 = 1; count2 < rxMessage.Payload[6]; count2++ ){
//                PrintDec(rxMessage.Payload[count2]);
//                Printf("|");
//
//            }
            Printf("\r\n");
            switch (bufferDeRecepcao[4]){

                case REQUISITAR_ID_MEDIDOR:
                    //trataPacoteRequisitaIDMedidor(bufferDeRecepcao,&pacoteRecepcao);
                    break;
                case REQUISITAR_VERSAO_FIRMWARE:
                    trataPacoteRequisitaVersaoFirmware(bufferDeRecepcao,rxMessage,&pacoteRecepcao);
                    break;
                case REQUISITAR_INFO_MEDIDOR:
                    trataPacoteRequisitaInfoMedidor(bufferDeRecepcao,rxMessage,&pacoteRecepcao);
                    break;
                case REQUISITAR_DATA_HORA_MEDIDOR:
                    trataPacoteRequisitaData_Hora(bufferDeRecepcao,rxMessage,&pacoteRecepcao);
                    break;
                case REQUISITAR_ENERGIA_MEDIDOR:
                    trataPacoteRequisitaEnergiaMedidor(bufferDeRecepcao,rxMessage,&pacoteRecepcao);
                    break;
                case GRAVAR_INFO_MEDIDOR:
                    trataPacoteGravaInfoMedidor(bufferDeRecepcao,rxMessage,&pacoteRecepcao);
                    break;
                case GRAVAR_DATA_HORA_MEDIDOR:
                    trataPacoteGravaData_HoraMedidor(bufferDeRecepcao,rxMessage,&pacoteRecepcao);
                    break;
                case REQUISITAR_DADOS_MEDICAO_UC:
                    trataPacoteRequisitaDadosDeMedicaoUC(bufferDeRecepcao,rxMessage,&pacoteRecepcao);
                    break;
                case FUNCAO_REQ_ARMA_RELE:
                    trataPacoteRespostaArmaRele(bufferDeRecepcao,rxMessage,&pacoteRecepcao);
                    break;
                case FUNCAO_REQ_DESARMA_RELE:
                    trataPacoteRespostaDesarmaRele(bufferDeRecepcao,rxMessage,&pacoteRecepcao);
                    break;
                case FUNCAO_ZERA_ENERGIA_MEDIDOR:
                    trataPacoteZeraEnergiaMedidor(bufferDeRecepcao,rxMessage,&pacoteRecepcao);
                    break;
                case FUNCAO_REQ_TOTAL_DATA:
                    trataPacoteRequisitaTotalData(bufferDeRecepcao,rxMessage,&pacoteRecepcao);
                    break;

                default:


                    break;


            }

//           if(estadoLeiturasMedidor){
               MiApp_FlushTx();
               MiApp_WriteData(0xC1);
               Printf("\r\n");
               Printf("TAMANHO DO PACOTE A SER ENVIADO: ");
               PrintDec(pacoteRecepcao.getTamanho);
               Printf("\r\n");
               PrintDec(pacoteRecepcao.getPacoteMontado[5]);
               Printf("\r\n");
                for(count = 0; count < pacoteRecepcao.getPacoteMontado[5] + 6; count++)
                {
                     MiApp_WriteData(pacoteRecepcao.getPacoteMontado[count]);
                     PrintDec(pacoteRecepcao.getPacoteMontado[count]);
                     Printf("|");
                }
                Printf("\r\n");
    //           if(MiApp_UnicastAddress(rxMessage.SourceAddress,FALSE,FALSE)){
    //                Printf("\r\nMensagem Unicast enviada com sucesso");
    //
    //           }else
    //                 Printf("\r\nMensagem Unicast n�o enviada");


                 if(MiApp_UnicastAddress(rxMessage.SourceAddress,FALSE,FALSE)){
                        Printf("\r\nMensagem Unicast enviada com sucesso");
                 } else
                       Printf("\r\nMensagem Unicast n�o enviada");
//           }



}

int configuraGRC(){
    BYTE isRunning = 1, aux,channeMap[4],longAddress[8],panID[2],uc[8];
    int16 estado = 0, contadorDeBytes = 0;
    while(isRunning){
        if(serial2_tem_dado_bufferRx())
        {
           aux = serial2_retira_dado_bufferRx();
           if(aux == 'N')
           {
              estado = CONF_CHANNEL_MAP;
              aux = serial2_retira_dado_bufferRx();
           }else if(aux == 'X'){

               setUC(uc);
               setLongAddress(longAddress);
               setPANID(panID);
               setPANIDConfigurado(panID);
               setChannel(channeMap);
               estadoConfiguracao=0xFF;
               setEstadoConfiguracao(&estadoConfiguracao);
               Printf("OK");
               isRunning = 0;
               MiApp_RemoveConnection(0xFF);
               ler_Conf_Inicial();
               Reset();
               return 1;
           }
           switch (estado){
               case CONF_CHANNEL_MAP:{
                   channeMap[contadorDeBytes] = aux;
                   contadorDeBytes++;
                   if(contadorDeBytes == 4)
                   {
                       
                       estado = CONF_PANID;
                       contadorDeBytes = 0;
 
                   }
                  
                 break;
               }

               case CONF_PANID:{
                  panID[contadorDeBytes] = aux;
                   contadorDeBytes++;
                   if(contadorDeBytes == 2)
                   {
                       
                       estado = CONF_LONGADDRESS;
                       contadorDeBytes = 0;
                      
                   }
                   
                 break;
               }

               case CONF_LONGADDRESS:{
                   longAddress[contadorDeBytes] = aux;
                   contadorDeBytes++;
                   if(contadorDeBytes == 8)
                   {
                       
                       estado = CONF_UC;
                       contadorDeBytes = 0;
                      
                   }
                   
                 break;
               }

               case CONF_UC:{
                   uc[contadorDeBytes] = aux;
                   contadorDeBytes++;
                   if(contadorDeBytes == 8)
                   {
                       
                       estado = CONF_CHANNEL_MAP;
                       contadorDeBytes = 0;
                       
                   }
                   
                 break;
               }

               default:{
                  
                   break;
               }
           }



        }
        
        
    }
    

}


int configuraGRC_serial(){
    BYTE isRunning = 1, aux,channeMap[4],longAddress[8],panID[2],uc[8];
    int16 i, contadorDeBytes = 0;
    int16 estado;

    estado = AG_START;
    while(1){
        while(isRunning){
            if(serial2_tem_dado_bufferRx())
            {
               aux = serial2_retira_dado_bufferRx();
               switch(estado){
                  case(AG_START):
                      if(aux=='S'){
    //                      Printf("\n\r Aqui1");
                          contadorDeBytes=0;
                          estado = AG_STOP;
                      }else{
                          break;
                      }
                  break;
                  case(AG_STOP):
                      if(aux=='X'){
                          estado = AG_START;
                          isRunning=0;
    //                      Printf("\n\r Aqui2\n\r");
                      }else{
                          Array[contadorDeBytes++]=aux;
                      }
                  break;
                  default:
                      break;

               }
            }
        }



        switch(Array[0]){
            case(0x57):{//'W'
                for(i=0;i<100;i++){
                    Array2[i] = convertASCII_to_HEX(Array[i]);
                }
                formatArrayToHex(&Array2[1], 49, Pacote);
                for(i=0;i<4;i++)
                    channeMap[3-i] = Pacote[i];
                for(i=0;i<2;i++)
                    panID[1-i]= Pacote[i+4];
                for(i=0;i<8;i++)
                    longAddress[i] = Pacote[i+6];
                for(i=0;i<8;i++)
                    uc[i] = Pacote[i+14];

               setUC(uc);
               setLongAddress(longAddress);
               setPANID(panID);
               setPANIDConfigurado(panID);
               setChannel(channeMap);
               estadoConfiguracao=0xFF;
               setEstadoConfiguracao(&estadoConfiguracao);
//               for(i=0;i<8;i++)
//                    PrintChar(uc[i]);
               Printf("CSWOKX");
               MiApp_RemoveConnection(0xFF);
               ler_Conf_Inicial();
//               Reset();
            }
            break;
            case(0x52):{//'R'
               getUC(uc);
               getLongAddress(longAddress);
               getPANIDConfigurado(panID);
               getChannel(channeMap);
               Printf("CSR");
               for(i=0;i<4;i++)
                    PrintChar(channeMap[3-i]);
               for(i=0;i<2;i++)
                    PrintChar(panID[1-i]);
               for(i=0;i<8;i++)
                    PrintChar(longAddress[i]);
               for(i=0;i<8;i++)
                    PrintChar(uc[i]);
               Printf("X");
            }
            break;
            case(0x45):{//'E'
                zerarEnergiaMedidor();
                Printf("CSEOKX");
            }
            break;
            case(0x5A):{//'Z'
                Printf("CSZOKX");
                DelayMs(1000);
                Reset();
            }
            break;
            default:
                Printf("CSFFX");
            break;
         }
        estado = AG_START;
        isRunning = 1;

    }
    return 1;
}


void tratar_comandos_radio(RECEIVED_MESSAGE  rxMessage){
    BYTE estadoConfiguracao;
    BYTE index;
    BYTE k,q;
    int j;

    #if defined(CONFIGURADOR)

    switch(rxMessage.Payload[0]){
        case 0xA1:
            Printf("\r\n--------------- COMUNICA��O INICIADA !!! ---------------");
            L1_RADIO = 1;
            break;

        case 0xA3:
            Printf("\r\n--------------- DADOS RECEBIDOS DO GRC !!! ---------------");
            separaPayloadRecebidoViaRadio(rxMessage.Payload, rxMessage.PayloadSize, atributos_recebidos_via_radio);
            ConfiguradorMostraAtributos(atributos_recebidos_via_radio);
            break;

        case 0xA5:
            Printf("\r\n--------------- DADOS SALVOS COM SUCESSO !!! ---------------");
            break;

        case 0xB1:
            Printf("\r\n--------------- GRC ENCONTRADO !!! ---------------");
            Printf("\r\n--------------- INICIANDO PROCESSO DE CONFIGURA��O !!! ---------------");
            separaPayloadRecebidoViaRadio(rxMessage.Payload, rxMessage.PayloadSize, atributos_recebidos_via_radio);
            defineShortAddressDoGRCQueEstaSendoConfigurado(rxMessage.SourceAddress,short_address_GRC_na_rede);
            ConfiguradorIniciaComunicacaoGRCNaRede(rxMessage.SourceAddress);
            break;

        case 0xB3:
            Printf("\r\n--------------- COMUNICA��O INICIADA O GRC !!! ---------------");
            CONFIGURANDO_GRC_NOVO = FALSE;
            L1_RADIO = 1;
            break;

        case 0xB4:
            Printf("\r\n--------------- COMUNICA��O INICIADA O GRC !!! ---------------");
            L1_RADIO = 1;
            break;

        case 0xB5:
            Printf("\r\n--------------- DADOS RECEBIDOS DO GRC !!! ---------------");
            separaPayloadRecebidoViaRadio(rxMessage.Payload, rxMessage.PayloadSize, atributos_recebidos_via_radio);
            ConfiguradorMostraAtributos(atributos_recebidos_via_radio);
            break;

        case 0xC1:
            Printf("\r\n--------------- DADOS DE LEITURA RECEBIDOS DO GRC !!! ---------------");
            separaPayloadRecebidoViaRadio(rxMessage.Payload, rxMessage.PayloadSize, atributos_recebidos_via_radio);
            ConfiguradorMostraAtributos(atributos_recebidos_via_radio);
            break;
        case 0xDA:
            Printf("\r\n--------------- SETADO COMO CONFIGURADO !!! ---------------");
            estadoConfiguracao = 0xFF;
            setEstadoConfiguracao(estadoConfiguracao);
            MiApp_RemoveConnection(0xFF);
            Reset();
            break;
        case 0xDB:
            Printf("\r\n--------------- SETADO COMO DESCONFIGURADO !!! ---------------");
            estadoConfiguracao = 0x00;
            setEstadoConfiguracao(estadoConfiguracao);
            MiApp_RemoveConnection(0xFF);
            Reset();
            break;

        case 0xDC:
            Printf("\r\n----------------- Connection Table ---------------------\r\n");
            for(k = 1; k < MY_ADDRESS_LENGTH+1; k++)
            {
                PrintChar(rxMessage.Payload[k]);
            }
            ConsolePut('  ');
            PrintChar(rxMessage.Payload[9]);//k++;//            MiApp_WriteData(myShortAddress.v[1]);
            PrintChar(rxMessage.Payload[10]);//k++;//            MiApp_WriteData(myShortAddress.v[0]);
            ConsolePut('  ');
            if(rxMessage.Payload[11]==1){
                ConsolePut('Y');
            }else{
                ConsolePut('N');
            }
//            k++;//MiApp_WriteData(ConnectionTable[index].status.bits.directConnection);
            ConsolePut('  ');
            PrintChar(rxMessage.Payload[12]);k++;//MiApp_WriteData(ConnectionTable[index].PANID.v[1]);
            PrintChar(rxMessage.Payload[13]);k++;//MiApp_WriteData(ConnectionTable[index].PANID.v[0]);
            ConsolePut('  ');
            PrintChar(rxMessage.Payload[14]);k++;//MiApp_WriteData(ConnectionTable[index].AltAddress.v[1]);
            PrintChar(rxMessage.Payload[15]);k++;//MiApp_WriteData(ConnectionTable[index].AltAddress.v[0]);
            ConsolePut('  ');
//            q=k;
            for(j = 16; j < MY_ADDRESS_LENGTH+16; j++)
            {
                PrintChar(rxMessage.Payload[j]);
            }
            Printf('  \n\r');


            break;

        default:
            break;
    }
        
    #else

    switch(rxMessage.Payload[0]){
        case 0xA0:
            Printf("\r\n--------------- COMUNICA��O INICIADA COM O CONFIGURADOR !!! ---------------");
            GRC_novo_envia_ACK_Comunicacao();
            break;
            
        case 0xA2:
            Printf("\r\n--------------- CONFIGURADOR REQUISITOU OS ATRIBUTOS !!! ---------------");
            GRC_GetAtributos(atributos_para_enviar_ao_configurador);
            enviaDados(atributos_para_enviar_ao_configurador, 0xA3);
            Printf("\r\n--------------- ATRIBUTOS ENVIADOS !!! ---------------");
            break;
            
        case 0xA4:
            Printf("\r\n--------------- RECEBI OS ATRIBUTOS DO CONFIGURADOR  !!! ---------------");
            separaPayloadRecebidoViaRadio(rxMessage.Payload, rxMessage.PayloadSize, atributos_recebidos_via_radio);
            GRC_SalvaAtributos(atributos_recebidos_via_radio);
            GRC_MostraAtributos();
            MiApp_RemoveConnection(0xFF);
            ler_Conf_Inicial();
            Reset();
            break;
        

        case 0xB0:
            Printf("\r\n--------------- RECEBI PACOTE DE REQUISI��O POR CONFIGURA��O  !!! ---------------");
            separaPayloadRecebidoViaRadio(rxMessage.Payload, rxMessage.PayloadSize, longAddress_recebido_via_radio);
            LongAddressIgual(longAddress_recebido_via_radio, &FLAG);
            if(FLAG == 0x01){
                Printf("\r\n--------------- LONG ADDRESS ENVIADO BATE COM O MEU !!! ---------------");
                Printf("\r\n--------------- ENVIANDO AVISO AO CONFIGURADOR !!! ---------------");
                GRC_SinalizaQueLongAddressRecebidoEquivaleAoSeu(rxMessage.SourceAddress);
            }else{
                Printf("\r\n--------------- LONG ADDRESS ENVIADO N�O BATE COM O MEU !!! ---------------");
            }
            break;
            
        case 0xB2:
            Printf("\r\n--------------- COMUNICA��O INICIADA COM O CONFIGURADOR !!! ---------------");
            GRC_na_rede_envia_ACK_Comunicacao(rxMessage.SourceAddress);
            break;

        case 0xB4:
            Printf("\r\n--------------- CONFIGURADOR REQUISITOU OS MEUS ATRIBUTOS !!! ---------------");
            GRC_GetAtributos(atributos_para_enviar_ao_configurador);
            enviaDadosNaConfiguracaoGRCNaRede(atributos_para_enviar_ao_configurador, 0xB5, rxMessage.SourceAddress);
            break;

        case 0xB6:
            Printf("\r\n--------------- RECEBI OS ATRIBUTOS DO CONFIGURADOR  !!! ---------------");
            separaPayloadRecebidoViaRadio(rxMessage.Payload, rxMessage.PayloadSize, atributos_recebidos_via_radio);
            Printf("\r\n--------------- SALVANDO ATRIBUTOS !!! ---------------");
            GRC_SalvaAtributos(atributos_recebidos_via_radio);
            Printf("\r\n--------------- ATRIBUTOS SALVOS COM SUCESSO !!! ---------------");
            GRC_MostraAtributos();
            break;

        case 0xC1:
            Printf("\r\n ***RECEBIMENTO Payload Avaliable***: ");
            PrintDec(rxMessage.PayloadSize);
            Printf("\r\n");
            encaminhaPacoteParaTratamento(rxMessage);
            break;


        case 0xD0:
            Printf("\r\nChange Output Power RF: ");
            separaPayloadRecebidoViaRadio(rxMessage.Payload, rxMessage.PayloadSize, atributos_recebidos_via_radio);
            if(MiMAC_SetPower(atributos_recebidos_via_radio[0])){
                 PrintChar(atributos_recebidos_via_radio[0]);
                 Printf("\r\n aaa  ");
                 PrintChar(rxMessage.Payload[0]);
            }
            break;


        case 0xDA:
            Printf("\r\n--------------- SETADO COMO CONFIGURADO !!! ---------------");
            estadoConfiguracao = 0xFF;
            setEstadoConfiguracao(estadoConfiguracao);
            MiApp_RemoveConnection(0xFF);
            Reset();
            break;
        case 0xDB:
            Printf("\r\n--------------- SETADO COMO DESCONFIGURADO !!! ---------------");
            estadoConfiguracao = 0x00;
            setEstadoConfiguracao(estadoConfiguracao);
            MiApp_RemoveConnection(0xFF);
            Reset();
            break;

        case 0xDC:
            
            tempLongAddress[1] = rxMessage.SourceAddress[1];//ConnectionTable[index].AltAddress.v[1];
            tempLongAddress[0] = rxMessage.SourceAddress[0];//ConnectionTable[index].AltAddress.v[0];
            Printf("\r\n\n\n--------------- TABELA DE CONEX�ES REQUISITADA !!! ---------------\n\n\r");

            for(index = 0; index < CONNECTION_SIZE; index++)
            {


                if(ConnectionTable[index].status.bits.longAddressValid)
                {
                    MiApp_FlushTx();

                    MiApp_WriteData(0xDC);
                    for(k = 0; k < MY_ADDRESS_LENGTH; k++)
                    {
                        MiApp_WriteData(myLongAddress[MY_ADDRESS_LENGTH-1-k]);
                    }
                    MiApp_WriteData(myShortAddress.v[1]);
                    MiApp_WriteData(myShortAddress.v[0]);
                    if(ConnectionTable[index].status.bits.directConnection){
                        MiApp_WriteData(0x01);
                    }else{
                        MiApp_WriteData(0x00);
                    }
//                    MiApp_WriteData(ConnectionTable[index].status.bits.directConnection);
                    MiApp_WriteData(ConnectionTable[index].PANID.v[1]);
                    MiApp_WriteData(ConnectionTable[index].PANID.v[0]);
                    MiApp_WriteData(ConnectionTable[index].AltAddress.v[1]);
                    MiApp_WriteData(ConnectionTable[index].AltAddress.v[0]);

                    for(k = 0; k < MY_ADDRESS_LENGTH; k++)
                    {
                        MiApp_WriteData(ConnectionTable[index].Address[MY_ADDRESS_LENGTH-1-k]);
                    }

                    for(j = 0 ; j < 3; j++)
                    {
                        if(MiApp_UnicastAddress(tempLongAddress, FALSE, FALSE))
                        {
                            break;
                        }
                        DelayMs(20);
                    }
                }

                
            }
            break;
       case 0xDD:
            Printf("\r\n--------------- Destruir Rede!! ---------------");
            MiApp_RemoveConnection(0xFF);
            ler_Conf_Inicial2();
            break;

       case 0xE0:
            Printf("\r\n--------------- I know you, do you know me? ---------------\n\r");
            for (j=0; j<MY_ADDRESS_LENGTH; j++){
                tempLongAddress[j]=rxMessage.Payload[j+1];
            }
            tempShortAddress.v[0]=rxMessage.Payload[j+1];
            tempShortAddress.v[1]=rxMessage.Payload[j+2];

            if(SearchForLongAddress()== 0xFF){
                Printf("\r\n no, i dont\r\n");
//                AddNodeToNetworkTable();
                MiApp_FlushTx();
                MiApp_WriteData(0xDD);
                if(MiApp_UnicastAddress(tempLongAddress, TRUE, FALSE)){
                    Printf("\r\nMensagem Unicast enviada com sucesso");
                }
                else{
                    Printf("\r\nMensagem Unicast n�o enviada");
                }
            }else{
                Printf("\r\n Yes, i do\r\n");
            }


            break;

        case 0xE1:
            zerarEnergiaMedidor();
            break;

        default:
            break;
    }
    
    #endif
}

#if defined(__18CXX)
void main(void)
#else
int main(void)
#endif

{
    BYTE i, j;
    BYTE TxSynCount = 0;
    BOOL bReceivedMessage = FALSE;
    int k;
    BYTE index;
    
    BYTE aux;

    MIWI_TICK tickDifference;
    MIWI_TICK currentTime;
    MIWI_TICK eventTime;

    DWORD   channelMap;
    BYTE    Mode;
    BOOL   rele;
    BYTE sinalMaiorPotencia;
    BYTE indiceMaiorPotencia;


    rele = TRUE;


    BoardInit();
    ConsoleInit();
    serial_init();
    disjuntorInit();

    iniciar_leds();
    iniciar_leds();
    iniciar_leds();
    
    InitSymbolTimer();



    eventTime.Val = 0;
    EventTime_interrupt.Val = 0;
    EventTime_interruptled.Val = 0;
    EventTime_interruptrele.Val = 0;

    


    ler_Conf_Inicial();

    Flag  = FALSE; // PING ATIVO
    Flag2 = FALSE; //I KNOW YOU, DO YOU KNOW ME?

    if(estadoConfiguracao != 0x00){
       
        Mode = START_CONN_ENERGY_SCN;
//        channelMap = 0x00020000;//channelMap = 0x000FF000;
        getChannel(&channelMap);
    }
    else{//nao

        Mode = START_CONN_ENERGY_SCN;//START_CONN_DIRECT;
//        setPANID(0x1234);
        channelMap = 0x00040000;
//        getChannel(&channelMap);
    }
//    Printf("\r\n Rand = ");
//    currentTime = MiWi_TickGet();
//    tickDifference.Val = MiWi_TickGetDiff(currentTime,eventTime);
//    srand(tickDifference.Val);
//    PrintDec(rand());


    Printf("\r\nStarting Testing Interface for MiWi(TM) PRO Stack ...");
    #if defined(MRF24J40)
    Printf("\r\n     RF Transceiver: MRF24J40");
    #elif defined(MRF49XA)
    Printf("\r\n     RF Transceiver: MRF49XA");
    #elif defined(MRF89XA)
    Printf("\r\n     RF Transceiver: MRF89XA");
    #endif
    Printf("\r\n   Demo Instruction:");
    Printf("\r\n                     Press Enter to bring up the menu.");
    Printf("\r\n                     Type in hyper terminal to choose");
    Printf("\r\n                     menu item. ");
    Printf("\r\n\r\n");

    if((MiApp_ProtocolInit(TRUE) == FALSE))
    {
        MiApp_ProtocolInit(FALSE);

        L2_RADIO = 1;
        L3_RADIO = 1;

        #ifdef ENABLE_ACTIVE_SCAN

            myChannel = 0xFF;
            ConsolePutROMString((ROM char *)"\r\nStarting Active Scan...");

            i = MiApp_SearchConnection(10, channelMap);

            if( i > 0 )
            {
                indiceMaiorPotencia = 0x00;
                sinalMaiorPotencia = 0x00;
                // now print out the scan result.
                Printf("\r\nActive Scan Results: \r\n");
                for(j = 0; j < i; j++)
                {
                    Printf("\n\r Channel: ");
                    PrintDec(ActiveScanResults[j].Channel );
                    Printf("   RSSI: ");
                    PrintChar(ActiveScanResults[j].RSSIValue);
                    if(ActiveScanResults[j].RSSIValue>= sinalMaiorPotencia){
                        sinalMaiorPotencia = ActiveScanResults[j].RSSIValue;
                        indiceMaiorPotencia = j;
                    }

                    myChannel = ActiveScanResults[j].Channel;
                    currentChannel = myChannel;
                    Printf("\n\rPeerInfo: ");
                    PrintChar( ActiveScanResults[j].PeerInfo[0]);
                    Printf("   PANID:  ");
                    PrintChar(ActiveScanResults[j].PANID.v[1]);
                    PrintChar(ActiveScanResults[j].PANID.v[0]);
                    Printf("\r\nAddress:  ");
                    PrintChar(ActiveScanResults[j].Address[1]);
                    PrintChar(ActiveScanResults[j].Address[0]);
                    Printf("\n\r");
                }
            }
        #endif

        MiApp_ConnectionMode(ENABLE_ALL_CONN);



        if( i > 0)
        {
            #ifndef CONFIGURADOR
                if( MiApp_EstablishConnection(indiceMaiorPotencia, CONN_MODE_DIRECT) == 0xFF )
                {
                    Printf("\r\nJoin Fail");
                }
            #else
                MiApp_StartConnection(Mode,9,channelMap);
            #endif
        }
        else
        {
            #ifdef CONFIGURADOR
                #ifdef ENABLE_ED_SCAN
                    MiApp_StartConnection(Mode, 9, channelMap);
                #endif
            #endif
        }

        // Turn on LED 1 to indicate ready to accept new connections
        L2_RADIO = 0;
    }
    else
    {
        Printf("\r\nNetwork Freezer Feature is enabled. There will be no hand-shake process.\r\n");
        L2_RADIO = 0;
        DumpConnection(0xFF);
    }

    while(1)
    {





        if( MiApp_MessageAvailable() )
        {
            /*******************************************************************/
            // If a packet has been received, following code prints out some of
            // the information available in rxFrame.
            /*******************************************************************/
            if( rxMessage.flags.bits.secEn )
            {
                ConsolePutROMString((ROM char *)"Secured ");
            }

            if( rxMessage.flags.bits.broadcast )
            {
                ConsolePutROMString((ROM char *)"Broadcast Packet with RSSI ");
            }
            else
            {
                ConsolePutROMString((ROM char *)"Unicast Packet with RSSI ");
            }

            PrintChar(rxMessage.PacketRSSI);

            if( rxMessage.flags.bits.srcPrsnt )
            {
                ConsolePutROMString((ROM char *)" from ");
                if( rxMessage.flags.bits.altSrcAddr )
                {
                    PrintChar(rxMessage.SourceAddress[1]);
                    PrintChar(rxMessage.SourceAddress[0]);
                }
                else
                {
                    for(i = 0; i < MY_ADDRESS_LENGTH; i++)
                    {
                        PrintChar(rxMessage.SourceAddress[MY_ADDRESS_LENGTH-1-i]);
                    }
                }
            }

//            ConsolePutROMString((ROM char *)": ");
//
//            for(i = 0; i < rxMessage.PayloadSize; i++)
//            {
//                ConsolePut(rxMessage.Payload[i]);
//            }

            // Toggle LED2 to indicate receiving a packet.
            L3_RADIO ^= 1;

            tratar_comandos_radio(rxMessage);

            MiApp_DiscardMessage();

            bReceivedMessage = TRUE;

        }//tratamento comunica��o radio
        #if defined(CONFIGURADOR)


            if(serial2_tem_dado_bufferRx() > 0){
                dado = serial2_retira_dado_bufferRx();
                ConsolePut(dado);
                trataPacotesVindosDaSerial(dado);
            }
        #else
           if(serial2_tem_dado_bufferRx())
            {
                aux = serial2_retira_dado_bufferRx();
                
                //VERIFICA SE O SOFTWARE DA MICROCHIP ESTA TENTANDO SE COMUNICAR
                if(aux == 'E'){
                    CALIBRACAO_ATIVADA = TRUE;
                }

                if(!CALIBRACAO_ATIVADA){
                    if(aux == 'C'){
                        configuraGRC_serial();
                    }
                    trataMenuDeEnvio(aux);
                }else{
                    serial_insere_dados_bufferTx(&aux,1);
                }

//
            }
            if(serial_tem_dado_bufferRx())
            {
                if(CALIBRACAO_ATIVADA){
                    aux = serial_retira_dado_bufferRx();
                    serial2_insere_dados_bufferTx(&aux,1);
                }
            }


        #endif
//        if(serial_tem_dado_bufferRx())
//        {
//            aux = serial_retira_dado_bufferRx();
//            serial2_insere_dados_bufferTx(&aux,1);
//        }

      /*
       * if(tickDifference.Val > (ONE_SECOND))
          {
              if(!DISJUNTOR_ARMADO)
                {*/
       
                 if(ativador_armar)
                 {
                    armaDisjuntor();
                 }
                    
          //      }
          //      else
          //      {
                  if(ativador_desarmar)
                  {
                    desarmaDisjuntor();
                  }
                   
          //     }
        //  }
      

        if(!CALIBRACAO_ATIVADA){
            currentTime = MiWi_TickGet();
            tickDifference.Val = MiWi_TickGetDiff(currentTime,eventTime);

           

            if(tickDifference.Val > (ONE_SECOND))
            {
              
                eventTime = currentTime;
                medidorGRC_atualiza_medidas();



//                Printf("\r\n");
//                Printf("MEDIDAS ATUALIZADAS");
//                Printf("\r\n");

            }

            medidorGRC_tarefas();
        }
        if(myShortAddress.v[0]==0xFF & myShortAddress.v[1]==0xFF ){
             L2_RADIO = 1;

             #ifdef CONFIGURADOR
                        #ifdef ENABLE_ED_SCAN
                            MiApp_StartConnection(START_CONN_ENERGY_SCN, 9, channelMap);//0x02000000);
                         #endif
             #else
                 i = MiApp_SearchConnection(9, channelMap);
                 if(i>0){
                    sinalMaiorPotencia = 0x00;
                    indiceMaiorPotencia = 0x00;
                     Printf("\r\nActive Scan Results: \r\n");
                    for(j = 0; j < i; j++)
                    {
                        
                        Printf("\n\r Channel: ");
                        PrintDec(ActiveScanResults[j].Channel );
                        Printf("   RSSI: ");
                        PrintChar(ActiveScanResults[j].RSSIValue);
                        if(ActiveScanResults[j].RSSIValue>= sinalMaiorPotencia){
                            sinalMaiorPotencia = ActiveScanResults[j].RSSIValue;
                            indiceMaiorPotencia = j;
                        }
                        myChannel = ActiveScanResults[j].Channel;
                        currentChannel = myChannel;
                        Printf("    PeerInfo:    ");
                        PrintChar( ActiveScanResults[j].PeerInfo[0]);
                        Printf("\n\rPANID:  ");
                        PrintChar(ActiveScanResults[j].PANID.v[1]);
                        PrintChar(ActiveScanResults[j].PANID.v[0]);
                        Printf("   Address:  ");
                        PrintChar(ActiveScanResults[j].Address[1]);
                        PrintChar(ActiveScanResults[j].Address[0]);
                        Printf("\n\r");

                    }
                     if(MiApp_EstablishConnection(indiceMaiorPotencia, CONN_MODE_DIRECT)){
                            L2_RADIO = 0;
                     }
                }//end if verifica��o de canais e teste de conex�o
            #endif
        }


        if (Flag) {

            MiApp_FlushTx();

            Flag = FALSE;

            for (index = 0; index < CONNECTION_SIZE; index++) {
                for (j = 0; j < 3; j++) {
                    if (ConnectionTable[index].status.bits.isValid) {
                        TxBuffer[0] = defaultHops;		    //number of hops
                        TxBuffer[1] = 0x02;		            //Frame Control
                        TxBuffer[2] = ConnectionTable[index].PANID.v[0];//sourcePANID.v[0];
                        TxBuffer[3] = ConnectionTable[index].PANID.v[1];//sourcePANID.v[1];
                        TxBuffer[4] = ConnectionTable[index].AltAddress.v[0]; //sourceShortAddress.v[0];
                        TxBuffer[5] = ConnectionTable[index].AltAddress.v[1]; //sourceShortAddress.v[1];
                        TxBuffer[6] = myPANID.v[0];			//source PANID LSB
                        TxBuffer[7] = myPANID.v[1];			//source PANID MSB
                        TxBuffer[8] = myShortAddress.v[0];	//source address LSB
                        TxBuffer[9] = myShortAddress.v[1];	//source address MSB
                        TxBuffer[10] = MiWiPROSeqNum++;   //sequence number
                        TxBuffer[11] = MIWI_PRO_STACK_REPORT_TYPE;
                        TxBuffer[12] = ACK_REPORT_TYPE;
                        TxData = 13;

                        MTP.flags.Val = 0;
                        MTP.flags.bits.ackReq = 1;

                        #if defined(IEEE_802_15_4)
                            MTP.altDestAddr = TRUE;
                            MTP.altSrcAddr = TRUE;
                            MTP.DestAddress = ConnectionTable[index].AltAddress.v;
                            MTP.DestPANID.Val = ConnectionTable[index].PANID.Val;
                        #else
                            tempShortAddress.Val = sourceShortAddress.Val;
                            tempPANID.Val = sourcePANID.Val;
                            if( (i = SearchForShortAddress()) != 0xFF )
                            {
                                MTP.DestAddress = ConnectionTable[i].Address;
                            }
                        #endif

                        if(MiMAC_SendPacket(MTP, TxBuffer, TxData)){
                            break;
                        } else {
                            if (j == 2) {
                                #if defined(CONFIGURADOR)
                                    MiApp_RemoveConnection(index);
                                #else
                                    if (index == myParent){
                                        Printf("\r\n excluindo");
                                        MiApp_RemoveConnection(0xFF);
                                        ler_Conf_Inicial2();
                                    }
                                    else
                                        MiApp_RemoveConnection(index);

                                #endif
                            }
                        }
                    }else {
                        if (index == 0x00) {
                            #if defined(CONFIGURADOR)
                                Printf("\n\r Vazio \n\r");
                            #else
                                MiApp_RemoveConnection(0xFF);
                                ler_Conf_Inicial2();
                            #endif

                        }
                    }
                }// end for para as tr�s tentativas de comunica��o

            }// end for para cada index da tabela

        }//end if manuten��o da pr�pria tabela

        if(Flag2){//I know you, do you know me?

            Flag2= FALSE;


            MiApp_FlushTx();
            MiApp_WriteData(0xE0);
            for (j=0; j< MY_ADDRESS_LENGTH; j++){
                MiApp_WriteData(myLongAddress[j]);
            }
            MiApp_WriteData(myShortAddress.v[0]);
            MiApp_WriteData(myShortAddress.v[1]);

            if(myParent!= 0xFF){
                MiApp_UnicastAddress(ConnectionTable[myParent].Address, TRUE, FALSE);
            }

        }// end if manuten��o da tabela do "parent"



        //       manter a rede
        CurrentTime_interrupt = MiWi_TickGet();

        TickDifference_interrupt.Val = MiWi_TickGetDiff(CurrentTime_interrupt,EventTime_interruptled);

        if(TickDifference_interrupt.Val > (ONE_SECOND))
        {
            L1_RADIO^=1;
            EventTime_interruptled = CurrentTime_interrupt;
//            tempShortAddress.v[0]=0x00;
//            tempShortAddress.v[1]=0x00;
//            if(MiApp_UnicastAddress(tempShortAddress.v,FALSE,FALSE)){//myParent == 0xFF){
//                L2_RADIO ^= 1;}
            if(myParent == 0xFF){
                L2_RADIO = 1;
            }else{
                L2_RADIO = 0;
            }
        }// end if temporizador led


         TickDifference_interrupt.Val = MiWi_TickGetDiff(CurrentTime_interrupt,EventTime_interrupt);
        #if defined(CONFIGURADOR)
            if(TickDifference_interrupt.Val > (ONE_MINUTE))
        #else
            if(TickDifference_interrupt.Val > (ONE_MINUTE))
        #endif
        {
            Flag=TRUE;
            EventTime_interrupt = CurrentTime_interrupt;
        }// end if temporizador para manuten��o da pr�pria tabela

        TickDifference_interrupt.Val = MiWi_TickGetDiff(CurrentTime_interrupt,EventTime_interrupt2);
        if(TickDifference_interrupt.Val > (2*ONE_MINUTE)){
            EventTime_interrupt2 = CurrentTime_interrupt;
            Flag2=TRUE;
        }// end if temporizador para manuten��o da tabela do "parent"

//        if(!PUSH_BUTTON){
//         #ifdef TESTE_RELE
//
//
//            CurrentTime_interrupt = MiWi_TickGet();
//
//            TickDifference_interrupt.Val = MiWi_TickGetDiff(CurrentTime_interrupt,EventTime_interruptrele);
//
//            if(TickDifference_interrupt.Val > (ONE_SECOND/2)){
//
//
//                EventTime_interruptrele = CurrentTime_interrupt;
//                if(rele){
//                    ativador_armar = 1;
//                    rele = FALSE;
//                    L2_RADIO =1;
//                    L3_RADIO =1;
//                    L1_RADIO =1;
//                }else{
//                    ativador_desarmar = 1;
//                    rele = TRUE;
//                    L2_RADIO = 0;
//                    L3_RADIO = 0;
//                    L1_RADIO = 0;
//                }
//
//            }
//        #endif
//        }// end if button debuger
        ClrWdt();

    } //end while



}