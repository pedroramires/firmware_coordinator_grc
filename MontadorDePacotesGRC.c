/*
 * File:   MontadorDePacoteGRC.c
 * Author: Ramires
 *
 * Created on 13 de Fevereiro de 2014, 15:46
 */

#include <stdio.h>
#include <stdlib.h>
#include "Console.h"
#include "MontadorDePacotesGRC.h"




static BYTE pacoteMontadoRequisicao[50], pacoteMontadoGrava[50];


void montaPacoteRespostaRequisitaVersaoFirmware(int32 idConcentrador,BYTE *idMedidor, BYTE *versaoFirmware,pacoteGRC *pacote  )
{
     conversor4 id_Concentrador;


    id_Concentrador.numero           = idConcentrador;



    pacoteMontadoGrava[0]  /*=pacote->getId[0]*/            = id_Concentrador.toArrayBytes[3];
    pacoteMontadoGrava[1]  /*=pacote->getId[1]*/            = id_Concentrador.toArrayBytes[2];
    pacoteMontadoGrava[2]  /*=pacote->getId[2]*/            = id_Concentrador.toArrayBytes[1];
    pacoteMontadoGrava[3]  /*=pacote->getId[3]*/            = id_Concentrador.toArrayBytes[0];

    pacoteMontadoGrava[4] = pacote->getFuncao           = REQUISITAR_VERSAO_FIRMWARE;

    pacoteMontadoGrava[5]    = 12;
    pacote->getTamanho       = 12;

    pacoteMontadoGrava[6]   =  idMedidor[0];
    pacoteMontadoGrava[7]   =  idMedidor[1];
    pacoteMontadoGrava[8]   =  idMedidor[2];
    pacoteMontadoGrava[9]   =  idMedidor[3];
    pacoteMontadoGrava[10]  =  idMedidor[4];
    pacoteMontadoGrava[11]  =  idMedidor[5];
    pacoteMontadoGrava[12]  =  idMedidor[6];
    pacoteMontadoGrava[13]  =  idMedidor[7];

    pacoteMontadoGrava[14]   = versaoFirmware[3];
    pacoteMontadoGrava[15]   = versaoFirmware[2];
    pacoteMontadoGrava[16]   = versaoFirmware[1];
    pacoteMontadoGrava[17]   = versaoFirmware[0];

    pacote->getPacoteMontado = pacoteMontadoGrava;

}
void montaPacoteRespostaRequisitaInfoMedidor(int32 idConcentrador, BYTE *idMedidor,BYTE *numeroDaCasa, BYTE *unidadeConsumidora, BYTE* latitude, BYTE* longitude, pacoteGRC *pacote)
{
    conversor4 id_Concentrador;

    id_Concentrador.numero           = idConcentrador;

    pacoteMontadoGrava[0] = pacote->getId[0]            = id_Concentrador.toArrayBytes[3];
    pacoteMontadoGrava[1] = pacote->getId[1]            = id_Concentrador.toArrayBytes[2];
    pacoteMontadoGrava[2] = pacote->getId[2]            = id_Concentrador.toArrayBytes[1];
    pacoteMontadoGrava[3] = pacote->getId[3]            = id_Concentrador.toArrayBytes[0];

    pacoteMontadoGrava[4] = pacote->getFuncao           = REQUISITAR_INFO_MEDIDOR;

    pacoteMontadoGrava[5] = pacote->getTamanho          = 28;

    pacoteMontadoGrava[6]   =  idMedidor[0];
    pacoteMontadoGrava[7]   =  idMedidor[1];
    pacoteMontadoGrava[8]   =  idMedidor[2];
    pacoteMontadoGrava[9]   =  idMedidor[3];
    pacoteMontadoGrava[10]  =  idMedidor[4];
    pacoteMontadoGrava[11]  =  idMedidor[5];
    pacoteMontadoGrava[12]  =  idMedidor[6];
    pacoteMontadoGrava[13]  =  idMedidor[7];

    pacoteMontadoGrava[14]  = numeroDaCasa[0];
    pacoteMontadoGrava[15]  = numeroDaCasa[1];
    pacoteMontadoGrava[16] = numeroDaCasa[2];
    pacoteMontadoGrava[17] = numeroDaCasa[3];
    pacoteMontadoGrava[18] = numeroDaCasa[4];
    pacoteMontadoGrava[19] = numeroDaCasa[5];
    pacoteMontadoGrava[20] = numeroDaCasa[6];
    pacoteMontadoGrava[21] = numeroDaCasa[7];

    pacoteMontadoGrava[22]  = unidadeConsumidora[0];
    pacoteMontadoGrava[23]  = unidadeConsumidora[1];
    pacoteMontadoGrava[24]  = unidadeConsumidora[2];
    pacoteMontadoGrava[25]  = unidadeConsumidora[3];
    pacoteMontadoGrava[26]  = unidadeConsumidora[4];
    pacoteMontadoGrava[27]  = unidadeConsumidora[5];
    pacoteMontadoGrava[28]  = unidadeConsumidora[6];
    pacoteMontadoGrava[29]  = unidadeConsumidora[7];

    pacoteMontadoGrava[30]  = latitude[0];
    pacoteMontadoGrava[31]  = latitude[1];
    pacoteMontadoGrava[32]  = longitude[0];
    pacoteMontadoGrava[33]  = longitude[1];

    pacote->getPacoteMontado = pacoteMontadoGrava;

}

void montaPacoteRespostaRequisitaData_Hora(int32 idConcentrador,BYTE *idMedidor,  BYTE *dataHora, pacoteGRC *pacote )
{
    conversor4 id_Concentrador;
    id_Concentrador.numero           = idConcentrador;


    pacoteMontadoGrava[0] = pacote->getId[0]            = id_Concentrador.toArrayBytes[3];
    pacoteMontadoGrava[1] = pacote->getId[1]            = id_Concentrador.toArrayBytes[2];
    pacoteMontadoGrava[2] = pacote->getId[2]            = id_Concentrador.toArrayBytes[1];
    pacoteMontadoGrava[3] = pacote->getId[3]            = id_Concentrador.toArrayBytes[0];

    pacoteMontadoGrava[4] = pacote->getFuncao           = REQUISITAR_DATA_HORA_MEDIDOR;

    pacoteMontadoGrava[5] = pacote->getTamanho          = 14;

    pacoteMontadoGrava[6]   =  idMedidor[0];
    pacoteMontadoGrava[7]   =  idMedidor[1];
    pacoteMontadoGrava[8]   =  idMedidor[2];
    pacoteMontadoGrava[9]   =  idMedidor[3];
    pacoteMontadoGrava[10]  =  idMedidor[4];
    pacoteMontadoGrava[11]  =  idMedidor[5];
    pacoteMontadoGrava[12]  =  idMedidor[6];
    pacoteMontadoGrava[13]  =  idMedidor[7];

    pacoteMontadoGrava[14]  = dataHora[0];
    pacoteMontadoGrava[15]  = dataHora[1];
    pacoteMontadoGrava[16]  = dataHora[2];
    pacoteMontadoGrava[17]  = dataHora[3];
    pacoteMontadoGrava[18]  = dataHora[4];
    pacoteMontadoGrava[19]  = dataHora[5];

    pacote->getPacoteMontado = pacoteMontadoGrava;
}

void montaPacoteRespostaRequisitaEnergiaMedidor(int32 idConcentrador,BYTE *idMedidor,BYTE *horaDoAferimento, BYTE *energia, pacoteGRC *pacote )
{
    conversor4 id_Concentrador, energia_v;
    conversor2 id_Medidor;
    id_Concentrador.numero           = idConcentrador;
    //energia_v.numero                 = energia;

    pacoteMontadoGrava[0] = /*pacote->getId[0]            =*/ id_Concentrador.toArrayBytes[3];
    pacoteMontadoGrava[1] = /*pacote->getId[1]            =*/ id_Concentrador.toArrayBytes[2];
    pacoteMontadoGrava[2] = /*pacote->getId[2]            =*/ id_Concentrador.toArrayBytes[1];
    pacoteMontadoGrava[3] = /*pacote->getId[3]            =*/ id_Concentrador.toArrayBytes[0];

    pacoteMontadoGrava[4] = pacote->getFuncao             = REQUISITAR_ENERGIA_MEDIDOR;

    pacoteMontadoGrava[5] = pacote->getTamanho            = 18;

    pacoteMontadoGrava[6]   =  idMedidor[0];
    pacoteMontadoGrava[7]   =  idMedidor[1];
    pacoteMontadoGrava[8]   =  idMedidor[2];
    pacoteMontadoGrava[9]   =  idMedidor[3];
    pacoteMontadoGrava[10]  =  idMedidor[4];
    pacoteMontadoGrava[11]  =  idMedidor[5];
    pacoteMontadoGrava[12]  =  idMedidor[6];
    pacoteMontadoGrava[13]  =  idMedidor[7];

    pacoteMontadoGrava[14]  = energia[0];
    pacoteMontadoGrava[15]  = energia[1];
    pacoteMontadoGrava[16]  = energia[2];
    pacoteMontadoGrava[17]  = energia[3];

    pacoteMontadoGrava[18]  = horaDoAferimento[0];
    pacoteMontadoGrava[19]  = horaDoAferimento[1];
    pacoteMontadoGrava[20]  = horaDoAferimento[2];
    pacoteMontadoGrava[21]  = horaDoAferimento[3];
    pacoteMontadoGrava[22]  = horaDoAferimento[4];
    pacoteMontadoGrava[23]  = horaDoAferimento[5];
   

    pacote->getPacoteMontado = pacoteMontadoGrava;
}

void montaPacoteRespostaGravaInfoMedidor(int32 idConcentrador,BYTE *idMedidor,BOOL retorno, pacoteGRC *pacote )
{
    conversor4 id_Concentrador;
    conversor2 id_Medidor;
    id_Concentrador.numero           = idConcentrador;

    pacoteMontadoGrava[0] = pacote->getId[0]            = id_Concentrador.toArrayBytes[3];
    pacoteMontadoGrava[1] = pacote->getId[1]            = id_Concentrador.toArrayBytes[2];
    pacoteMontadoGrava[2] = pacote->getId[2]            = id_Concentrador.toArrayBytes[1];
    pacoteMontadoGrava[3] = pacote->getId[3]            = id_Concentrador.toArrayBytes[0];

    pacoteMontadoGrava[4] = pacote->getFuncao           = GRAVAR_INFO_MEDIDOR;

    pacoteMontadoGrava[5] = pacote->getTamanho          = 9;

    pacoteMontadoGrava[6]   =  idMedidor[0];
    pacoteMontadoGrava[7]   =  idMedidor[1];
    pacoteMontadoGrava[8]   =  idMedidor[2];
    pacoteMontadoGrava[9]   =  idMedidor[3];
    pacoteMontadoGrava[10]  =  idMedidor[4];
    pacoteMontadoGrava[11]  =  idMedidor[5];
    pacoteMontadoGrava[12]  =  idMedidor[6];
    pacoteMontadoGrava[13]  =  idMedidor[7];

    pacoteMontadoGrava[14]  = retorno;

     pacote->getPacoteMontado = pacoteMontadoGrava;

}

void montaPacoteRespostaGravaData_HoraMedidor(int32 idConcentrador,BYTE *idMedidor, BYTE retorno, pacoteGRC *pacote )
{
    conversor4 id_Concentrador;
    conversor2 id_Medidor;
    id_Concentrador.numero           = idConcentrador;
    pacoteMontadoGrava[0] = pacote->getId[0]            = id_Concentrador.toArrayBytes[3];
    pacoteMontadoGrava[1] = pacote->getId[1]            = id_Concentrador.toArrayBytes[2];
    pacoteMontadoGrava[2] = pacote->getId[2]            = id_Concentrador.toArrayBytes[1];
    pacoteMontadoGrava[3] = pacote->getId[3]            = id_Concentrador.toArrayBytes[0];

    pacoteMontadoGrava[4] = pacote->getFuncao           = GRAVAR_DATA_HORA_MEDIDOR;

    pacoteMontadoGrava[5] = pacote->getTamanho          = 9;

    pacoteMontadoGrava[6]   =  idMedidor[0];
    pacoteMontadoGrava[7]   =  idMedidor[1];
    pacoteMontadoGrava[8]   =  idMedidor[2];
    pacoteMontadoGrava[9]   =  idMedidor[3];
    pacoteMontadoGrava[10]  =  idMedidor[4];
    pacoteMontadoGrava[11]  =  idMedidor[5];
    pacoteMontadoGrava[12]  =  idMedidor[6];
    pacoteMontadoGrava[13]  =  idMedidor[7];

    pacoteMontadoGrava[14]  = retorno;

     pacote->getPacoteMontado = pacoteMontadoGrava;



}

void montaPacoteRespostaRequisitaDadosDeMedicaoUC(int32 idConcentrador,BYTE *idMedidor, BYTE *tensaoFase, BYTE *correnteFase, BYTE *potenciaNominal, pacoteGRC *pacote)
{

    conversor4 id_Concentrador, potenciaNominalUC;
    conversor2 id_Medidor;
    int count;

    id_Concentrador.numero           = idConcentrador;
//    tensaoDaFase.numero              = tensaoFase;
//    correnteDafase.numero            = correnteFase;
//   potenciaNominalUC.numero         = potenciaNominal;

    pacoteMontadoGrava[0] = pacote->getId[0]            = id_Concentrador.toArrayBytes[3];
    pacoteMontadoGrava[1] = pacote->getId[1]            = id_Concentrador.toArrayBytes[2];
    pacoteMontadoGrava[2] = pacote->getId[2]            = id_Concentrador.toArrayBytes[1];
    pacoteMontadoGrava[3] = pacote->getId[3]            = id_Concentrador.toArrayBytes[0];

    pacoteMontadoGrava[4] = pacote->getFuncao           = REQUISITAR_DADOS_MEDICAO_UC;

    pacoteMontadoGrava[5] = pacote->getTamanho          = 16;

    pacoteMontadoGrava[6]   =  idMedidor[0];
    pacoteMontadoGrava[7]   =  idMedidor[1];
    pacoteMontadoGrava[8]   =  idMedidor[2];
    pacoteMontadoGrava[9]   =  idMedidor[3];
    pacoteMontadoGrava[10]  =  idMedidor[4];
    pacoteMontadoGrava[11]  =  idMedidor[5];
    pacoteMontadoGrava[12]  =  idMedidor[6];
    pacoteMontadoGrava[13]  =  idMedidor[7];

    pacoteMontadoGrava[14]  = tensaoFase[0];
    pacoteMontadoGrava[15]  = tensaoFase[1];

    pacoteMontadoGrava[16]  = correnteFase[0];
    pacoteMontadoGrava[17]  = correnteFase[1];

    pacoteMontadoGrava[18]  = potenciaNominal[0];
    pacoteMontadoGrava[19]  = potenciaNominal[1];
    pacoteMontadoGrava[20]  = potenciaNominal[2];
    pacoteMontadoGrava[21]  = potenciaNominal[3];

    pacote->getPacoteMontado = pacoteMontadoGrava;


}

void montaPacoteRespostaArmaRele(int32 idConcentrador,BYTE *idMedidor, BYTE estadoRele, pacoteGRC *pacote)
{
     conversor4 id_Concentrador;
    conversor2 id_Medidor;
    id_Concentrador.numero           = idConcentrador;

    pacoteMontadoGrava[0] = pacote->getId[0]            = id_Concentrador.toArrayBytes[3];
    pacoteMontadoGrava[1] = pacote->getId[1]            = id_Concentrador.toArrayBytes[2];
    pacoteMontadoGrava[2] = pacote->getId[2]            = id_Concentrador.toArrayBytes[1];
    pacoteMontadoGrava[3] = pacote->getId[3]            = id_Concentrador.toArrayBytes[0];

    pacoteMontadoGrava[4] = pacote->getFuncao           = FUNCAO_REQ_ARMA_RELE;

    pacoteMontadoGrava[5] = pacote->getTamanho          = 1;

    pacoteMontadoGrava[6]   =  idMedidor[0];
    pacoteMontadoGrava[7]   =  idMedidor[1];
    pacoteMontadoGrava[8]   =  idMedidor[2];
    pacoteMontadoGrava[9]   =  idMedidor[3];
    pacoteMontadoGrava[10]  =  idMedidor[4];
    pacoteMontadoGrava[11]  =  idMedidor[5];
    pacoteMontadoGrava[12]  =  idMedidor[6];
    pacoteMontadoGrava[13]  =  idMedidor[7];

    pacoteMontadoGrava[14]  = estadoRele;

     pacote->getPacoteMontado = pacoteMontadoGrava;
}


void montaPacoteRespostaDesarmaRele(int32 idConcentrador,BYTE *idMedidor, BYTE estadoRele, pacoteGRC *pacote)
{
     conversor4 id_Concentrador;
    conversor2 id_Medidor;
    id_Concentrador.numero           = idConcentrador;

    pacoteMontadoGrava[0] = pacote->getId[0]            = id_Concentrador.toArrayBytes[3];
    pacoteMontadoGrava[1] = pacote->getId[1]            = id_Concentrador.toArrayBytes[2];
    pacoteMontadoGrava[2] = pacote->getId[2]            = id_Concentrador.toArrayBytes[1];
    pacoteMontadoGrava[3] = pacote->getId[3]            = id_Concentrador.toArrayBytes[0];

    pacoteMontadoGrava[4] = pacote->getFuncao           = FUNCAO_REQ_DESARMA_RELE;

    pacoteMontadoGrava[5] = pacote->getTamanho          = 1;

    pacoteMontadoGrava[6]   =  idMedidor[0];
    pacoteMontadoGrava[7]   =  idMedidor[1];
    pacoteMontadoGrava[8]   =  idMedidor[2];
    pacoteMontadoGrava[9]   =  idMedidor[3];
    pacoteMontadoGrava[10]  =  idMedidor[4];
    pacoteMontadoGrava[11]  =  idMedidor[5];
    pacoteMontadoGrava[12]  =  idMedidor[6];
    pacoteMontadoGrava[13]  =  idMedidor[7];

    pacoteMontadoGrava[14]  = estadoRele;

     pacote->getPacoteMontado = pacoteMontadoGrava;
}

void montaPacoteRespostaZeraEnergiaMedidor(int32 idConcentrador,BYTE *idMedidor, BYTE estadoEnergia, pacoteGRC *pacote)
{
     conversor4 id_Concentrador;
    conversor2 id_Medidor;
    id_Concentrador.numero           = idConcentrador;

    pacoteMontadoGrava[0] = pacote->getId[0]            = id_Concentrador.toArrayBytes[3];
    pacoteMontadoGrava[1] = pacote->getId[1]            = id_Concentrador.toArrayBytes[2];
    pacoteMontadoGrava[2] = pacote->getId[2]            = id_Concentrador.toArrayBytes[1];
    pacoteMontadoGrava[3] = pacote->getId[3]            = id_Concentrador.toArrayBytes[0];

    pacoteMontadoGrava[4] = pacote->getFuncao           = FUNCAO_ZERA_ENERGIA_MEDIDOR;

    pacoteMontadoGrava[5] = pacote->getTamanho          = 1;

    pacoteMontadoGrava[6]   =  idMedidor[0];
    pacoteMontadoGrava[7]   =  idMedidor[1];
    pacoteMontadoGrava[8]   =  idMedidor[2];
    pacoteMontadoGrava[9]   =  idMedidor[3];
    pacoteMontadoGrava[10]  =  idMedidor[4];
    pacoteMontadoGrava[11]  =  idMedidor[5];
    pacoteMontadoGrava[12]  =  idMedidor[6];
    pacoteMontadoGrava[13]  =  idMedidor[7];

    pacoteMontadoGrava[14]  = estadoEnergia;

     pacote->getPacoteMontado = pacoteMontadoGrava;
}


void montaPacoteRespostaRequisitaTotalData(int32 idConcentrador, BYTE* idMedidor,BYTE* tensaoFase, BYTE* correnteFase,BYTE* potenciaAtiva,BYTE* energiaMedidor,BYTE* dataHoraMedidor, BYTE* fatorPotenciaMedidor,pacoteGRC *pacote){
    conversor4 id_Concentrador, potenciaNominalUC;
    conversor2 id_Medidor;
    int count;

    id_Concentrador.numero           = idConcentrador;
//    tensaoDaFase.numero              = tensaoFase;
//    correnteDafase.numero            = correnteFase;
//   potenciaNominalUC.numero         = potenciaNominal;

    pacoteMontadoGrava[0] = pacote->getId[0]            = id_Concentrador.toArrayBytes[3];
    pacoteMontadoGrava[1] = pacote->getId[1]            = id_Concentrador.toArrayBytes[2];
    pacoteMontadoGrava[2] = pacote->getId[2]            = id_Concentrador.toArrayBytes[1];
    pacoteMontadoGrava[3] = pacote->getId[3]            = id_Concentrador.toArrayBytes[0];

    pacoteMontadoGrava[4] = pacote->getFuncao           = FUNCAO_REQ_TOTAL_DATA;

    pacoteMontadoGrava[5] = pacote->getTamanho          = 28;

    pacoteMontadoGrava[6]   =  idMedidor[0];
    pacoteMontadoGrava[7]   =  idMedidor[1];
    pacoteMontadoGrava[8]   =  idMedidor[2];
    pacoteMontadoGrava[9]   =  idMedidor[3];
    pacoteMontadoGrava[10]  =  idMedidor[4];
    pacoteMontadoGrava[11]  =  idMedidor[5];
    pacoteMontadoGrava[12]  =  idMedidor[6];
    pacoteMontadoGrava[13]  =  idMedidor[7];

    pacoteMontadoGrava[14]  = tensaoFase[0];
    pacoteMontadoGrava[15]  = tensaoFase[1];

    pacoteMontadoGrava[16]  = correnteFase[0];
    pacoteMontadoGrava[17]  = correnteFase[1];

    pacoteMontadoGrava[18]  = potenciaAtiva[0];
    pacoteMontadoGrava[19]  = potenciaAtiva[1];
    pacoteMontadoGrava[20]  = potenciaAtiva[2];
    pacoteMontadoGrava[21]  = potenciaAtiva[3];

    pacoteMontadoGrava[22]  = energiaMedidor[0];
    pacoteMontadoGrava[23]  = energiaMedidor[1];
    pacoteMontadoGrava[24]  = energiaMedidor[2];
    pacoteMontadoGrava[25]  = energiaMedidor[3];

    pacoteMontadoGrava[26]  = dataHoraMedidor[0];
    pacoteMontadoGrava[27]  = dataHoraMedidor[1];
    pacoteMontadoGrava[28]  = dataHoraMedidor[2];
    pacoteMontadoGrava[29]  = dataHoraMedidor[3];
    pacoteMontadoGrava[30]  = dataHoraMedidor[4];
    pacoteMontadoGrava[31]  = dataHoraMedidor[5];

    pacoteMontadoGrava[32]  = fatorPotenciaMedidor[0];
    pacoteMontadoGrava[33]  = fatorPotenciaMedidor[1];


    pacote->getPacoteMontado = pacoteMontadoGrava;
}