/*
 * File:   InfoMedidor.c
 * Author: Ramires
 *
 * Created on 19 de Fevereiro de 2014, 10:38
 */

#include <stdio.h>
#include <stdlib.h>
#include "InfoMedidor.h"
#include "NVM.h"



BYTE* getIDMedidor(){
    BYTE valor[ID_MEDIDOR_SIZE];
    nvmGetIdMedidor(valor);
    return valor;
}

void  getVersaoDoFirmware(BYTE *valor){
    nvmGetVersaoFirmware(valor);
}

void   getNumeroDaCasa(BYTE* retorno){
    nvmGetNumeroDaCasa(retorno);
}

void   getUnidadeConsumidora(BYTE* retorno){
    nvmGetUnidadeConsumidora(retorno);
}

void   getLatitudeMedidor(BYTE* valor){
    nvmGetLatitudeMedidor(valor);
}

void   getLongitudeMedidor(BYTE* valor){
    nvmGetLongitudeMedidor(valor);
}

void   getDataHoraMedidor(BYTE* retorno){
    nvmGetDataHoraMedidor(retorno);
}

void  getEnergia(BYTE* energia){
    //DWORD valor;
    nvmGetEnergia(energia);
    //return valor;
}

void   getDataHoraDaMedicaoDeEnergia(BYTE* retorno){
    nvmGetDataHoraDaMedicaoDeEnergia(retorno);
}

BYTE   getRetornoInfoMedidor(){
    BYTE valor;
    nvmGetRetornoInfoMedidor(valor);
    return valor;
}

BYTE   getRetornoDataHoraMedidor(){
    BYTE valor;
    nvmGetRetornoDataHoraMedidor(valor);
    return valor;
}

void   getTensaoFaseMedidor(BYTE *retorno){
    nvmGetTensaoFaseMedidor(retorno);
}

void   getCorrenteFaseMedidor(WORD retorno){
    nvmGetCorrenteFaseMedidor(retorno);
}

void   getDatahoraDaMedicao(BYTE* retorno){
    nvmGetDataHoraDaMedicao(retorno);
}



void  getPotenciaAtiva(BYTE *valor){
    nvmGetPotenciaAtiva(valor);

}

WORD   getGeoLocalizacao(){
    WORD valor;
    nvmGetGeoLocalizacao(valor);
    return valor;
}


BYTE   getEvento(){
    BYTE valor;
    nvmGetEvento(valor);
    return valor;
}



BYTE   getSensorTemperatura(){
    BYTE valor;
    nvmGetSensorTemperatura(valor);
    return valor;
}

BYTE   getSensorBateria(){
    BYTE valor;
    nvmGetSensorBateria(valor);
    return valor;
}

BOOL   gravaInfoMedidor(BYTE* idMedidor, BYTE* numeroDaCasa, BYTE* unidadeConsumidora, BYTE* latitude, BYTE* longitude)
{

    nvmPutNumeroDaCasa(numeroDaCasa);
    nvmPutUnidadeConsumidora(unidadeConsumidora);
    nvmPutLatitudeMedidor(latitude);
    nvmPutLongitudeMedidor(longitude);

}

BYTE   gravaDataHoraMedidor(BYTE* idMedidor, BYTE* dataHora)
{
    gravaHoraMedidorGRC(dataHora);
    return 1;
    //nvmPutDataHoraMedidor(dataHora);


}