/*
 * File:   TratadorDePacotes.c
 * Author: Ramires
 *
 * Created on 17 de Fevereiro de 2014, 14:30
 */

#include <stdio.h>
#include <stdlib.h>
#include "TratadorDePacotes.h"
#include "MontadorDePacotesGRC.h"
#include "InfoMedidor.h"
#include "Microchip/Include/WirelessProtocols/Console.h"
#include "WirelessProtocols/MCHP_API.h"
#include "acessaIdentificadores.h"
#include "HardwareProfile.h"
#include "medidorGRC.h"

/*
 *
 */


extern int16 ativador_armar;
extern int16 ativador_desarmar;

static BYTE bufferDataHora[6],numero_Casa[8], unidade_Consumidora[8];

static BYTE energiaTotal[4];

extern DWORD GRCEnergia;


extern BYTE                medidorEnergia[4];
extern BYTE                medidorDataHoraMedidor[6];
extern BYTE                medidorTensaoFaseMedidor[2];
extern BYTE                medidorCorrenteFaseMedidor[2];
extern BYTE                medidorPotenciaAtiva[4];







void trataPacoteRequisitaVersaoFirmware(BYTE *bufferDeRecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote)
{
   int i = 0;
   BYTE versaoFirmware[4], id_Medidor[8];
   conversor4 id_Concentrador;
   Printf("TRATADOR DE PACOTE REQUISITA VERS�O DO FIRMWARE!");
   id_Concentrador.toArrayBytes[0] = bufferDeRecepcao[3];
   id_Concentrador.toArrayBytes[1] = bufferDeRecepcao[2];
   id_Concentrador.toArrayBytes[2] = bufferDeRecepcao[1];
   id_Concentrador.toArrayBytes[3] = bufferDeRecepcao[0];

   id_Medidor[0] = rxMessage.Payload[7];
   id_Medidor[1] = rxMessage.Payload[8];
   id_Medidor[2] = rxMessage.Payload[9];
   id_Medidor[3] = rxMessage.Payload[10];
   id_Medidor[4] = rxMessage.Payload[11];
   id_Medidor[5] = rxMessage.Payload[12];
   id_Medidor[6] = rxMessage.Payload[13];
   id_Medidor[7] = rxMessage.Payload[14];

   getVersaoDoFirmware(versaoFirmware);

   montaPacoteRespostaRequisitaVersaoFirmware(id_Concentrador.numero,id_Medidor,versaoFirmware, pacote);

}

void trataPacoteRequisitaInfoMedidor(BYTE *bufferDeRecepcao,RECEIVED_MESSAGE  rxMessage,pacoteGRC *pacote)
{
   conversor4 id_Concentrador;
   BYTE id_Medidor[8],latitude[2],longitude[2];

   id_Concentrador.toArrayBytes[0] = bufferDeRecepcao[3];
   id_Concentrador.toArrayBytes[1] = bufferDeRecepcao[2];
   id_Concentrador.toArrayBytes[2] = bufferDeRecepcao[1];
   id_Concentrador.toArrayBytes[3] = bufferDeRecepcao[0];

   id_Medidor[0] = rxMessage.Payload[7];
   id_Medidor[1] = rxMessage.Payload[8];
   id_Medidor[2] = rxMessage.Payload[9];
   id_Medidor[3] = rxMessage.Payload[10];
   id_Medidor[4] = rxMessage.Payload[11];
   id_Medidor[5] = rxMessage.Payload[12];
   id_Medidor[6] = rxMessage.Payload[13];
   id_Medidor[7] = rxMessage.Payload[14];

   getNumeroDaCasa(numero_Casa);
   getUnidadeConsumidora(unidade_Consumidora);
   getLatitudeMedidor(latitude);
   getLongitudeMedidor(longitude);

   montaPacoteRespostaRequisitaInfoMedidor(id_Concentrador.numero,id_Medidor,numero_Casa,unidade_Consumidora,latitude, longitude,pacote);

}

void trataPacoteRequisitaData_Hora(BYTE *bufferDeRecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote)
{
   conversor4 id_Concentrador;
   BYTE id_Medidor[8];
   id_Concentrador.toArrayBytes[0] = bufferDeRecepcao[3];
   id_Concentrador.toArrayBytes[1] = bufferDeRecepcao[2];
   id_Concentrador.toArrayBytes[2] = bufferDeRecepcao[1];
   id_Concentrador.toArrayBytes[3] = bufferDeRecepcao[0];

   id_Medidor[0] = rxMessage.Payload[7];
   id_Medidor[1] = rxMessage.Payload[8];
   id_Medidor[2] = rxMessage.Payload[9];
   id_Medidor[3] = rxMessage.Payload[10];
   id_Medidor[4] = rxMessage.Payload[11];
   id_Medidor[5] = rxMessage.Payload[12];
   id_Medidor[6] = rxMessage.Payload[13];
   id_Medidor[7] = rxMessage.Payload[14];

//   getDataHoraMedidor(bufferDataHora);
//   montaPacoteRespostaRequisitaData_Hora(id_Concentrador.numero, id_Medidor,bufferDataHora, pacote);
   
   montaPacoteRespostaRequisitaData_Hora(id_Concentrador.numero, id_Medidor,medidorDataHoraMedidor, pacote);

}


void trataPacoteRequisitaEnergiaMedidor(BYTE *bufferDeRecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote)
{
   conversor4 id_Concentrador;
   int16 count;
   BYTE id_Medidor[8];
   DWORD aux;
   id_Concentrador.toArrayBytes[0] = bufferDeRecepcao[3];
   id_Concentrador.toArrayBytes[1] = bufferDeRecepcao[2];
   id_Concentrador.toArrayBytes[2] = bufferDeRecepcao[1];
   id_Concentrador.toArrayBytes[3] = bufferDeRecepcao[0];

   id_Medidor[0] = rxMessage.Payload[7];
   id_Medidor[1] = rxMessage.Payload[8];
   id_Medidor[2] = rxMessage.Payload[9];
   id_Medidor[3] = rxMessage.Payload[10];
   id_Medidor[4] = rxMessage.Payload[11];
   id_Medidor[5] = rxMessage.Payload[12];
   id_Medidor[6] = rxMessage.Payload[13];
   id_Medidor[7] = rxMessage.Payload[14];

//   getEnergia(energiaTotal);
//   getDataHoraMedidor(bufferDataHora);
//   montaPacoteRespostaRequisitaEnergiaMedidor(id_Concentrador.numero, id_Medidor,bufferDataHora,energiaTotal,pacote);
   aux = getLeituraEnergia();
   GRCEnergia = make32(medidorEnergia[3], medidorEnergia[2], medidorEnergia[1], medidorEnergia[0]);
   GRCEnergia = GRCEnergia + aux;
   montaPacoteRespostaRequisitaEnergiaMedidor(id_Concentrador.numero, id_Medidor,medidorDataHoraMedidor,GRCEnergia,pacote);


}

void trataPacoteGravaInfoMedidor(BYTE *bufferDeRecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote){

   conversor4 id_Concentrador;
   conversor2 latitude,longitude;
   BOOL retorno;
   BYTE numeroDaCasa[8], unidadeConsumidora[8], id_Medidor[8];

   id_Concentrador.toArrayBytes[0] = bufferDeRecepcao[3];
   id_Concentrador.toArrayBytes[1] = bufferDeRecepcao[2];
   id_Concentrador.toArrayBytes[2] = bufferDeRecepcao[1];
   id_Concentrador.toArrayBytes[3] = bufferDeRecepcao[0];

   id_Medidor[0] = rxMessage.Payload[7];
   id_Medidor[1] = rxMessage.Payload[8];
   id_Medidor[2] = rxMessage.Payload[9];
   id_Medidor[3] = rxMessage.Payload[10];
   id_Medidor[4] = rxMessage.Payload[11];
   id_Medidor[5] = rxMessage.Payload[12];
   id_Medidor[6] = rxMessage.Payload[13];
   id_Medidor[7] = rxMessage.Payload[14];

   numeroDaCasa[0]  = rxMessage.Payload[15];
   numeroDaCasa[1]  = rxMessage.Payload[16];
   numeroDaCasa[2]  = rxMessage.Payload[17];
   numeroDaCasa[3]  = rxMessage.Payload[18];
   numeroDaCasa[4]  = rxMessage.Payload[19];
   numeroDaCasa[5]  = rxMessage.Payload[20];
   numeroDaCasa[6]  = rxMessage.Payload[21];
   numeroDaCasa[7]  = rxMessage.Payload[22];

   unidadeConsumidora[0]  = rxMessage.Payload[23];
   unidadeConsumidora[1]  = rxMessage.Payload[24];
   unidadeConsumidora[2]  = rxMessage.Payload[25];
   unidadeConsumidora[3]  = rxMessage.Payload[26];
   unidadeConsumidora[4]  = rxMessage.Payload[27];
   unidadeConsumidora[5]  = rxMessage.Payload[28];
   unidadeConsumidora[6]  = rxMessage.Payload[29];
   unidadeConsumidora[7]  = rxMessage.Payload[30];

   latitude.toArrayBytes[0] = rxMessage.Payload[31];
   latitude.toArrayBytes[1] = rxMessage.Payload[32];

   Printf("\r\n LATITUDE\r\n");
   PrintDec(latitude.toArrayBytes[0]);
   PrintDec(latitude.toArrayBytes[1]);
   Printf("\r\n");

   longitude.toArrayBytes[0] = rxMessage.Payload[33];
   longitude.toArrayBytes[1] = rxMessage.Payload[34];

   Printf("\r\n LONGITUDE\r\n");
   PrintDec(longitude.toArrayBytes[0]);
   PrintDec(longitude.toArrayBytes[1]);
   Printf("\r\n");

   retorno = gravaInfoMedidor(id_Medidor,numeroDaCasa,unidadeConsumidora
             ,latitude.toArrayBytes,longitude.toArrayBytes);

   montaPacoteRespostaGravaInfoMedidor(id_Concentrador.numero,id_Medidor,retorno,pacote);
}

void trataPacoteGravaData_HoraMedidor(BYTE *bufferDeRecepcao, RECEIVED_MESSAGE  rxMessage,pacoteGRC *pacote)
{
   conversor4 id_Concentrador;
   BYTE id_Medidor[8];
   BYTE dataHora[8];
   BYTE retorno;

   id_Concentrador.toArrayBytes[0] = bufferDeRecepcao[3];
   id_Concentrador.toArrayBytes[1] = bufferDeRecepcao[2];
   id_Concentrador.toArrayBytes[2] = bufferDeRecepcao[1];
   id_Concentrador.toArrayBytes[3] = bufferDeRecepcao[0];

   id_Medidor[0] = rxMessage.Payload[7];
   id_Medidor[1] = rxMessage.Payload[8];
   id_Medidor[2] = rxMessage.Payload[9];
   id_Medidor[3] = rxMessage.Payload[10];
   id_Medidor[4] = rxMessage.Payload[11];
   id_Medidor[5] = rxMessage.Payload[12];
   id_Medidor[6] = rxMessage.Payload[13];
   id_Medidor[7] = rxMessage.Payload[14];

   dataHora[0] = rxMessage.Payload[20];
   dataHora[1] = rxMessage.Payload[19];
   dataHora[2] = rxMessage.Payload[18];
   dataHora[3] = rxMessage.Payload[15];
   dataHora[4] = 1;
   dataHora[5] = rxMessage.Payload[16];
   dataHora[6] = rxMessage.Payload[17];





//   dataHora[7] = rxMessage.Payload[16];

   retorno = gravaDataHoraMedidor(id_Medidor,dataHora);
   montaPacoteRespostaGravaData_HoraMedidor(id_Concentrador.numero,id_Medidor,retorno,pacote);


}

void trataPacoteRequisitaDadosDeMedicaoUC(BYTE *bufferDeRecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote)
{

     conversor4 id_Concentrador;
     BYTE id_Medidor[8];
     BYTE retornoTensao[2], retornoCorrente[2],retornopotenciaAtiva[4];

     id_Concentrador.toArrayBytes[0] = bufferDeRecepcao[3];
     id_Concentrador.toArrayBytes[1] = bufferDeRecepcao[2];
     id_Concentrador.toArrayBytes[2] = bufferDeRecepcao[1];
     id_Concentrador.toArrayBytes[3] = bufferDeRecepcao[0];

     id_Medidor[0] = rxMessage.Payload[7];
     id_Medidor[1] = rxMessage.Payload[8];
     id_Medidor[2] = rxMessage.Payload[9];
     id_Medidor[3] = rxMessage.Payload[10];
     id_Medidor[4] = rxMessage.Payload[11];
     id_Medidor[5] = rxMessage.Payload[12];
     id_Medidor[6] = rxMessage.Payload[13];
     id_Medidor[7] = rxMessage.Payload[14];

//     getTensaoFaseMedidor(retornoTensao);
//     getCorrenteFaseMedidor(retornoCorrente);
//     getPotenciaAtiva(retornopotenciaAtiva);
     memcpy(retornoTensao, medidorTensaoFaseMedidor,2);
     memcpy(retornoCorrente, medidorCorrenteFaseMedidor,2);
     memcpy(retornopotenciaAtiva, medidorPotenciaAtiva,4);
//     Printf("\r\n");
//     PrintDec(retornoCorrente[0]);
//     Printf("|");
//     PrintDec(retornoCorrente[1]);
//     Printf("\r\nRetorno tensao\r\n");
//     PrintDec(retornoTensao[0]);
//     Printf("|");
//     PrintDec(retornoTensao[1]);
//
//     Printf("\r\n tensao:\r\n");
//     PrintDec(medidorTensaoFaseMedidor[0]);
//     Printf("|");
//     PrintDec(medidorTensaoFaseMedidor[1]);
//     Printf("|");
//     PrintDec(potenciaNominal[2]);
//     Printf("|");
//     PrintDec(potenciaNominal[3]);
//     Printf("\r\n");


     montaPacoteRespostaRequisitaDadosDeMedicaoUC(id_Concentrador.numero,id_Medidor,retornoTensao,retornoCorrente,retornopotenciaAtiva,pacote);
}

void trataPacoteRespostaArmaRele(BYTE *bufferDeRecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote)
{

     conversor4 id_Concentrador;
     BYTE id_Medidor[8];


     id_Concentrador.toArrayBytes[0] = bufferDeRecepcao[3];
     id_Concentrador.toArrayBytes[1] = bufferDeRecepcao[2];
     id_Concentrador.toArrayBytes[2] = bufferDeRecepcao[1];
     id_Concentrador.toArrayBytes[3] = bufferDeRecepcao[0];

     id_Medidor[0] = rxMessage.Payload[7];
     id_Medidor[1] = rxMessage.Payload[8];
     id_Medidor[2] = rxMessage.Payload[9];
     id_Medidor[3] = rxMessage.Payload[10];
     id_Medidor[4] = rxMessage.Payload[11];
     id_Medidor[5] = rxMessage.Payload[12];
     id_Medidor[6] = rxMessage.Payload[13];
     id_Medidor[7] = rxMessage.Payload[14];

     ativador_armar = 1;

     montaPacoteRespostaArmaRele(id_Concentrador.numero,id_Medidor,0,pacote);

}


void trataPacoteRespostaDesarmaRele(BYTE *bufferDeRecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote)
{
     conversor4 id_Concentrador;
     BYTE id_Medidor[8];


     id_Concentrador.toArrayBytes[0] = bufferDeRecepcao[3];
     id_Concentrador.toArrayBytes[1] = bufferDeRecepcao[2];
     id_Concentrador.toArrayBytes[2] = bufferDeRecepcao[1];
     id_Concentrador.toArrayBytes[3] = bufferDeRecepcao[0];

     id_Medidor[0] = rxMessage.Payload[7];
     id_Medidor[1] = rxMessage.Payload[8];
     id_Medidor[2] = rxMessage.Payload[9];
     id_Medidor[3] = rxMessage.Payload[10];
     id_Medidor[4] = rxMessage.Payload[11];
     id_Medidor[5] = rxMessage.Payload[12];
     id_Medidor[6] = rxMessage.Payload[13];
     id_Medidor[7] = rxMessage.Payload[14];

     ativador_desarmar = 1;

     montaPacoteRespostaArmaRele(id_Concentrador.numero,id_Medidor,1,pacote);
}


void trataPacoteZeraEnergiaMedidor(BYTE *bufferDeRecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote)
{

     conversor4 id_Concentrador;
     BYTE id_Medidor[8];

     id_Concentrador.toArrayBytes[0] = bufferDeRecepcao[3];
     id_Concentrador.toArrayBytes[1] = bufferDeRecepcao[2];
     id_Concentrador.toArrayBytes[2] = bufferDeRecepcao[1];
     id_Concentrador.toArrayBytes[3] = bufferDeRecepcao[0];

     id_Medidor[0] = rxMessage.Payload[7];
     id_Medidor[1] = rxMessage.Payload[8];
     id_Medidor[2] = rxMessage.Payload[9];
     id_Medidor[3] = rxMessage.Payload[10];
     id_Medidor[4] = rxMessage.Payload[11];
     id_Medidor[5] = rxMessage.Payload[12];
     id_Medidor[6] = rxMessage.Payload[13];
     id_Medidor[7] = rxMessage.Payload[14];

//     getTensaoFaseMedidor(retornoTensao);
//     getCorrenteFaseMedidor(retornoCorrente);
//     getPotenciaAtiva(retornopotenciaAtiva);
     //     Printf("\r\n");
//     PrintDec(retornoCorrente[0]);
//     Printf("|");
//     PrintDec(retornoCorrente[1]);
//     Printf("\r\nRetorno tensao\r\n");
//     PrintDec(retornoTensao[0]);
//     Printf("|");
//     PrintDec(retornoTensao[1]);
//
//     Printf("\r\n tensao:\r\n");
//     PrintDec(medidorTensaoFaseMedidor[0]);
//     Printf("|");
//     PrintDec(medidorTensaoFaseMedidor[1]);
//     Printf("|");
//     PrintDec(potenciaNominal[2]);
//     Printf("|");
//     PrintDec(potenciaNominal[3]);
//     Printf("\r\n");
     zerarEnergiaMedidor();

     montaPacoteRespostaZeraEnergiaMedidor(id_Concentrador.numero,id_Medidor,1,pacote);
}


void trataPacoteRequisitaTotalData(BYTE *bufferDeRecepcao,RECEIVED_MESSAGE  rxMessage, pacoteGRC *pacote)
{

     conversor4 id_Concentrador;
     BYTE id_Medidor[8];
     BYTE retornoTensao[2], retornoCorrente[2],retornopotenciaAtiva[4];

     id_Concentrador.toArrayBytes[0] = bufferDeRecepcao[3];
     id_Concentrador.toArrayBytes[1] = bufferDeRecepcao[2];
     id_Concentrador.toArrayBytes[2] = bufferDeRecepcao[1];
     id_Concentrador.toArrayBytes[3] = bufferDeRecepcao[0];

     id_Medidor[0] = rxMessage.Payload[7];
     id_Medidor[1] = rxMessage.Payload[8];
     id_Medidor[2] = rxMessage.Payload[9];
     id_Medidor[3] = rxMessage.Payload[10];
     id_Medidor[4] = rxMessage.Payload[11];
     id_Medidor[5] = rxMessage.Payload[12];
     id_Medidor[6] = rxMessage.Payload[13];
     id_Medidor[7] = rxMessage.Payload[14];

     memcpy(retornoTensao, medidorTensaoFaseMedidor,2);
     memcpy(retornoCorrente, medidorCorrenteFaseMedidor,2);
     memcpy(retornopotenciaAtiva, medidorPotenciaAtiva,4);



//     Printf("\r\n tensao:\r\n");
//     PrintDec(medidorTensaoFaseMedidor[0]);
//     Printf("|");
//     PrintDec(medidorTensaoFaseMedidor[1]);
//     Printf("|");
//     PrintDec(potenciaNominal[2]);
//     Printf("|");
//     PrintDec(potenciaNominal[3]);
//     Printf("\r\n");

     
     montaPacoteRespostaRequisitaTotalData(id_Concentrador.numero,id_Medidor,retornoTensao,retornoCorrente,retornopotenciaAtiva,medidorEnergia,medidorDataHoraMedidor,medidorFatorPotencia,pacote);

}


//BOOL   gravaInfoMedidor(WORD idMedidor, BYTE* numeroDaCasa, BYTE* unidadeConsumidora, WORD latitude, WORD longitude){}
//BOOL   gravaDataHoraMedidor(WORD idMedidor, BYTE* dataHora){}
