/*
 * File:   ConfiguracaoViaSerial.h
 * Author: Thiago Serpa
 *
 * Created on 12 de Junho de 2014, 09:10
 */

//#ifndef CONFIGURACAOVIASERIAL_H
#define	CONFIGURACAOVIASERIAL_H


///////////////////////// HEADERS ///////////////////////////////////
#include "WirelessProtocols/Console.h"
#include "Transceivers/Transceivers.h"
#include "WirelessProtocols/MCHP_API.h"
#include "../NVM.h"
#include "GenericTypeDefs.h"
#include "acessaIdentificadores.h"
#include "../ConfiguracaoViaRadio.h"
#include "../../medidorGRC.h"
#include "../serial_old.h"
#include "../globais.h"
#include "../TratadorDePacotes.h"
#include "../inicio_miwi.h"
#include "../Console.h"







#if defined TESTE

extern BYTE NeighborRoutingTable[NUM_COORDINATOR][16 / 8];
extern BYTE RoutingTable[16 / 8];
extern BYTE FamilyTree[16];
#else

extern BYTE NeighborRoutingTable[NUM_COORDINATOR][NUM_COORDINATOR / 8];
extern BYTE RoutingTable[NUM_COORDINATOR / 8];
extern BYTE FamilyTree[NUM_COORDINATOR];
#endif

extern void PHYSetShortRAMAddr(INPUT BYTE address, INPUT BYTE value);
extern void InitMRF24J40(void);
//CONNECTION_ENTRY ConnectionTable2[CONNECTION_SIZE];


extern BYTE atributos_HEX[TOTAL_DATA_PACKET_SIZE], bufferDeRecepcao[50];
extern BYTE atributos_para_enviar_ao_configurador[TOTAL_DATA_PACKET_SIZE];

extern BYTE longAddressAbuscar_HEX[LONG_ADDRESS_SIZE];

extern BOOL RECEBENDO_ATRIBUTOS_VIA_RADIO;
extern BYTE indice_buffer_RX_radio;
extern BYTE atributos_recebidos_via_radio[TOTAL_DATA_PACKET_SIZE];
extern BYTE longAddress_recebido_via_radio[LONG_ADDRESS_SIZE];

extern BYTE short_address_GRC_na_rede[SHORT_ADDRESS_SIZE];
extern BOOL CONFIGURANDO_GRC_NOVO;
extern BYTE FLAG;
extern BOOL CALIBRACAO_ATIVADA;

extern int contador;
extern pacoteGRC p, pacoteRecepcao;

extern BYTE estadoConfiguracao;
extern int i;

extern BOOL Flag;
extern uint8 retorno;
extern RECEIVED_MESSAGE receive_protocolo;

extern BYTE defaultHops;
extern BYTE MiWiPROSeqNum;
extern BYTE TxBuffer[TX_BUFFER_SIZE+MIWI_PRO_HEADER_LEN];
extern BYTE            TxData;
extern MAC_TRANS_PARAM MTP;


extern INT CONTADOR;

extern BYTE myLongAddress[8];

extern BOOL RECEBENDO_ATRIBUTOS_PELA_SERIAL;
extern BOOL RECEBENDO_LONG_ADDRESS_PELA_SERIAL;
extern BOOL RECEBENDO_SOLICITACAO_PELA_SERIAL;

extern int indiceBuffer;
extern BYTE dado;

extern BOOL RECEBENDO_POTENCIA;
extern BOOL RECEBENDO_INICE;
extern BOOL RECEBENDO_ENDERECO;
extern BOOL REQUISITANDO_REDE;
extern BOOL DESTRUINDO_REDE;
extern BOOL RECEBENDO_LONG_ADDRESS;

extern BYTE shortAddressDestinoHex[SHORT_ADDRESS_SIZE];
extern BYTE shortAddressDestinoHexInvertido[SHORT_ADDRESS_SIZE];
extern BYTE shortAddressDestinoASCII[SHORT_ADDRESS_SIZE * 2];

extern BYTE longAddressDestinoHex[MY_ADDRESS_LENGTH];
extern BYTE longAddressDestinoHexInvertido[MY_ADDRESS_LENGTH];
extern BYTE longAddressDestinoASCII[MY_ADDRESS_LENGTH * 2];

extern DWORD GRCEnergia;
extern BYTE medidorEnergia[4];
  void exibe_energia();


void PrintMenu(void) ;

void showInfo(void) ;

void PrintMenuConfiguracao(void);

void inverteArray(BYTE *ori, BYTE *dest, int size) ;

void ProcessMenuConfiguracao(void);

BYTE convertASCII_to_HEX(BYTE c);

void formatArrayToHex(BYTE* src , int16 size_dest, BYTE* dest);

void ProcessMenu(BYTE c);

void trataMenuDeEnvio(BYTE dado);

void trataPacotesVindosDaSerial(BYTE dado);



//#endif	/* CONFIGURACAOVIASERIAL_H */

